import styled, { css } from "styled-components";
import { Link } from "react-router-dom";

import { colors } from "constants/.";

export const HeaderActions = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 30px 0;

  .rightBox {
    display: flex;
    align-items: inherit;
  }
`;

export const Container = styled.div`
  padding: 0 40px;
`;

export const ActionLink = styled(Link)<{ isactive?: "true" | "false" }>`
  margin-right: 24px;
  font-weight: 500;
  font-size: 22px;
  line-height: 28px;
  text-transform: capitalize;
  text-decoration: none;
  color: ${colors.lightGrey};

  ${props =>
    props.isactive === "true" &&
    css`
      color: ${colors.shipGray};
      pointer-events: none;
    `}
`;
