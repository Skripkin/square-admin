export const ACTIONS = [
  {
    image: "./actionimage.png",
    avatar: "./actionavatar.png",
    userinicial: "Mayke Schuurs",
    date: new Date()
  },
  {
    image: "./actionicecream.png",
    avatar: "./Arthur.png",
    userinicial: "Gloria Fisher",
    date: new Date()
  },
  {
    image: "./actionapple.png",
    avatar: "./Jorge.png",
    userinicial: "Brandie Fox",
    date: new Date()
  },
  {
    image: "./actionblini.png",
    avatar: "./Marvin.png",
    userinicial: "Ralph Edwards",
    date: new Date()
  },
  {
    image: "./actionpizza.png",
    avatar: "./Serenity.png",
    userinicial: "Eduardo Henry",
    date: new Date()
  },
  {
    image: "./actiontomato.png",
    avatar: "./actionavatar.png",
    userinicial: "Gregory Pena",
    date: new Date()
  },
  {
    image: "./actionbanana.png",
    avatar: "./Arthur.png",
    userinicial: "Annette Watson",
    date: new Date()
  },
  {
    image: "./actionpae.png",
    avatar: "./Jorge.png",
    userinicial: "Robert Mckinney",
    date: new Date()
  }
];
