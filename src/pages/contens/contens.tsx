import React from "react";

import { Actions, InnerSelect } from "components";
import { urls } from "constants/.";

import { ACTIONS } from "./mock";

import { HeaderActions, Container, ActionLink } from "./styles";

const Contens = () => {
  const [age, setAge] = React.useState<string | number>(20);

  return (
    <Container>
      <HeaderActions>
        <div>
          <ActionLink isactive="true" to="#">
            White label
          </ActionLink>
          <ActionLink to={urls.ACTIONS}>Actions</ActionLink>
        </div>
        <div className="rightBox">
          <InnerSelect
            title="Show"
            value={age}
            options={[
              { label: "All items", value: 10 },
              { label: "Today", value: 15 },
              { label: "All", value: 20 }
            ]}
            onChange={value => setAge(value)}
          />
        </div>
      </HeaderActions>
      <div>
        <Actions
          actions={ACTIONS}
          onAccept={() => null}
          onCancel={() => null}
        />
      </div>
    </Container>
  );
};

export default Contens;
