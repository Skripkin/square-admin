import React from "react";

import { Team, ChatRoom } from "components";

import { CHATS, TEAMATE_USERS } from "./mock";

import { Container } from "./styles";

const Chat = () => {
  return (
    <Container>
      <div className="chatroom">
        <ChatRoom chats={CHATS} />
      </div>
      <div className="teambox">
        <Team teamate={TEAMATE_USERS} />
      </div>
    </Container>
  );
};

export default Chat;
