import styled from "styled-components";

export const Container = styled.div`
  display: flex;

  .chatroom {
    margin: 24px;
  }

  .teambox {
    margin: 24px 0;
  }
`;
