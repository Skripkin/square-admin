export const CHATS = [
  {
    avatar: "./Debra.png",
    name: "Harold Watson",
    isactive: true,
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Jerome.png",
    name: "Frank Baker",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Serenity.png",
    name: "Francisco Mccoy",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Bessie.png",
    name: "Tyrone Pena",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Cameron.png",
    name: "Scarlett Murphy",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Brandor.png",
    name: "Brandor Lo",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Bessie.png",
    name: "Tyrone Pena",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Cameron.png",
    name: "Scarlett Murphy",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Brandor.png",
    name: "Brandor Lo",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  }
];

export const TEAMATE_USERS = [
  {
    image: "./Regina.png",
    text: "Regina Miles"
  },
  {
    image: "./Jerome.png",
    text: "Jerome Rusell"
  },
  {
    image: "./Serenity.png",
    text: "Serenity Fox"
  },
  {
    image: "./Bessie.png",
    text: "Bessie Flores"
  },
  {
    image: "./Cameron.png",
    text: "Cameron Black"
  },
  {
    image: "./Debra.png",
    text: "Debra Simmons"
  },
  {
    image: "./Arthur.png",
    text: "Arthur Wilson"
  },
  {
    image: "./Marvin.png",
    text: "Marvin Simmmons"
  }
];
