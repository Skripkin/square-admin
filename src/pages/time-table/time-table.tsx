import React from "react";

import Select from "@material-ui/core/Select";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";

import {
  WeeklyPlan,
  EventsForDate,
  AddButton,
  Offer,
  Button,
  CreateNewOffer,
  ModalBox,
  SettingTimeframe,
  ChooseDay
} from "components";

import { WEEK_DAYS } from "./mock";

import {
  LeftHeader,
  WeekContainer,
  RightBottom,
  RightHeader,
  HeaderBottom,
  SelectWrapper,
  SelectText,
  HeaderClick,
  FreePaid,
  ButtomLeftBox,
  EventsContainer,
  CopyIcon,
  HeaderSetting
} from "./styles";

const TimeTable = ({ match, history }) => {
  const [age, setAge] = React.useState(20);
  const handleChange = event => {
    const { value: perPage } = event.target;

    setAge(perPage);
  };

  return (
    <>
      <div className="inner-container">
        <div>
          <div>
            <LeftHeader>
              <span className="title">Weekly plan</span>
              <HeaderSetting>
                <ChooseDay />
                <AddButton
                  onClick={() =>
                    history.push(`${match.url}/create-new-timeframe`)
                  }
                />
              </HeaderSetting>
            </LeftHeader>
            <WeekContainer>
              <WeeklyPlan weekDays={WEEK_DAYS} />
            </WeekContainer>
          </div>
          <div>
            <HeaderBottom>
              <span className="title">Events</span>
              <ButtomLeftBox>
                <FreePaid>
                  <div className="pink" />
                  <span>Free</span>
                  <div className="green" />
                  <span>Paid</span>
                </FreePaid>
                <div>
                  <SelectWrapper>
                    <SelectText>Show:</SelectText>
                    <FormControl variant="outlined">
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        value={age}
                        onChange={handleChange}
                      >
                        <MenuItem value={10}>Day</MenuItem>
                        <MenuItem value={15}>Today</MenuItem>
                        <MenuItem value={20}>All</MenuItem>
                      </Select>
                    </FormControl>
                  </SelectWrapper>
                </div>
                <HeaderClick>
                  <CopyIcon iconName="copy" />
                </HeaderClick>
                <AddButton />
              </ButtomLeftBox>
            </HeaderBottom>
            <EventsContainer>
              <EventsForDate
                paid={false}
                personCount={5}
                from={new Date()}
                to={new Date()}
              />
              <EventsForDate
                paid={true}
                personCount={2}
                from={new Date()}
                to={new Date()}
              />
              <EventsForDate
                paid={true}
                personCount={8}
                from={new Date()}
                to={new Date()}
              />
              <EventsForDate
                paid={false}
                personCount={4}
                from={new Date()}
                to={new Date()}
              />
            </EventsContainer>
          </div>
        </div>
        <div className="inner-right-block">
          <RightHeader>
            <span>offers</span>
            <Button onClick={() => history.push(`${match.url}/create-new`)}>
              <span>+ Create</span>
            </Button>
          </RightHeader>
          <RightBottom>
            <Offer
              creditCounter={100}
              images="./actionicecream.png"
              title="Milk Icecream"
              link="#"
            />
            <Offer
              creditCounter={150}
              images="./actionpizza.png"
              title="Pizza Mania"
              link="#"
            />
            <Offer
              creditCounter={150}
              images="./actionpae.png"
              title="Cheesecake"
              link="#"
            />
            <Offer
              creditCounter={150}
              images="./actionapple.png"
              title="Same title"
              link="#"
            />
          </RightBottom>
        </div>
      </div>
      <ModalBox
        type="wide"
        path={`${match.url}/create-new-timeframe`}
        parentPath={match.url}
        Component={SettingTimeframe}
      />
      <ModalBox
        type="modal"
        path={`${match.url}/create-new`}
        parentPath={match.url}
        Component={CreateNewOffer}
      />
    </>
  );
};

export default TimeTable;
