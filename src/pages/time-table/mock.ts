import { IWeekDay } from "components/weekly-plan/weekly-plan";

export const WEEK_DAYS: IWeekDay[] = [
  {
    type: "Monday",
    offers: [
      { id: 0, count: 2, usersCount: 5, from: 6, to: 6 },
      { id: 1, count: 4, usersCount: 1, from: 9, to: 11 },
      { id: 2, count: 1, usersCount: 8, from: 9, to: 9 },
      { id: 3, count: 2, usersCount: 7, from: 9, to: 12 },
      { id: 4, count: 5, usersCount: 2, from: 10, to: 10 }
    ]
  }
];
