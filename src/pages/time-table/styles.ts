import styled from "styled-components";

import { colors } from "constants/.";

import Icon from "components/icon";

export const LeftHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const HeaderSetting = styled.div`
  display: flex;
  align-items: center;
`;

export const WeekContainer = styled.div`
  margin: 20px 0;

  & > div {
    border-radius: 12px;
  }
`;

export const RightHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 14px 16px;
  border-bottom: 1px solid ${colors.gallery};

  & > button {
    border-radius: 32px;
    background-color: rgba(241, 63, 152, 0.1);

    span {
      color: #f13f98;
    }
  }

  & > span {
    font-size: 16px;
    line-height: 20px;
    text-transform: capitalize;
    color: ${colors.shipGray};
  }
`;

export const RightBottom = styled.div`
  width: 275px;
  padding: 16px;

  a {
    margin: 16px 0;
  }
`;

export const HeaderBottom = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const SelectWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-left: 24px !important;
  padding: 8px 16px;
  width: 154px;
  height: 34px;
  box-sizing: border-box;
  border-radius: 20px;
  background-color: ${colors.white};

  .MuiSelect-root {
    padding: 0;
    font-size: 14px;
    line-height: 18px;
    text-align: right;
    color: ${colors.shipGray};
  }

  .MuiSelect-select:focus {
    background-color: transparent;
  }

  svg {
    position: relative;
    right: -8px;
  }

  fieldset {
    padding: 0;
    border: none;
  }
`;

export const SelectText = styled.span`
  display: inline-block;
  margin-right: 8px;
  font-size: 14px;
  color: ${colors.lightGrey};
`;

export const HeaderClick = styled.button`
  margin: 0 8px;
  padding: 9px;
  border: 1px solid ${colors.gallery};
  box-sizing: border-box;
  border-radius: 20px;
  background: ${colors.white};
  cursor: pointer;
`;

export const FreePaid = styled.div`
  & > div {
    display: inline-block;
    width: 8px;
    height: 8px;
    margin-right: 8px;
    border-radius: 50%;
  }

  & > span {
    vertical-align: middle;
    margin: 0 8px;
  }

  .pink {
    background: ${colors.pink};
  }

  .green {
    background: ${colors.green};
  }
`;

export const ButtomLeftBox = styled.div`
  display: flex;
  align-items: center;
`;

export const EventsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding-right: 15px;

  & > div {
    margin-top: 24px;
    min-width: 200px;
  }
`;

export const CopyIcon = styled(Icon)`
  width: 20px;
  height: 20px;
  stroke: ${colors.lightGrey};
  fill: ${colors.white};
`;
