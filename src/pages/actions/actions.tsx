import React from "react";

import { urls } from "constants/.";
import { Table, InnerSelect } from "components/.";

import { HeaderActions, Container, ActionLink } from "./styles";
import { TABLE_DATA } from "./mock";
import { TABLE_COLUMNS } from "./constants";

const Actions = () => {
  const [age, setAge] = React.useState<string | number>(20);

  return (
    <Container>
      <HeaderActions>
        <div>
          <ActionLink to={urls.CONTENS}>White label</ActionLink>
          <ActionLink isactive="true" to="#">
            Actions
          </ActionLink>
        </div>
        <div className="rightBox">
          <InnerSelect
            title="Show"
            value={age}
            options={[
              { label: "All items", value: 10 },
              { label: "Today", value: 15 },
              { label: "All", value: 20 }
            ]}
            onChange={value => setAge(value)}
          />
        </div>
      </HeaderActions>
      <div className="content">
        <Table
          title=""
          data={TABLE_DATA}
          columns={TABLE_COLUMNS}
          meta={{}}
          onQueryChange={() => null}
          withoutControls
        />
      </div>
    </Container>
  );
};

export default Actions;
