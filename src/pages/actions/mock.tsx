export const TABLE_DATA = [
  {
    user: "Dianne Alexander",
    date: "2015-11-03",
    timeframe: "14:00 - 16:00",
    src: "some_link",
    avatar: "../Arthur.png",
    instagram: true,
    facebook: false,
    camera: true,
    sova: true,
    location: false
  },
  {
    user: "Arlene Robertson",
    date: "2015-11-03",
    timeframe: "14:00 - 16:00",
    src: "some_link",
    avatar: "../Arthur.png",
    instagram: false,
    facebook: false,
    camera: false,
    sova: true,
    location: false
  },
  {
    user: "Ralph Cooper",
    date: "2015-11-03",
    timeframe: "14:00 - 16:00",
    src: "some_link",
    avatar: "../Arthur.png",
    instagram: true,
    facebook: true,
    camera: false,
    sova: false,
    location: false
  },
  {
    user: "Rosemary Williamson",
    date: "2015-11-03",
    timeframe: "14:00 - 16:00",
    src: "some_link",
    avatar: "../Arthur.png",
    instagram: true,
    facebook: false,
    camera: true,
    sova: false,
    location: false
  },
  {
    user: "Shawn Fox",
    date: "2015-11-03",
    timeframe: "14:00 - 16:00",
    src: "some_link",
    avatar: "../Arthur.png",
    instagram: true,
    facebook: false,
    camera: true,
    sova: false,
    location: false
  },
  {
    user: "Albert Flores",
    date: "2015-11-03",
    timeframe: "14:00 - 16:00",
    src: "some_link",
    avatar: "../Arthur.png",
    instagram: true,
    facebook: false,
    camera: true,
    sova: false,
    location: false
  },
  {
    user: "Shane Richards",
    date: "2015-11-03",
    timeframe: "14:00 - 16:00",
    src: "some_link",
    avatar: "../Arthur.png",
    instagram: true,
    facebook: false,
    camera: true,
    sova: false,
    location: false
  },
  {
    user: "Jane Watson",
    date: "2015-11-03",
    timeframe: "14:00 - 16:00",
    src: "some_link",
    avatar: "../Arthur.png",
    instagram: true,
    facebook: true,
    camera: false,
    sova: false,
    location: false
  },
  {
    user: "Bernard Miles",
    date: "2015-11-03",
    timeframe: "14:00 - 16:00",
    src: "some_link",
    avatar: "../Arthur.png",
    instagram: true,
    facebook: true,
    camera: true,
    sova: false,
    location: false
  },
  {
    user: "Name Surname",
    date: "2015-11-03",
    timeframe: "14:00 - 16:00",
    src: "some_link",
    avatar: "../Arthur.png",
    instagram: true,
    facebook: false,
    camera: true,
    sova: false,
    location: true
  }
];
