import styled, { css } from "styled-components";
import { Link as ReactLink } from "react-router-dom";

import Done from "@material-ui/icons/Done";

import { colors } from "constants/.";
import { Icon } from "components/.";

export const HeaderActions = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 30px 0;

  .rightBox {
    display: flex;
    align-items: inherit;
  }
`;

export const Container = styled.div`
  padding: 0 40px;
`;

export const ActionLink = styled(ReactLink)<{ isactive?: "true" | "false" }>`
  margin-right: 24px;
  font-weight: 500;
  font-size: 22px;
  line-height: 28px;
  text-transform: capitalize;
  text-decoration: none;
  color: ${colors.lightGrey};

  ${props =>
    props.isactive === "true" &&
    css`
      color: ${colors.shipGray};
      pointer-events: none;
    `}
`;

export const TableLink = styled(ReactLink)`
  color: ${colors.shipGray};
  font-size: 15px;
  line-height: 19px;
  text-decoration: none;
`;

export const ColUserWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const ImgWrapper = styled.div`
  width: 40px;
  height: 40px;
  margin-right: 12px;
`;

export const Img = styled.img`
  width: 100%;
  object-fit: cover;
`;

export const ActionIcon = styled(Icon)<{ otherType?: "true" | "false" }>`
  * {
    fill: white !important;
    stroke: ${colors.lightGrey};
  }

  ${props =>
    props.otherType === "true" &&
    css`
      * {
        stroke-width: 0;
        stroke: white;
        fill: ${colors.lightGrey} !important;
      }
    `}
`;

export const ActionMark = styled(Done)<{ checked?: boolean }>`
  font-size: 16px;
  color: ${colors.gallery};

  ${props =>
    !!props.checked &&
    css`
      color: ${colors.shipGray};
    `}
`;
