import React from "react";
import _get from "lodash/get";

import {
  ColUserWrapper,
  Img,
  ImgWrapper,
  ActionIcon,
  ActionMark
} from "./styles";

const createActionColumn = (actionName, otherType?: boolean) => ({
  key: actionName,
  render: rowItem => {
    const checked = _get(rowItem, actionName);

    if (!rowItem) {
      return (
        <ActionIcon
          iconName={actionName}
          otherType={otherType ? "true" : "false"}
        />
      );
    }

    return <ActionMark checked={!!checked} />;
  }
});

const createEmptyColumn = () => ({
  key: String(Math.random()),
  render: rowItem => {
    return !rowItem ? <span /> : <span />;
  }
});

export const TABLE_COLUMNS = [
  {
    key: "user",
    label: "User",
    title: "Complex Code",
    render: rowItem => (
      <React.Fragment>
        <ColUserWrapper>
          <ImgWrapper>
            <Img src={rowItem.avatar} alt={rowItem.user} />
          </ImgWrapper>
          <span>{rowItem.user}</span>
        </ColUserWrapper>
      </React.Fragment>
    )
  },
  {
    key: "date",
    label: "Date",
    title: "Date"
  },
  {
    key: "timeframe",
    label: "Timeframe",
    title: "Timeframe"
  },
  createActionColumn("instagram"),
  createActionColumn("facebook"),
  createActionColumn("camera"),
  createActionColumn("sova", true),
  createActionColumn("location", true),
  createEmptyColumn(),
  createEmptyColumn()
];
