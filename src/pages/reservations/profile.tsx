import React from "react";
import _get from "lodash/get";

import { Profile as SQProfile } from "components/.";

import { RESERVATION } from "./mock";

const Profile = props => {
  const name = _get(props, "[0].match.params.name", "");
  const profile = RESERVATION.find(reserv => reserv.name === name);

  return <SQProfile {...profile} />;
};

export default Profile;
