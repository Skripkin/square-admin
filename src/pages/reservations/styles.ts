import styled from "styled-components";

import { colors } from "constants/.";

export const ActionText = styled.div`
  p {
    margin-right: 24px;
    font-weight: 500;
    font-size: 22px;
    line-height: 28px;
    text-transform: capitalize;
    color: ${colors.shipGray};
  }

  .left {
    font-size: 15px;
    line-height: 19px;
    color: ${colors.lightGrey};
  }
`;

export const SelectWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-left: 24px !important;
  padding: 8px 16px;
  width: 154px;
  height: 34px;
  box-sizing: border-box;
  border-radius: 20px;
  background-color: ${colors.white};

  .MuiSelect-root {
    padding: 0;
    font-size: 14px;
    line-height: 18px;
    text-align: right;
    color: ${colors.shipGray};
  }

  .MuiSelect-select:focus {
    background-color: transparent;
  }

  svg {
    position: relative;
    right: -8px;
  }

  fieldset {
    padding: 0;
    border: none;
  }
`;

export const SelectText = styled.span`
  display: inline-block;
  margin-right: 8px;
  font-size: 14px;
  color: ${colors.lightGrey};
`;

export const LeftBlock = styled.div`
  max-width: 865px;
  width: 100%;

  .reservBlock {
    margin: 16px 0;
  }
`;

export const HeadBlock = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 20px 0;
`;

export const EventContainer = styled.div`
  padding: 16px 20px;
`;

export const RightButtom = styled.div`
  background: #ffffff;
  border-top: 1px solid #f2f2f2;

  p {
    margin: 24px 20px 0;
    font-weight: 500;
    font-size: 18px;
    line-height: 23px;
    text-transform: capitalize;
    color: #404043;
  }
`;
