export const RESERVATION = [
  {
    image: "../Jerome.png",
    name: "Jerome Jones",
    admin: 1,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  },
  {
    image: "../Serenity.png",
    name: "Serenity Fox",
    admin: 0,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  },
  {
    image: "../Bessie.png",
    name: "Bessie Flores",
    admin: 1,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  },
  {
    image: "../Cameron.png",
    name: "Cameron Black",
    admin: 2,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  },
  {
    image: "../Debra.png",
    name: "Debra Simmons",
    admin: 0,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  },
  {
    image: "../Debra.png",
    name: "Debra Simmmons",
    admin: 0,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  },
  {
    image: "../Brandor.png",
    name: "Wade Steward",
    admin: 2,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  },
  {
    image: "../Brandor.png",
    name: "Brandon Watson",
    admin: 1,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  }
];

export const RESERVATION2 = [
  {
    image: "../Jerome.png",
    name: "Jerome Jones",
    admin: 1,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  },
  {
    image: "../Serenity.png",
    name: "Serenity Fox",
    admin: 0,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  },
  {
    image: "../Bessie.png",
    name: "Bessie Flores",
    admin: 1,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  }
];

export const RESERVATION3 = [
  {
    image: "../Jerome.png",
    name: "Jerome Jones",
    admin: 1,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  },
  {
    image: "../Serenity.png",
    name: "Serenity Fox",
    admin: 0,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  },
  {
    image: "../Bessie.png",
    name: "Bessie Flores",
    admin: 1,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  },
  {
    image: "../Cameron.png",
    name: "Cameron Black",
    admin: 2,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  },
  {
    image: "../Debra.png",
    name: "Debra Simmmons",
    admin: 0,
    alt: "Profile photo",
    cost: 100,
    city: "Las Vegas",
    properties: [
      { label: "Height", value: 170 },
      { label: "Size", value: 47 },
      { label: "Ethnicity", value: "White" },
      { label: "Eye color", value: "Black" },
      { label: "Hair color", value: "Black" }
    ],
    instagramProfile: [
      {
        image: "../Rectangle 1.png",
        alt: "Rectangle 1"
      },
      {
        image: "../Rectangle 2.png",
        alt: "Rectangle 2"
      },
      {
        image: "../Rectangle 3.png",
        alt: "Rectangle 3"
      },
      {
        image: "../Rectangle 4.png",
        alt: "Rectangle 4"
      }
    ]
  }
];
