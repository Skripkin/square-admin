import React from "react";

import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

import { Datepicker, Reservation, Event, ModalBox } from "components";

import { RESERVATION, RESERVATION2, RESERVATION3 } from "./mock";

import {
  SelectWrapper,
  SelectText,
  ActionText,
  LeftBlock,
  HeadBlock,
  EventContainer,
  RightButtom
} from "./styles";
import Profile from "./profile";

const ReservationPage = ({ match, history }) => {
  const [age, setAge] = React.useState(20);
  const handleChange = event => {
    const { value: perPage } = event.target;

    setAge(perPage);
  };

  const [date, setDate] = React.useState([]);

  return (
    <div className="inner-container">
      <LeftBlock>
        <HeadBlock>
          <div>
            <ActionText>
              <p>Reservations</p>
              <p className="left">15th February</p>
            </ActionText>
          </div>
          <div>
            <SelectWrapper>
              <SelectText>View:</SelectText>
              <FormControl variant="outlined">
                <Select
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  value={age}
                  onChange={handleChange}
                >
                  <MenuItem value={10}>Day</MenuItem>
                  <MenuItem value={15}>Today</MenuItem>
                  <MenuItem value={20}>All</MenuItem>
                </Select>
              </FormControl>
            </SelectWrapper>
          </div>
        </HeadBlock>
        <div className="reservBlock">
          <Reservation
            from={new Date()}
            to={new Date()}
            reserv={RESERVATION}
            onClick={({ name }) => history.push(`${match.url}/profile/${name}`)}
          />
        </div>
        <div className="reservBlock">
          <Reservation
            from={new Date()}
            to={new Date()}
            reserv={RESERVATION2}
          />
        </div>
        <div className="reservBlock">
          <Reservation
            from={new Date()}
            to={new Date()}
            reserv={RESERVATION3}
          />
        </div>
      </LeftBlock>
      <div className="inner-right-block">
        <Datepicker value={date} onChange={setDate} />
        <RightButtom>
          <p>Upcoming Events</p>
          <EventContainer>
            <Event
              members={Array(9)
                .fill("")
                .map(() => "./user.png")}
              date={new Date()}
              status="active"
            />
          </EventContainer>
          <EventContainer>
            <Event
              members={Array(4)
                .fill("")
                .map(() => "./user.png")}
              date={new Date()}
              status="inactive"
            />
          </EventContainer>
          <EventContainer>
            <Event
              members={Array(8)
                .fill("")
                .map(() => "./user.png")}
              date={new Date()}
              status="active"
            />
          </EventContainer>
        </RightButtom>
      </div>
      <ModalBox
        parentPath={match.url}
        path={`${match.url}/profile/:name`}
        Component={Profile}
      />
    </div>
  );
};

export default ReservationPage;
