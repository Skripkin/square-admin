import React from "react";

import { SubMenu, Table } from "components";

import { SUB_MENU_ITEMS, TABLE_COLUMNS, TABLE_DATA } from "./mock";

import { Container } from "./styles";

const Billing = () => {
  return (
    <Container>
      <Table
        title="Billing"
        columns={TABLE_COLUMNS}
        data={TABLE_DATA}
        onQueryChange={() => null}
        meta={{}}
      />
      <SubMenu links={SUB_MENU_ITEMS} />
    </Container>
  );
};

export default Billing;
