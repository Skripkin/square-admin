import { urls } from "constants/.";

export const SUB_MENU_ITEMS = [
  { icon: "user", title: "Account Settings", link: urls.ACCOUNTSETTINGS },
  { icon: "image", title: "Images", link: urls.IMAGES },
  { icon: "single-folded-content", title: "Contacts", link: "#" },
  { icon: "setting", title: "Company Details", link: "#" },
  { icon: "credit", title: "Payment Method", link: "#" },
  { icon: "lightning", title: "Subscription", link: urls.SUBSCRIPTIONS },
  {
    icon: "single-folded-content",
    title: "Billing",
    link: urls.BILLING,
    isactive: true
  }
];

export const TABLE_COLUMNS = [
  {
    key: "invoice",
    label: "Invoice"
  },
  {
    key: "date",
    label: "Date"
  },
  {
    key: "period",
    label: "Period"
  },
  {
    key: "products",
    label: "Products"
  },
  {
    key: "price",
    label: "Price"
  }
];

export const TABLE_DATA = [
  {
    invoice: "Invoice n* 180/2019",
    date: "Aug 16th, 2019",
    period: "2019.07.13 - 2019.08.12",
    products: "01+02+03+4xPh",
    price: "$120.00"
  },
  {
    invoice: "Invoice n* 180/2019",
    date: "Aug 16th, 2019",
    period: "2019.07.13 - 2019.08.12",
    products: "01+02+03+4xPh",
    price: "$120.00"
  },
  {
    invoice: "Invoice n* 180/2019",
    date: "Aug 16th, 2019",
    period: "2019.07.13 - 2019.08.12",
    products: "01+02+03+4xPh",
    price: "$120.00"
  }
];
