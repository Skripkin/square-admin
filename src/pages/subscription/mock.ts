import { urls } from "constants/.";

export const SUB_MENU_ITEMS = [
  { icon: "user", title: "Account Settings", link: urls.ACCOUNTSETTINGS },
  { icon: "image", title: "Images", link: urls.IMAGES },
  { icon: "single-folded-content", title: "Contacts", link: "#" },
  { icon: "setting", title: "Company Details", link: "#" },
  { icon: "credit", title: "Payment Method", link: "#" },
  {
    icon: "lightning",
    title: "Subscription",
    link: urls.SUBSCRIPTIONS,
    isactive: true
  },
  { icon: "single-folded-content", title: "Billing", link: urls.BILLING }
];

export const SUB_MENU_ITEMS2 = [
  { Icon: "", title: "Choose location", link: "#" },
  { Icon: "", title: "55 Milano", link: "#" },
  { Icon: "", title: "Pizza mania", link: "#" },
  { Icon: "", title: "+ Create New", link: "#" }
];
