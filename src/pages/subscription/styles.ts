import styled from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div`
  position: relative;
  padding: 24px 330px 24px 40px;

  .MenuBlock > div:last-child {
    top: 260px;
  }

  .FirstMenu > div > a:last-child {
    color: ${colors.pink};
  }
`;

export const HeaderBlock = styled.div`
  background: ${colors.whisper};
  border: 1px solid ${colors.gallery};
  box-sizing: border-box;
  border-radius: 12px 12px 0 0;

  p {
    padding: 20px 24px;
    font-weight: 500;
    font-size: 18px;
    line-height: 23px;
    text-align: left;
    text-transform: capitalize;
    color: ${colors.shipGray};
  }
`;

export const ItemBox = styled.div`
  display: flex;
`;

export const CheckContainer = styled.div`
  background: ${colors.white};
  border-left: 1px solid ${colors.gallery};
  border-bottom: 1px solid ${colors.gallery};
  border-radius: 0 0 0 12px;
  box-sizing: border-box;

  p {
    margin: 25px 32px;
    font-size: 16px;
    line-height: 20px;
    text-transform: capitalize;
    color: ${colors.darkGrey};
  }

  button {
    margin: 16px 15px;
    min-width: 188px;
  }
`;

export const SubContainer = styled.div`
  background: ${colors.white};
  border: 1px solid ${colors.gallery};
  border-top: none;
  border-radius: 0 0 12px 0;
  box-sizing: border-box;
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);

  & > p {
    margin: 25px 40px;
    font-size: 16px;
    line-height: 20px;
    text-transform: capitalize;
    color: ${colors.darkGrey};
  }

  & > div {
    margin: 16px 40px;
  }

  & > div:last-child {
    margin: 0;
  }
`;

export const ButtomBox = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: baseline;
  margin-top: 60px !important;
  padding: 16px;
  border-top: 1px solid ${colors.gallery};

  .UpButton {
    padding: 10px 24px;
  }

  .bottomBox > span {
    vertical-align: middle;
  }

  .title {
    font-weight: 500;
    font-size: 18px;
    line-height: 23px;
    color: ${colors.shipGray};
    margin-right: 12px;
  }

  .price {
    font-weight: bold;
    font-size: 24px;
    line-height: 30px;
    color: #f13f98;
    margin-right: 8px;
  }

  .months {
    font-size: 16px;
    line-height: 20px;
    color: ${colors.darkGrey};
  }
`;
