import React from "react";

import { SubMenu, Checkbox, Subscription, Button } from "components";

import { SUB_MENU_ITEMS, SUB_MENU_ITEMS2 } from "./mock";
import {
  Container,
  ItemBox,
  CheckContainer,
  SubContainer,
  ButtomBox,
  HeaderBlock
} from "./styles";

const Subscriptions = () => {
  const [checked, setChecked] = React.useState(false);

  return (
    <Container>
      <div className="LeftContainer">
        <HeaderBlock>
          <p>Subscription</p>
        </HeaderBlock>
        <ItemBox>
          <CheckContainer>
            <p>Select items</p>
            <Checkbox
              checked={checked}
              name="instagram-stories"
              title="Instagram stories"
              onChange={(_, checked) => setChecked(checked)}
            />
            <Checkbox
              checked={checked}
              name="No tags"
              title="No tags"
              onChange={(_, checked) => setChecked(checked)}
            />
            <Checkbox
              checked={checked}
              name="Open reviews"
              title="Open reviews"
              onChange={(_, checked) => setChecked(checked)}
            />
            <Checkbox
              checked={checked}
              name="10k+ Only"
              title="10k+ Only"
              onChange={(_, checked) => setChecked(checked)}
            />
            <Checkbox
              checked={checked}
              name="Super contents"
              title="Super contents"
              onChange={(_, checked) => setChecked(checked)}
            />
            <Checkbox
              checked={checked}
              name="Smm service"
              title="Smm service"
              onChange={(_, checked) => setChecked(checked)}
            />
            <Checkbox
              checked={checked}
              name="Events no limit"
              title="Events no limit"
              onChange={(_, checked) => setChecked(checked)}
            />
            <Checkbox
              checked={checked}
              name="Boost me up"
              title="Boost me up"
              onChange={(_, checked) => setChecked(checked)}
            />
          </CheckContainer>
          <SubContainer>
            <p>Select items</p>
            <Subscription
              title="IG Stories"
              price="$99"
              description="Consectetur ipsum et Lorem velit labore voluptate aliqua amet culpa veniam labore dolore aute reprehenderit"
            />
            <Subscription
              title="10k Only"
              price="$40"
              description="Consectetur ipsum et Lorem velit labore voluptate aliqua amet culpa veniam labore dolore aute reprehenderit"
            />
            <Subscription
              title="Super Contents"
              price="$25"
              description="Consectetur ipsum et Lorem velit labore voluptate aliqua amet culpa veniam labore dolore aute reprehenderit"
            />
            <ButtomBox>
              <div className="bottomBox">
                <span className="title">Total:</span>
                <span className="price">$164</span>
                <span className="months">/month</span>
              </div>
              <Button className="UpButton">Upgrade my plan</Button>
            </ButtomBox>
          </SubContainer>
        </ItemBox>
      </div>
      <div className="MenuBlock">
        <div className="FirstMenu">
          <SubMenu links={SUB_MENU_ITEMS2} />
        </div>
        <SubMenu links={SUB_MENU_ITEMS} />
      </div>
    </Container>
  );
};

export default Subscriptions;
