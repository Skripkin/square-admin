import { urls } from "constants/.";

export const SUB_MENU_ITEMS = [
  { icon: "user", title: "Account Settings", link: urls.ACCOUNTSETTINGS },
  { icon: "image", title: "Images", link: urls.IMAGES, isactive: true },
  { icon: "single-folded-content", title: "Contacts", link: "#" },
  { icon: "setting", title: "Company Details", link: "#" },
  { icon: "credit", title: "Payment Method", link: "#" },
  { icon: "lightning", title: "Subscription", link: urls.SUBSCRIPTIONS },
  { icon: "single-folded-content", title: "Billing", link: urls.BILLING }
];
