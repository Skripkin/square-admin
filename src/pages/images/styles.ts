import styled from "styled-components";

import { colors } from "constants/.";

export const TitleWrapper = styled.div`
  padding: 21px 24px;
  border: 1px solid ${colors.gallery};
  border-radius: 12px 12px 0 0;
  background-color: ${colors.whisper};
`;

export const Title = styled.h1`
  font-size: 18px;
  color: ${colors.shipGray};
`;

export const Content = styled.div`
  padding: 24px;
  height: 100%;
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);
  border-radius: 0 0 12px 12px;
  background-color: white;
`;
