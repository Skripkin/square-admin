import React from "react";

import { UploadImages, SubMenu } from "components/.";

import { Content, TitleWrapper, Title } from "./styles";
import { SUB_MENU_ITEMS } from "./mock";

const Images = () => {
  const [images, setImages] = React.useState([{ src: "./upsimage.png" }]);

  return (
    <div className="inner-container">
      <TitleWrapper>
        <Title>Images</Title>
      </TitleWrapper>
      <Content>
        <UploadImages maxLength={10} onChange={setImages} images={images} />
      </Content>
      <SubMenu links={SUB_MENU_ITEMS} />
    </div>
  );
};

export default Images;
