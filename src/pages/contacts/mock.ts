import Person from "@material-ui/icons/PersonOutline";
import BrokenImage from "@material-ui/icons/BrokenImageOutlined";
import InsertDriveFile from "@material-ui/icons/InsertDriveFileOutlined";
import Settings from "@material-ui/icons/SettingsApplicationsOutlined";
import Payment from "@material-ui/icons/PaymentOutlined";
import Subscription from "@material-ui/icons/SubscriptionsOutlined";

import { urls } from "./../../constants/.";

export const SUB_MENU_ITEMS = [
  { Icon: Person, title: "Account Settings", link: "#" },
  { Icon: BrokenImage, title: "Images", link: "#" },
  {
    Icon: InsertDriveFile,
    title: "Contacts",
    link: urls.CONTACTS,
    isactive: true
  },
  { Icon: Settings, title: "Company Details", link: "#" },
  { Icon: Payment, title: "Payment Method", link: "#" },
  { Icon: Subscription, title: "Subscription", link: "#" },
  { Icon: InsertDriveFile, title: "Billing", link: urls.BILLING }
];

export const SELECT_PHONE_CODE = [
  {
    title: "+62",
    value: "+62"
  },
  {
    title: "+7",
    value: "+7"
  }
];

export const SELECTED_STAF = [
  {
    image: "./Arthur.png",
    text: "Arthur Wilson"
  },
  {
    image: "./Jorge.png",
    text: "Jorge Flores"
  },
  {
    image: "./Marvin.png",
    text: "Marvin Simmmons"
  },
  {
    image: "./Serenity.png",
    text: "Serenity Simmmons"
  }
];
