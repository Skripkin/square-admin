import styled from "styled-components";
import { colors } from "constants/.";

export const Container = styled.div`
  position: relative;
  padding: 24px 350px 24px 40px;
`;

export const Title = styled.div`
  width: 100%;
  padding: 21px 0 20px 24px;
  background-color: ${colors.whisper};
  border: 1px solid ${colors.gallery};
  box-sizing: border-box;
  border-radius: 12px 12px 0 0;

  p {
    margin-right: auto;
    font-weight: 500;
    font-size: 18px;
    line-height: 23px;
    text-align: left;
    text-transform: capitalize;
    color: ${colors.shipGray};
  }
`;

export const ContactsContainer = styled.div`
  padding: 8px 24px 0 22px;
  background-color: ${colors.white};
  border: 1px solid ${colors.gallery};
  box-sizing: border-box;
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);
  border-radius: 0 0 12px 12px;
`;

export const Inputs = styled.div`
  max-width: 495px;
  input {
    background-color: ${colors.whisper};
  }
`;

export const DoubleInput = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;

  .MuiFormControl-root {
    margin-top: 24px;
  }
`;

export const DoubleInputNoWrap = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 24px 0;
`;

export const HashtagInput = styled.div`
  input {
    color: ${colors.cornflowerBlue};
  }
`;

export const PhoneContainer = styled.div`
  display: flex;
  padding-left: 40px;

  & * > p {
    white-space: nowrap;
  }

  .MuiFormControl-root {
    min-width: 85px;
  }

  .MuiOutlinedInput-root {
    border-radius: 4px 0 0 4px;
    background-color: ${colors.whisper};
  }

  input[type="text"] {
    margin-top: 17px;
    border-radius: 0 4px 4px 0;
  }
`;

export const DivideLabel = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 40px;

  span {
    font-weight: 500;
    font-size: 16px;
    line-height: 20px;
    color: ${colors.lightGrey};
  }

  .MuiDivider-root {
    width: 70%;
    padding-left: 24px;
    margin-top: 3px;
    border: 1px solid ${colors.concrete};
  }
`;

export const StaffContainer = styled.div`
  margin: 24px 0;
`;
