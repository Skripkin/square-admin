import React from "react";

import { SubMenu, Select, Input, StafContact } from "components";

import { SELECTED_STAF, SUB_MENU_ITEMS, SELECT_PHONE_CODE } from "./mock";

import {
  Container,
  Title,
  ContactsContainer,
  PhoneContainer,
  DoubleInput,
  DoubleInputNoWrap,
  Inputs,
  DivideLabel,
  StaffContainer,
  HashtagInput
} from "./styles";
import { Divider } from "@material-ui/core";

const Contacts = () => {
  return (
    <>
      <Container>
        <SubMenu links={SUB_MENU_ITEMS} />
        <Title>
          <p>Contacts</p>
        </Title>
        <ContactsContainer>
          <Inputs>
            <DoubleInputNoWrap>
              <Input
                placeholder=""
                title="Email"
                value=""
                onChange={() => null}
              />

              <PhoneContainer>
                <Select
                  title="Phone number"
                  options={SELECT_PHONE_CODE}
                  value={SELECT_PHONE_CODE[0].value}
                  onChange={() => null}
                />
                <Input
                  placeholder=""
                  title=" "
                  value=""
                  onChange={() => null}
                />
              </PhoneContainer>
            </DoubleInputNoWrap>

            <Input
              placeholder=""
              title="Website"
              value=""
              onChange={() => null}
            />

            <DivideLabel>
              <span>Social accounts</span>
              <Divider />
            </DivideLabel>

            <DoubleInput>
              <Input
                placeholder=""
                title="Instagram"
                value=""
                onChange={() => null}
              />
              <Input
                placeholder=""
                title="Facebook"
                value=""
                onChange={() => null}
              />
              <Input
                placeholder=""
                title="Tripadvisor"
                value=""
                onChange={() => null}
              />
              <Input
                placeholder=""
                title="Google Maps"
                value=""
                onChange={() => null}
              />
              <Input
                placeholder=""
                title="Yelp"
                value=""
                onChange={() => null}
              />
              <HashtagInput>
                <Input
                  placeholder=""
                  title="Hashtags"
                  value="#tag1, #tag2, tag3"
                  onChange={() => null}
                />
              </HashtagInput>
            </DoubleInput>

            <DivideLabel>
              <span>Staf contacts</span>
              <Divider />
            </DivideLabel>
          </Inputs>
          <StaffContainer>
            <StafContact offers={SELECTED_STAF} />
          </StaffContainer>
          <Inputs>
            <Input
              placeholder=""
              title="Manager Name"
              value=""
              onChange={() => null}
            />
            <DoubleInputNoWrap>
              <Input
                placeholder=""
                title="Email"
                value=""
                onChange={() => null}
              />

              <PhoneContainer>
                <Select
                  title="Phone number"
                  options={SELECT_PHONE_CODE}
                  value={SELECT_PHONE_CODE[0].value}
                  onChange={() => null}
                />
                <Input
                  placeholder=""
                  title=" "
                  value=""
                  onChange={() => null}
                />
              </PhoneContainer>
            </DoubleInputNoWrap>
          </Inputs>
        </ContactsContainer>
      </Container>
    </>
  );
};

export default Contacts;
