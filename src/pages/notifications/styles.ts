import { Link as ReactLink } from "react-router-dom";

import styled from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div`
  padding: 0 40px;
`;

export const TableLink = styled(ReactLink)`
  color: ${colors.shipGray};
  font-size: 15px;
  line-height: 19px;
  text-decoration: none;
`;

export const ColUserWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const ImgWrapper = styled.div`
  width: 40px;
  height: 40px;
  margin-right: 12px;
`;

export const Img = styled.img`
  width: 100%;
  object-fit: cover;
`;
