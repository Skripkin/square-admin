import React from "react";

import { PrimaryTable } from "components";

import { TABLE_DATA } from "./mock";

import {
  Container,
  TableLink,
  ColUserWrapper,
  Img,
  ImgWrapper
} from "./styles";

const Contens = () => {
  const TABLE_COLUMNS = [
    {
      key: "user",
      label: "User",
      title: "Complex Code",
      render: rowItem => (
        <React.Fragment>
          <ColUserWrapper>
            <ImgWrapper>
              <Img src={rowItem.avatar} alt={rowItem.user} />
            </ImgWrapper>
            <span>{rowItem.user}</span>
          </ColUserWrapper>
        </React.Fragment>
      )
    },
    {
      key: "details",
      label: "Details",
      title: "Complex Code",
      render: rowItem => (
        <React.Fragment>
          <TableLink to={rowItem.src}>{rowItem.details}</TableLink>
        </React.Fragment>
      )
    },
    {
      key: "date",
      label: "Date",
      title: "Date"
    }
  ];

  return (
    <Container>
      <PrimaryTable
        title="Notifications"
        columns={TABLE_COLUMNS}
        data={TABLE_DATA}
        onQueryChange={() => null}
        meta={{}}
      />
    </Container>
  );
};

export default Contens;
