export const TABLE_DATA = [
  {
    user: "Dianne Alexander",
    details:
      "Ex culpa et excepteur ipsum non occaecat non nisi eu ex nostrud laborum proident .",
    date: "14:00 - 16:00",
    src: "some_link",
    avatar: "./Arthur.png"
  },
  {
    user: "Arlene Robertson",
    details:
      "Ex culpa et excepteur ipsum non occaecat non nisi eu ex nostrud laborum proident .",
    date: "14:00 - 16:00",
    src: "some_link",
    avatar: "./Arthur.png"
  },
  {
    user: "Ralph Cooper",
    details:
      "Ex culpa et excepteur ipsum non occaecat non nisi eu ex nostrud laborum proident .",
    date: "14:00 - 16:00",
    src: "some_link",
    avatar: "./Arthur.png"
  },
  {
    user: "Rosemary Williamson",
    details:
      "Ex culpa et excepteur ipsum non occaecat non nisi eu ex nostrud laborum proident .",
    date: "14:00 - 16:00",
    src: "some_link",
    avatar: "./Arthur.png"
  },
  {
    user: "Shawn Fox",
    details:
      "Ex culpa et excepteur ipsum non occaecat non nisi eu ex nostrud laborum proident .",
    date: "14:00 - 16:00",
    src: "some_link",
    avatar: "./Arthur.png"
  },
  {
    user: "Albert Flores",
    details:
      "Ex culpa et excepteur ipsum non occaecat non nisi eu ex nostrud laborum proident .",
    date: "14:00 - 16:00",
    src: "some_link",
    avatar: "./Arthur.png"
  },
  {
    user: "Shane Richards",
    details:
      "Ex culpa et excepteur ipsum non occaecat non nisi eu ex nostrud laborum proident .",
    date: "14:00 - 16:00",
    src: "some_link",
    avatar: "./Arthur.png"
  },
  {
    user: "Jane Watson",
    details:
      "Ex culpa et excepteur ipsum non occaecat non nisi eu ex nostrud laborum proident .",
    date: "14:00 - 16:00",
    src: "some_link",
    avatar: "./Arthur.png"
  },
  {
    user: "Bernard Miles",
    details:
      "Ex culpa et excepteur ipsum non occaecat non nisi eu ex nostrud laborum proident .",
    date: "14:00 - 16:00",
    src: "some_link",
    avatar: "./Arthur.png"
  },
  {
    user: "Name Surname",
    details:
      "Ex culpa et excepteur ipsum non occaecat non nisi eu ex nostrud laborum proident .",
    date: "14:00 - 16:00",
    src: "some_link",
    avatar: "./Arthur.png"
  }
];
