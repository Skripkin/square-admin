import styled from "styled-components";
import { Link } from "react-router-dom";

import { colors } from "constants/.";

export const Container = styled.div`
  padding: 24px 40px;
`;

export const TitleWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const Text = styled.span`
  font-size: 18px;
`;

export const MetricsContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  flex-wrap: wrap;
  margin-top: 24px;
`;

export const MetricWrapper = styled.div`
  margin-right: 24px;
  margin-bottom: 24px;
  width: calc(25% - 18px);

  &:last-child {
    margin-right: 0;
  }
`;

export const BoxesWrapper = styled.div`
  display: flex;
  align-items: flex-start;
`;

export const Box = styled.div`
  margin-top: 16px;
  padding: 24px;
  width: calc(25% - 16px);
  border: 1px solid ${colors.gallery};
  box-sizing: border-box;
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);
  border-radius: 12px;
  background-color: white;
`;

export const BookingsBox = styled(Box)`
  margin-right: 24px;
  width: 75%;
`;

export const BookingWrapper = styled(MetricWrapper)`
  width: calc(33% - 14px);
`;

export const BoxTitle = styled.span`
  font-size: 18px;
  text-transform: capitalize;
  color: ${colors.shipGray};
`;

export const SeeAll = styled(Link)`
  font-size: 16px;
  font-weight: 500;
  text-decoration: none;
  color: ${colors.pink};
`;

export const UpcomingEventWrapper = styled.div`
  position: relative;
  padding-top: 16px;
  padding-bottom: 16px;

  &::after {
    content: "";
    position: absolute;
    right: -24px;
    left: -24px;
    height: 1px;
    bottom: 0;
    background-color: ${colors.concrete};
  }

  &:first-child {
    margin-top: 8px;
  }

  &:last-child {
    padding-bottom: 6px;

    &::after {
      content: initial;
    }
  }
`;
