import React from "react";

import { Metric, Booking, InnerSelect, UpcomingEvent } from "components";

import { METRICS, BOOKING } from "./mock";

import {
  Container,
  TitleWrapper,
  MetricsContainer,
  MetricWrapper,
  BoxesWrapper,
  Box,
  BookingsBox,
  BookingWrapper,
  BoxTitle,
  SeeAll,
  UpcomingEventWrapper
} from "./styles";

const Home = () => {
  const [days, setDays] = React.useState<string | number>(30);

  return (
    <Container>
      <TitleWrapper>
        <span className="title">Metrics</span>
        <InnerSelect
          title="Show"
          value={days}
          options={[{ label: "30 Days", value: 30 }]}
          onChange={value => setDays(value)}
        />
      </TitleWrapper>

      <MetricsContainer>
        {METRICS.map((metric, i) => (
          <MetricWrapper key={i}>
            <Metric {...metric} />
          </MetricWrapper>
        ))}
      </MetricsContainer>

      <BoxesWrapper>
        <BookingsBox>
          <TitleWrapper>
            <BoxTitle>Upcoming Bookings</BoxTitle>
            <SeeAll to="#">See all</SeeAll>
          </TitleWrapper>

          <MetricsContainer>
            <BookingWrapper>
              <Booking {...BOOKING} />
            </BookingWrapper>
            <BookingWrapper>
              <Booking {...BOOKING} />
            </BookingWrapper>
            <BookingWrapper>
              <Booking {...BOOKING} />
            </BookingWrapper>
          </MetricsContainer>
        </BookingsBox>
        <Box>
          <TitleWrapper>
            <BoxTitle>Upcoming Events</BoxTitle>
            <SeeAll to="#">See all</SeeAll>
          </TitleWrapper>
          <div>
            <UpcomingEventWrapper>
              <UpcomingEvent
                from={new Date()}
                to={new Date()}
                type="Paid"
                budget={250}
                spot={{ value: 6, max: 10 }}
                members={{
                  images: Array(5)
                    .fill("")
                    .map(() => "./user.png"),
                  counter: 5
                }}
              />
            </UpcomingEventWrapper>
            <UpcomingEventWrapper>
              <UpcomingEvent
                from={new Date()}
                to={new Date()}
                type="Free"
                spot={{ value: 4, max: 5 }}
                members={{
                  images: Array(5)
                    .fill("")
                    .map(() => "./user.png"),
                  counter: 3
                }}
              />
            </UpcomingEventWrapper>
          </div>
        </Box>
      </BoxesWrapper>
    </Container>
  );
};

export default Home;
