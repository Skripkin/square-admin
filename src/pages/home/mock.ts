import { MetricType } from "components/metric/metric";

export const METRICS: MetricType[] = [
  {
    icon: "checkuser",
    count: 169,
    text: "Total booking",
    cost: 3,
    sign: "+"
  },
  {
    icon: "lightning",
    count: 328,
    text: "Total Actions",
    cost: 6,
    sign: "-"
  },
  {
    icon: "box",
    count: 39,
    text: "All contents",
    cost: 1,
    sign: "-"
  },
  {
    icon: "wallet",
    count: "$250.00",
    text: "Wallet",
    cost: 7,
    sign: "+"
  }
];

export const BOOKING = {
  slotCounter: 10,
  members: [
    {
      dateFrom: new Date(),
      dateTo: new Date(),
      members: Array(8)
        .fill("")
        .map(() => "./user.png")
    },
    {
      dateFrom: new Date(),
      dateTo: new Date(),
      members: Array(5)
        .fill("")
        .map(() => "./user.png")
    },
    {
      dateFrom: new Date(),
      dateTo: new Date(),
      members: Array(5)
        .fill("")
        .map(() => "./user.png")
    }
  ],
  date: new Date()
};

export const CHATS = [
  {
    avatar: "./Debra.png",
    name: "Harold Watson",
    isactive: true,
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Jerome.png",
    name: "Frank Baker",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Serenity.png",
    name: "Francisco Mccoy",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Bessie.png",
    name: "Tyrone Pena",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Cameron.png",
    name: "Scarlett Murphy",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Brandor.png",
    name: "Brandor Lo",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Bessie.png",
    name: "Tyrone Pena",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Cameron.png",
    name: "Scarlett Murphy",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  },
  {
    avatar: "./Brandor.png",
    name: "Brandor Lo",
    text: "Sunt eu elit eu non ad irure ut consectetur pariatur",
    date: "9:34 am"
  }
];

export const SELECT_OPTIONS = [
  {
    title: "SomeItems",
    value: "1"
  },
  {
    title: "SomeItems",
    value: "2"
  },
  {
    title: "SomeItems",
    value: "3"
  },
  {
    title: "SomeItems",
    value: "4"
  },
  {
    title: "SomeItems",
    value: "5"
  }
];

export const SELECTED_OFFERS = [
  {
    image: "./ice-cream.png",
    text: "Milk Icecream"
  },
  {
    image: "./pizza.png",
    text: "Pizza Mania"
  },
  {
    image: "./salat.png",
    text: "Cheesecake"
  }
];

export const RESERVATION = [
  {
    image: "./Jerome.png",
    name: "Jerome Jones",
    admin: 1
  },
  {
    image: "./Serenity.png",
    name: "Serenity Fox",
    admin: 0
  },
  {
    image: "./Bessie.png",
    name: "Bessie Flores",
    admin: 1
  },
  {
    image: "./Cameron.png",
    name: "Cameron Black",
    admin: 2
  },
  {
    image: "./Debra.png",
    name: "Debra Simmons",
    admin: 0
  },
  {
    image: "./Debra.png",
    name: "Debra Simmmons",
    admin: 0
  },
  {
    image: "./Brandor.png",
    name: "Brandon Watsonn",
    admin: 2
  },
  {
    image: "./Brandor.png",
    name: "Brandonn Watson",
    admin: 1
  },
  {
    image: "./Brandor.png",
    name: "Brandon Watson",
    admin: 0
  }
];

export const TABLE_COLUMNS = [
  {
    key: "invoice",
    label: "Invoice"
  },
  {
    key: "date",
    label: "Date"
  },
  {
    key: "period",
    label: "Period"
  },
  {
    key: "products",
    label: "Products"
  },
  {
    key: "price",
    label: "Price"
  }
];

export const TABLE_DATA = [
  {
    invoice: "Invoice n* 180/2019",
    date: "Aug 16th, 2019",
    period: "2019.07.13 - 2019.08.12",
    products: "01+02+03+4xPh",
    price: "$120.00"
  },
  {
    invoice: "Invoice n* 180/2019",
    date: "Aug 16th, 2019",
    period: "2019.07.13 - 2019.08.12",
    products: "01+02+03+4xPh",
    price: "$120.00"
  },
  {
    invoice: "Invoice n* 180/2019",
    date: "Aug 16th, 2019",
    period: "2019.07.13 - 2019.08.12",
    products: "01+02+03+4xPh",
    price: "$120.00"
  }
];

export const INSTAGRAM_PROFILE_IMG = [
  {
    image: "./Rectangle 1.png",
    alt: "Rectangle 1"
  },
  {
    image: "./Rectangle 2.png",
    alt: "Rectangle 2"
  },
  {
    image: "./Rectangle 3.png",
    alt: "Rectangle 3"
  },
  {
    image: "./Rectangle 4.png",
    alt: "Rectangle 4"
  }
];

export const TEAMATE_USERS = [
  {
    image: "./Regina.png",
    text: "Regina Miles"
  },
  {
    image: "./Jerome.png",
    text: "Jerome Rusell"
  }
];
