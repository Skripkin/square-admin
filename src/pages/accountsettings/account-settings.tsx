import React from "react";

import CheckIcon from "@material-ui/icons/Check";

import { Checkbox, Select, Input, SubMenu } from "components";
import {
  Container,
  HeadContent,
  HeadLeft,
  FormBox,
  CheckBottom,
  BottomBox
} from "./styles";
import { COUNTRY, TYPE, CATEGORY, TIPS, SUB_MENU_ITEMS } from "./mock";

const AccountSettings = () => {
  const [checked, setChecked] = React.useState(false);

  return (
    <Container>
      <div className="box">
        <HeadContent>
          <p>Account settings</p>
          <HeadLeft>
            <CheckIcon />
            <span>Save</span>
          </HeadLeft>
        </HeadContent>
        <FormBox>
          <div className="flexbox">
            <Input
              placeholder=""
              title="Venues name"
              value=""
              onChange={() => null}
            />
            <Input
              placeholder=""
              title="Adress"
              value=""
              onChange={() => null}
            />
          </div>
          <div className="flexbox">
            <Select
              title="Country"
              options={COUNTRY}
              value={COUNTRY[0].value}
              onChange={() => null}
            />
            <Input placeholder="" title="City" value="" onChange={() => null} />
          </div>
          <div className="flexbox">
            <Select
              title="Type"
              options={TYPE}
              value={TYPE[0].value}
              onChange={() => null}
            />
            <Select
              title="Category"
              options={CATEGORY}
              value={CATEGORY[0].value}
              onChange={() => null}
            />
          </div>
          <div className="description">
            <Input
              placeholder=""
              title="Description"
              value=""
              onChange={() => null}
            />
          </div>
          <div className="flexbox">
            <Select
              title="Tips/comissions"
              options={TIPS}
              value={TIPS[0].value}
              onChange={() => null}
            />
            <Input
              placeholder=""
              title="Amount"
              value=""
              onChange={() => null}
            />
          </div>
          <span>Details</span>
          <div className="flexbox">
            <Checkbox
              icon="leaf"
              checked={checked}
              name="vegan"
              title="Vegan"
              onChange={(_, checked) => setChecked(checked)}
            />
            <Checkbox
              icon="plant"
              checked={checked}
              name="Gluten free"
              title="Gluten free"
              onChange={(_, checked) => setChecked(checked)}
            />
          </div>
          <div className="flexbox">
            <Checkbox
              icon="sneaker"
              checked={checked}
              name="Casual"
              title="Casual"
              onChange={(_, checked) => setChecked(checked)}
            />
            <Checkbox
              icon="womenshoe"
              checked={checked}
              name="Elegant"
              title="Elegant"
              onChange={(_, checked) => setChecked(checked)}
            />
          </div>
          <BottomBox>
            <span>Available for</span>
            <CheckBottom>
              <Checkbox
                icon="man"
                checked={checked}
                name="Man"
                title="Man"
                onChange={(_, checked) => setChecked(checked)}
              />
              <Checkbox
                icon="woman"
                checked={checked}
                name="Woman"
                title="Woman"
                onChange={(_, checked) => setChecked(checked)}
              />
              <Checkbox
                icon="pepole"
                checked={checked}
                name="All"
                title="All"
                onChange={(_, checked) => setChecked(checked)}
              />
            </CheckBottom>
          </BottomBox>
        </FormBox>
      </div>
      <div>
        <SubMenu links={SUB_MENU_ITEMS} />
      </div>
    </Container>
  );
};

export default AccountSettings;
