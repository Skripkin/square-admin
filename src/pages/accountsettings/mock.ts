import { urls } from "constants/.";

export const SUB_MENU_ITEMS = [
  {
    icon: "user",
    title: "Account Settings",
    link: urls.ACCOUNTSETTINGS,
    isactive: true
  },
  { icon: "image", title: "Images", link: urls.IMAGES },
  { icon: "single-folded-content", title: "Contacts", link: "#" },
  { icon: "setting", title: "Company Details", link: "#" },
  { icon: "credit", title: "Payment Method", link: "#" },
  { icon: "lightning", title: "Subscription", link: urls.SUBSCRIPTIONS },
  { icon: "single-folded-content", title: "Billing", link: urls.BILLING }
];

export const COUNTRY = [
  {
    title: "SomeItems",
    value: "1"
  },
  {
    title: "SomeItems",
    value: "2"
  },
  {
    title: "SomeItems",
    value: "3"
  },
  {
    title: "SomeItems",
    value: "4"
  },
  {
    title: "SomeItems",
    value: "5"
  }
];

export const TYPE = [
  {
    title: "SomeItems",
    value: "1"
  },
  {
    title: "SomeItems",
    value: "2"
  },
  {
    title: "SomeItems",
    value: "3"
  },
  {
    title: "SomeItems",
    value: "4"
  },
  {
    title: "SomeItems",
    value: "5"
  }
];

export const CATEGORY = [
  {
    title: "SomeItems",
    value: "1"
  },
  {
    title: "SomeItems",
    value: "2"
  },
  {
    title: "SomeItems",
    value: "3"
  },
  {
    title: "SomeItems",
    value: "4"
  },
  {
    title: "SomeItems",
    value: "5"
  }
];

export const TIPS = [
  {
    title: "SomeItems",
    value: "1"
  },
  {
    title: "SomeItems",
    value: "2"
  },
  {
    title: "SomeItems",
    value: "3"
  },
  {
    title: "SomeItems",
    value: "4"
  },
  {
    title: "SomeItems",
    value: "5"
  }
];
