import styled from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div`
  position: relative;
  padding: 24px 330px 24px 40px;

  .box {
    max-width: 836px;
  }
`;

export const HeadContent = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 20px 24px;
  border: 1px solid ${colors.gallery};
  box-sizing: border-box;
  border-radius: 12px 12px 0 0;
  background: ${colors.whisper};

  & > p {
    font-weight: 500;
    font-size: 18px;
    line-height: 23px;
    text-transform: capitalize;
    color: ${colors.shipGray};
  }
`;

export const FormBox = styled.div`
  padding: 24px 330px 24px 24px;
  border: 1px solid ${colors.gallery};
  border-radius: 0 0 12px 12px;
  box-sizing: border-box;
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);
  background: ${colors.white};

  .description {
    margin-bottom: 24px;
  }

  .flexbox {
    display: flex;

    & > div {
      min-width: 220px;
      margin-right: 40px;
      margin-bottom: 24px;
    }

    & > button {
      min-width: 220px;
      margin-bottom: 24px;
      margin-right: 40px;
    }
  }

  & > span {
    display: inline-block;
    margin-bottom: 16px;
    font-size: 14px;
    line-height: 18px;
    color: ${colors.darkGrey};
  }
`;

export const HeadLeft = styled.button`
  border: none;
  background: inherit;

  svg {
    vertical-align: text-bottom;
    font-size: 1.2rem;
    color: ${colors.pink};
  }

  span {
    font-size: 14px;
    line-height: 18px;
    color: ${colors.pink};
  }
`;

export const BottomBox = styled.div`
  & > span {
    display: inline-block;
    margin-bottom: 16px;
    font-size: 14px;
    line-height: 18px;
    color: ${colors.darkGrey};
  }
`;

export const CheckBottom = styled.div`
  display: flex;
  justify-content: space-between;

  button {
    min-width: 0;
  }
`;
