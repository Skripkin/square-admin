import React from "react";
import moment from "moment";

import { Members, Status } from "components";

import {
  Container,
  Info,
  Date,
  IconWrapper,
  Count,
  MembersWrapper,
  InfoContent,
  EventIcon
} from "./styles";

interface IProps {
  date: Date;
  members: string[];
  status: "active" | "inactive";
}

const Event = ({ date, members, status }: IProps) => {
  const formattedDate = moment(date).format("ddd, DD");

  return (
    <Container>
      <Info>
        <Date>{formattedDate}</Date>
        <InfoContent>
          <IconWrapper>
            <EventIcon iconName="users" />
          </IconWrapper>
          <Count>{members.length}</Count>
        </InfoContent>
      </Info>
      <MembersWrapper>
        <Members images={members} counter={5} />
      </MembersWrapper>
      <Status isactive={status} />
    </Container>
  );
};

export default Event;
