import styled from "styled-components";

import { colors } from "constants/.";

import Icon from "../icon";

export const Container = styled.div`
  display: flex;
  align-items: center;
  padding: 8px;
  border: 1px solid ${colors.gallery};
  box-sizing: border-box;
  border-radius: 12px;
  background: white;
`;

export const Info = styled.div`
  margin-right: 8px;
  padding: 12px 10px;
  border-radius: 8px;
  background-color: ${colors.royalBlue};
  color: white;
`;

export const InfoContent = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  margin-top: 4px;
`;

export const Date = styled.span`
  font-size: 14px;
  color: white;
`;

export const MembersWrapper = styled.div`
  margin-right: auto;
`;

export const IconWrapper = styled.span`
  margin-right: 5px;
  font-size: 18px;
  opacity: 0.7;
`;

export const Count = styled.span`
  font-weight: 600;
  font-size: 14px;
  opacity: 0.7;
`;

export const EventIcon = styled(Icon)`
  width: 16px;
  height: 16px;
  stroke: ${colors.white};
  fill: ${colors.royalBlue};
`;
