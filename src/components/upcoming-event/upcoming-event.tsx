import React from "react";
import moment from "moment";

import Members, { MembersType } from "components/members/members";

import {
  Container,
  Day,
  Time,
  InfoWrapper,
  Info,
  Label,
  Value,
  SpotMax
} from "./styles";

interface ISpot {
  max: number;
  value: number;
}

interface IProps {
  from: Date;
  to: Date;
  type: string;
  budget?: number;
  spot: ISpot;
  members: MembersType;
}

const UpcomingEvent = ({ from, to, type, budget, spot, members }: IProps) => {
  const day = moment(from).format("ddd, Do MMM");
  const time = `${moment(from).format("hh:ss")} - ${moment(to).format(
    "hh:ss"
  )}`;

  return (
    <Container>
      <Day>{day}</Day>
      <Time>{time}</Time>

      <InfoWrapper>
        <Info>
          <Label>Type:</Label>
          <Value>{type}</Value>
        </Info>

        <Info>
          <Label>Budget:</Label>
          <Value>{!!budget ? `$${budget}` : "-"}</Value>
        </Info>

        <Info>
          <Label>Spots:</Label>
          <Value>
            {spot.value} / <SpotMax>{spot.max}</SpotMax>
          </Value>
        </Info>
      </InfoWrapper>

      <Members {...members} />
    </Container>
  );
};

export default UpcomingEvent;
