import styled from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div``;

export const Day = styled.p`
  margin-bottom: 5px;
  font-size: 15px;
  color: ${colors.cornflowerBlue};
`;

export const Time = styled.p`
  margin-bottom: 16px;
  font-size: 14px;
  color: ${colors.lightGrey};
`;

export const InfoWrapper = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
`;

export const Info = styled.div``;

export const Label = styled.span`
  display: block;
  font-size: 13px;
  color: ${colors.darkGrey};
`;

export const Value = styled(Label)`
  margin-top: 6px;
  margin-bottom: 16px;
  color: ${colors.shipGray};
`;

export const SpotMax = styled.span`
  color: ${colors.lightGrey};
`;
