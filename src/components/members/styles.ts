import styled from "styled-components";

export const Container = styled.div``;

export const ImgConteiner = styled.div`
  display: inline-block;
  position: relative;
  margin-left: -5px;
  width: 30px;
  height: 30px;
  text-align: center;
  border-radius: 50%;
  box-shadow: 0px 0px 0px 2px white;
  color: white;

  &:first-child {
    margin-left: 0;
  }
`;

export const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 50%;
`;

export const Count = styled.span`
  position: absolute;
  border-radius: 50%;
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  background: rgba(0, 0, 0, 0.4);
`;
