import React from "react";

import { Container, Img, ImgConteiner, Count } from "./styles";

export interface MembersType {
  images: string[];
  counter: number;
}

const Members = ({ images, counter }: MembersType) => {
  const displayedImages = images.slice(0, counter);

  return (
    <Container>
      {displayedImages.map((src, index) => (
        <ImgConteiner key={index} style={{ zIndex: index }}>
          <Img src={src} />
          {index === counter - 1 && counter !== images.length && (
            <Count>{`+${images.length + 1 - counter}`}</Count>
          )}
        </ImgConteiner>
      ))}
    </Container>
  );
};

export default Members;
