import React from "react";
import moment from "moment";

import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import CheckIcon from "@material-ui/icons/Check";
import ClearIcon from "@material-ui/icons/Clear";

import {
  Container,
  People,
  Person,
  PeopleNumber,
  TimeBlock,
  InfoBox,
  ProfileIcon,
  PeopleIconWrapper,
  DotSymbol,
  ReservationIcon,
  AvatarButton
} from "./styles";

enum ReservationStatus {
  None,
  Unconfirmed,
  Confirmed
}

interface IReserv {
  image: string;
  name: string;
  admin: ReservationStatus;
}

interface IReservElement {
  reserv: IReserv[];
  from: Date;
  to: Date;
  onClick?: (value: IReserv) => void;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      "& > *": {
        margin: theme.spacing(1)
      }
    },
    bigAvatar: {
      width: 64,
      height: 64
    }
  })
);

const Reservation = ({
  reserv = [],
  from,
  to,
  onClick = () => null
}: IReservElement) => {
  const classes = useStyles("");
  const time = `${moment(from).format("hh:ss")} - ${moment(to).format(
    "hh:ss"
  )}`;
  const personCount = reserv.length;

  return (
    <Container>
      <InfoBox>
        <TimeBlock>{time}</TimeBlock>
        <DotSymbol>&middot;</DotSymbol>
        <PeopleNumber>
          <PeopleIconWrapper>
            <ReservationIcon iconName="users" />
          </PeopleIconWrapper>
          <span>{personCount}</span>
        </PeopleNumber>
      </InfoBox>
      <People>
        {reserv.map(item => (
          <Person key={item.name}>
            <AvatarButton
              className={classes.root + " active"}
              onClick={() => onClick(item)}
            >
              <Avatar
                alt="Error"
                src={item.image}
                className={classes.bigAvatar}
              />
            </AvatarButton>
            <p>{item.name}</p>
            <ProfileIcon>
              {item.admin === 0 && <div />}
              {item.admin === 1 && <CheckIcon className="Check" />}
              {item.admin === 2 && <ClearIcon className="Clear" />}
            </ProfileIcon>
          </Person>
        ))}
      </People>
    </Container>
  );
};

export default Reservation;
