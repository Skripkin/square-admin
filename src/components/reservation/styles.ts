import styled from "styled-components";

import { colors } from "constants/.";

import Icon from "../icon";

export const Container = styled.div`
  padding: 8px 24px;
  border: 1px solid ${colors.gallery};
  box-sizing: border-box;
  border-radius: 12px;
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);
  background: ${colors.white};
  overflow: hidden;
`;

export const People = styled.div`
  display: flex;
  width: 100%;
  overflow-x: auto;
  overflow-y: hidden;
`;

export const Person = styled.button`
  margin: 15px 15px 0;
  border: none;
  background: inherit;
  outline: none;

  &:active {
    .active > div {
      box-shadow: 0 0 0 2px red;
    }
  }

  div {
    justify-content: center;
  }

  p {
    text-align: center;
    white-space: nowrap;
  }
`;

export const InfoBox = styled.div`
  display: flex;
  align-items: center;
`;

export const DotSymbol = styled.span`
  display: inline-block;
  line-height: 0;
  margin-right: 8px;
  margin-left: 8px;
`;

export const PeopleNumber = styled.span`
  display: flex;
  span {
    color: ${colors.lightGrey};
    vertical-align: middle;
  }
`;

export const PeopleIconWrapper = styled.span`
  margin-right: 4px;
  line-height: 0;
  font-size: 16px;
  color: ${colors.lightGrey};
`;

export const TimeBlock = styled.span`
  vertical-align: middle;
  color: ${colors.shipGray};
`;

export const ProfileIcon = styled.div`
  margin-top: 4px;
  text-align: center;

  .Check {
    color: ${colors.pink};
    font-size: 1rem;
  }

  .Clear {
    color: ${colors.lightGrey};
    font-size: 1rem;
  }

  div {
    margin: 1.5px 0;
    width: 1rem;
    height: 1rem;
  }
`;

export const ReservationIcon = styled(Icon)`
  width: 16px;
  height: 16px;
  vertical-align: text-bottom;
  stroke: ${colors.lightGrey};
  fill: ${colors.white};
`;

export const AvatarButton = styled.button``;
