import styled from "styled-components";
import { Link } from "react-router-dom";

import { colors } from "constants/.";

export const Container = styled(Link)`
  display: block;
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);
  border: 1px solid ${colors.gallery};
  border-radius: 12px;
  text-decoration: none;
  color: ${colors.shipGray};
`;

export const ImgConteiner = styled.div`
  width: auto;
  height: 130px;
`;

export const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 8px 8px 0px 0px;
`;

export const TextWrapper = styled.div`
  padding: 12px 18px;
`;

export const Title = styled.div`
  line-height: 20px;
  font-family: CircularStd;
  font-weight: bold;
  font-size: 16px;
`;

export const Counter = styled.div`
  line-height: 18px;
  font-family: CircularStd;
  font-size: 14px;
  opacity: 0.7;
`;
