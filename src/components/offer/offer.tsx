import React from "react";

import {
  Container,
  ImgConteiner,
  TextWrapper,
  Img,
  Counter,
  Title
} from "./styles";

interface IProps {
  images: string;
  title: string;
  link: string;
  creditCounter: number;
}

const Offer = ({ images, title, creditCounter, link }: IProps) => (
  <Container link={link}>
    <ImgConteiner>
      <Img src={images} />
    </ImgConteiner>
    <TextWrapper>
      <Title>{title}</Title>
      <Counter>
        {creditCounter} {creditCounter > 1 ? "credits" : "credit"}
      </Counter>
    </TextWrapper>
  </Container>
);

export default Offer;
