import styled from "styled-components";

import { colors } from "constants/.";

export default styled.span<{ isactive: "active" | "inactive" }>`
  display: inline-block;
  width: 8px;
  height: 8px;
  border-radius: 50%;

  background-color: ${props =>
    props.isactive === "active" ? colors.green : colors.pink};
`;
