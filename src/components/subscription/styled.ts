import styled from "styled-components";

import { colors } from "constants/.";

export const SubContainer = styled.div`
  padding: 24px;
  box-sizing: border-box;
  border: 1px solid ${colors.concrete};
  border-radius: 8px;
  background: ${colors.whisper};
`;

export const TitlePrice = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 15px;
`;

export const StyleTitle = styled.span`
  line-height: 23px;
  font-weight: 500;
  font-size: 18px;
  color: ${colors.shipGray};
`;

export const StylePrice = styled.span`
  font-size: 16px;
  color: ${colors.darkGrey};

  span {
    line-height: 25px;
    font-weight: 500;
    font-size: 20px;
    text-align: right;
    color: ${colors.shipGray};
  }
`;

export const StyleDescription = styled.p`
  line-height: 20px;
  font-size: 16px;
  color: ${colors.darkGrey};
`;
