import React from "react";

import {
  SubContainer,
  TitlePrice,
  StyleTitle,
  StylePrice,
  StyleDescription
} from "./styled";

interface SubItems {
  title: string;
  price: string;
  description: string;
}

const Subscription = ({ title, price, description }: SubItems) => {
  return (
    <SubContainer>
      <TitlePrice>
        <StyleTitle>{title}</StyleTitle>
        <StylePrice>
          <span>{price}</span>/month
        </StylePrice>
      </TitlePrice>
      <StyleDescription>{description}</StyleDescription>
    </SubContainer>
  );
};

export default Subscription;
