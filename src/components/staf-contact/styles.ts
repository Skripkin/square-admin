import styled from "styled-components";

import { colors } from "constants/.";

export const Staf = styled.div`
  margin-right: 15px;
  padding: 0 16px 0 8px;
  border: 1.5px solid ${colors.concrete};
  box-sizing: border-box;
  border-radius: 4px;
  box-shadow: 0px 2px 12px ${colors.concrete};
  background: ${colors.white};
  opacity: 0.6;

  p {
    white-space: nowrap;
  }
`;

export const Contaner = styled.div`
  display: flex;
  justify-content: space-between;
  overflow-x: auto;
  overflow-y: hidden;
  width: 100%;
`;

export const Input = styled.input`
  position: absolute;
  transform: scale(0.01);
  opacity: 0;

  &:checked + ${Staf} {
    border: 1.5px solid ${colors.pink};
    opacity: 1;
  }
`;
