import React from "react";

import SelectedOffer, { IOffer } from "../selected-offer/selected-offer";

import { Contaner, Staf, Input } from "./styles";

interface IProps {
  offers: IOffer[];
}

const StafContact = ({ offers }: IProps) => {
  return (
    <Contaner>
      {offers.map((item, i) => (
        <label key={item.text} htmlFor={`Stafcheck${i}`}>
          <Input type="checkbox" id={`Stafcheck${i}`} />
          <Staf>
            <SelectedOffer image={item.image} text={item.text} />
          </Staf>
        </label>
      ))}
    </Contaner>
  );
};

export default StafContact;
