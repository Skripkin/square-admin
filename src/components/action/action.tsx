import React from "react";
import moment from "moment";

import Avatar from "@material-ui/core/Avatar";
import CloseIcon from "@material-ui/icons/Close";
import CheckIcon from "@material-ui/icons/Check";

import {
  ActionContainer,
  ImageBlock,
  UserInfo,
  StyleText,
  TextBlock,
  IconBlock,
  ActionButton
} from "./styles";

export interface IProps {
  image: string;
  avatar: string;
  userinicial: string;
  date: Date;
  onAccept: () => void;
  onCancel: () => void;
}

const Action = ({
  image,
  avatar,
  userinicial,
  date,
  onAccept,
  onCancel
}: IProps) => {
  const formattedDate = moment(date).format("DD.MM.YYYY");

  return (
    <ActionContainer>
      <ImageBlock>
        <img src={image} alt="Error" />
        <IconBlock>
          <ActionButton onClick={onAccept}>
            <CloseIcon />
          </ActionButton>
          <ActionButton onClick={onCancel}>
            <CheckIcon />
          </ActionButton>
        </IconBlock>
      </ImageBlock>
      <UserInfo>
        <Avatar alt="Error" src={avatar} />
        <TextBlock>
          <StyleText>{userinicial}</StyleText>
          <StyleText>{formattedDate}</StyleText>
        </TextBlock>
      </UserInfo>
    </ActionContainer>
  );
};

export default Action;
