import styled from "styled-components";

import { colors } from "constants/.";

export const ActionContainer = styled.div`
  border: 1px solid ${colors.gallery};
  border-radius: 5%;
  box-sizing: border-box;
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);
  background: ${colors.white};
`;

export const IconBlock = styled.div`
  display: none;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  line-height: 0;

  & > * {
    margin: 0 8px;
    outline: 0;
    color: ${colors.lightGrey};

    .MuiSvgIcon-root {
      vertical-align: middle;
    }

    &:hover {
      color: ${colors.shipGray};
    }
  }
`;

export const ImageBlock = styled.div`
  display: flex;
  position: relative;

  &:hover ${IconBlock} {
    display: block;
  }

  img {
    width: 100%;
  }
`;

export const ActionButton = styled.button`
  width: 40px;
  height: 40px;
  border: 1px solid ${colors.gallery};
  box-sizing: border-box;
  border-radius: 50%;
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);
  background: ${colors.white};
  cursor: pointer;
`;

export const UserInfo = styled.div`
  display: flex;
  padding: 22px 15px;
`;

export const TextBlock = styled.div`
  margin-left: 8px;
`;

export const StyleText = styled.p`
  &:first-child {
    margin-bottom: 8px;
    font-size: 16px;
    line-height: 20px;
    color: ${colors.shipGray};
  }

  &:last-child {
    font-size: 14px;
    line-height: 16px;
    letter-spacing: 0.1px;
    color: ${colors.lightGrey};
  }
`;
