import styled from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div`
  width: 275px;
  position: relative;
  background-color: ${colors.white};
  box-shadow: -1px 0 0 ${colors.gallery};
  padding-bottom: 64px;
  overflow-y: auto;
`;

export const ProfileHeader = styled.div`
  position: relative;
`;

export const ProfilePhoto = styled.img`
  width: 100%;
`;

export const ProfileHeading = styled.div`
  position: absolute;
  top: 16px;
  left: 16px;
  right: 16px;
  display: flex;
  justify-content: space-between;

  svg {
    color: ${colors.white};
  }

  p {
    width: 54px;
    height: 24px;
    padding: 2.5px 0;
    background-color: ${colors.white};
    border-radius: 24px;
    text-align: center;
    color: ${colors.shipGray};
    span {
      line-height: 18px;
      font-weight: 500;
      font-size: 14px;
    }
  }
`;

export const NameContainer = styled.div`
  margin: 16px 0;
  text-align: center;
  text-transform: capitalize;
`;

export const NameText = styled.p`
  font-weight: bold;
  font-size: 18px;
  line-height: 23px;
  color: ${colors.shipGray};
`;

export const CityText = styled.p`
  margin-top: 8px;
  font-size: 16px;
  line-height: 20px;
  color: ${colors.lightGrey};
`;

export const ProfileInfo = styled.div`
  margin: 0 24px;
  padding: 6px 0;
  border-top: 1px solid ${colors.gallery};
  border-bottom: 1px solid ${colors.gallery};
`;

export const Info = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const Label = styled.p`
  margin: 13px 0;
  line-height: 19px;
  font-size: 15px;
  color: ${colors.darkGrey};
`;

export const Value = styled(Label)`
  color: ${colors.black};
`;

export const InstagramProfileWrapper = styled.div`
  margin: 0 24px;
`;

export const InstagramProfileHeading = styled.p`
  margin: 16px 0 21px 0;
  font-weight: 500;
  font-size: 18px;
  line-height: 23px;
  text-transform: capitalize;
`;

export const InstagramImages = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  overflow-y: auto;
  overflow-x: hidden;
`;

export const InstagramProfileImg = styled.div`
  width: calc(50% - 9px);
  margin-bottom: 10px;
  img {
    width: 100%;
    line-height: 0;
  }
`;

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: space-between;
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  padding: 12px 24px;
  height: 64px;
  box-sizing: border-box;
  box-shadow: 0px -1px 0px ${colors.concrete};
  background-color: ${colors.white};

  svg {
    vertical-align: sub;
    font-size: 1rem;
    margin-right: 5px;
  }

  button:first-child {
    background-color: ${colors.whisper};
    color: ${colors.shipGray};
  }
`;
