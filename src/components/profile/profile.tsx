import React from "react";

import * as Styled from "./styles";

import { ArrowBack, Close, Check } from "@material-ui/icons";
import Button from "../button";

interface ProfileInstagram {
  image: string;
  alt: string;
}

interface IProperty {
  label: string;
  value: string | number;
}

interface IProfileSettings {
  image?: any;
  alt?: string;
  cost: number;
  name: string;
  city?: string;
  properties: IProperty[];
  instagramProfile?: ProfileInstagram[];
}

const Profile = function ProfileDrawer({
  image,
  alt,
  cost,
  name,
  city,
  properties,
  instagramProfile = []
}: IProfileSettings) {
  return (
    <Styled.Container>
      <Styled.ProfileHeader>
        <Styled.ProfileHeading>
          <ArrowBack />
          <p>
            <span>${cost}</span>
          </p>
        </Styled.ProfileHeading>
        <Styled.ProfilePhoto src={image} alt={alt} />
      </Styled.ProfileHeader>
      <Styled.NameContainer>
        <Styled.NameText>{name}</Styled.NameText>
        <Styled.CityText>{city}</Styled.CityText>
      </Styled.NameContainer>
      {!!properties.length && (
        <Styled.ProfileInfo>
          {properties.map(({ label, value }) => (
            <Styled.Info key={label}>
              <Styled.Label>{label}</Styled.Label>
              <Styled.Value>{value}</Styled.Value>
            </Styled.Info>
          ))}
        </Styled.ProfileInfo>
      )}
      <Styled.InstagramProfileWrapper>
        <Styled.InstagramProfileHeading>
          Instagram profile
        </Styled.InstagramProfileHeading>
        <Styled.InstagramImages>
          {instagramProfile.map(item => (
            <Styled.InstagramProfileImg key={item.alt}>
              <img src={item.image} alt={item.alt} />
            </Styled.InstagramProfileImg>
          ))}
        </Styled.InstagramImages>
      </Styled.InstagramProfileWrapper>
      <Styled.ButtonContainer>
        <Button>
          <span>
            <Close />
            Decline
          </span>
        </Button>
        <Button>
          <span>
            <Check />
            Accept
          </span>
        </Button>
      </Styled.ButtonContainer>
    </Styled.Container>
  );
};

export default Profile;
