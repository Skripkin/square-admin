import React from "react";
import { withRouter } from "react-router-dom";
import _get from "lodash/get";

import { Container, StyledModalBox, Content, ModalStyle } from "./styles";

interface IProps {
  type?: "wide" | "modal";
  path: string;
  parentPath: string;
  Component: any;
}

const ModalBox = ({ type = "wide", path, parentPath, Component }: IProps) => {
  return (
    <Container>
      <StyledModalBox
        path={path}
        parentPath={parentPath}
        chartModal
        variant="wide"
        outDelay={300}
        component={(...props) => {
          const currPath = _get(props, "0.match.path", "");

          return (
            <Content type={type}>
              {currPath === path && <ModalStyle type={type} />}
              <Component {...props} />
            </Content>
          );
        }}
      />
    </Container>
  );
};

export default withRouter(ModalBox);
