import { ModalRoute } from "react-router-modal";
import styled, { css, createGlobalStyle } from "styled-components";
import "react-router-modal/css/react-router-modal.css";

interface IModalType {
  type: "wide" | "modal";
}

export const ModalStyle = createGlobalStyle<IModalType>`
  .react-router-modal {
    &__container {
      top: 0;
      right: 0;
      bottom: 0;
      width: 275px;
    }

    &__backdrop {
      display: none;
    }
  }

  ${({ type }) =>
    type === "modal" &&
    css`
      .react-router-modal {
        &__container {
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          z-index: 1001 !important;
          margin: auto;
          width: 456px;
        }

        &__backdrop {
          display: block;
        }
      }
    `}
`;

export const StyledModalBox = styled(ModalRoute)`
  position: relative;
  padding: 12px 0;
  width: 100%;
  height: 100vh;
  box-sizing: border-box;
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);
`;

export const Content = styled.div<IModalType>`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  padding-top: 72px;
  width: 275px;
  overflow-y: auto;
  box-sizing: border-box;
  background-color: white;

  ${({ type }) =>
    type === "modal" &&
    css`
      top: 50px;
      bottom: 50px;
      padding-top: 0;
      width: 456px !important;
      border-radius: 12px;
    `}
`;

export const Container = styled.div`
  background-color: red;
`;
