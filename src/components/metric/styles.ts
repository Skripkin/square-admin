import styled from "styled-components";

import { colors } from "../../constants";

import Icon from "../icon";

export const MetricCard = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  background-color: ${colors.white};
  border: 1px solid ${colors.gallery};
  box-sizing: border-box;
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);
  border-radius: 12px;
`;

export const MetricContent = styled.div`
  display: flex;
  flex-direction: column;
  margin: 16px 0 12px 0;
  text-align: center;

  & {
    text-transform: capitalize;
  }
`;

export const MetricIcon = styled.div`
  align-self: center;
  padding: 10px;
  border-radius: 50%;
  color: ${colors.lightGrey};
  background-color: ${colors.whisper};
  border: 1px solid ${colors.gallery};
`;

export const MetricNums = styled.p`
  margin-top: 13px;
  font-weight: 500;
  font-size: 30px;
  line-height: 38px;
  color: ${colors.shipGray};
`;

export const MetricIconStrelka = styled(Icon)`
  display: none;
  width: 10px;
  height: 5px;
  margin-left: 4px;
  stroke: ${colors.darkGrey};
`;

export const MetricTextButton = styled.button`
  display: flex;
  align-items: center;
  margin: 5px auto;

  &:hover ${MetricIconStrelka} {
    display: inline-block;
  }

  &:active ${MetricIconStrelka} {
    display: inline-block;
  }
`;

export const MetricText = styled.p`
  border: none;
  background: none;
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 19px;
  color: ${colors.darkGrey};
`;

export const MetricProgress = styled.div`
  margin-top: 13px;

  .MuiSvgIcon-root {
    width: 18px;
    margin-bottom: -6px;
  }

  & {
    font-size: 15px;
    line-height: 19px;
  }
`;

export const MetricProcent = styled.span`
  margin-left: 3px;
`;

export const MetricIconic = styled(Icon)`
  width: 20px;
  height: 20px;
  stroke: ${colors.lightGrey};
  fill: ${colors.whisper};
`;
