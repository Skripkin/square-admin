import React from "react";

import * as Styled from "./styles";
import { colors } from "../../constants";

import Menu, { MenuProps } from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { ArrowDownward, ArrowUpward } from "@material-ui/icons";

import {
  createStyles,
  makeStyles,
  Theme,
  withStyles
} from "@material-ui/core/styles";

export interface MetricType {
  icon: any;
  count: number | string;
  text: string;
  cost: number;
  sign: "+" | "-";
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    menu: {
      boxShadow: "0px 2px 12px rgba(0, 0, 0, 0.1)",
      borderRadius: "8px",
      backgroundColor: "white"
    }
  })
);

const StyledMenu = withStyles({
  paper: {
    boxShadow: "0px 2px 12px rgba(0, 0, 0, 0.1)",
    borderRadius: "8px"
  }
})((props: MenuProps) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "right"
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "right"
    }}
    {...props}
  />
));

const Metric = ({ icon, count, text, cost, sign }: MetricType) => {
  const classes = useStyles("");
  const progressColor = sign === "+" ? colors.green : colors.carnation;
  const Arrow = sign === "+" ? ArrowUpward : ArrowDownward;

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Styled.MetricCard>
      <Styled.MetricContent>
        {!!icon && (
          <Styled.MetricIcon>
            <Styled.MetricIconic iconName={icon} />
          </Styled.MetricIcon>
        )}
        <Styled.MetricNums>{count}</Styled.MetricNums>
        <Styled.MetricTextButton onClick={handleClick}>
          <Styled.MetricText>{text}</Styled.MetricText>
          <Styled.MetricIconStrelka iconName="vector" />
        </Styled.MetricTextButton>
        <StyledMenu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
          PopoverClasses={{
            paper: classes.menu
          }}
        >
          <MenuItem>
            <p>Instagram stories</p>
          </MenuItem>
          <MenuItem>
            <p>Facebook</p>
          </MenuItem>
          <MenuItem>
            <p>Tripadvisor</p>
          </MenuItem>
          <MenuItem>
            <p>Google maps</p>
          </MenuItem>
        </StyledMenu>
        <Styled.MetricProgress style={{ color: progressColor }}>
          <Arrow />
          <Styled.MetricProcent>{sign + cost}%</Styled.MetricProcent>
        </Styled.MetricProgress>
      </Styled.MetricContent>
    </Styled.MetricCard>
  );
};

export default Metric;
