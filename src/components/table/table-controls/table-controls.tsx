import React from "react";

import FormControl from "@material-ui/core/FormControl";
import ArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import ArrowRight from "@material-ui/icons/KeyboardArrowRight";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

import {
  Container,
  Controls,
  Count,
  Title,
  SwitchButton,
  SelectWrapper,
  SelectText
} from "./styles";

const TableHeader = ({ title, meta, onQueryChange }) => {
  const { activePage = 1, totalItems = 0, perPage = 20 } = meta;
  const [age, setAge] = React.useState(10);

  const handleChange = event => {
    const { value: perPage } = event.target;
    const newMeta = { ...meta, perPage };

    setAge(perPage);
    onQueryChange(newMeta);
  };

  const handleGoBack = () => {
    const newMeta = { ...meta, activePage: activePage - 1 };

    onQueryChange(newMeta);
  };

  const handleGoNext = () => {
    const newMeta = { ...meta, activePage: activePage + 1 };

    onQueryChange(newMeta);
  };

  return (
    <Container>
      <Controls>
        <Title>{title}</Title>

        <Count>
          {1 + activePage * perPage - perPage}-{activePage * perPage} of{" "}
          {totalItems}
        </Count>

        <SwitchButton
          onClick={handleGoBack}
          isactive={activePage === 1 ? "false" : "true"}
        >
          <ArrowLeft fontSize="inherit" />
        </SwitchButton>
        <SwitchButton
          onClick={handleGoNext}
          isactive={activePage === totalItems / perPage ? "false" : "true"}
        >
          <ArrowRight fontSize="inherit" />
        </SwitchButton>

        <SelectWrapper>
          <SelectText>Show:</SelectText>
          <FormControl variant="outlined">
            <Select
              labelId="demo-simple-select-outlined-label"
              id="demo-simple-select-outlined"
              value={age}
              onChange={handleChange}
            >
              <MenuItem value={10}>10</MenuItem>
              <MenuItem value={15}>15</MenuItem>
              <MenuItem value={20}>20</MenuItem>
            </Select>
          </FormControl>
        </SelectWrapper>
      </Controls>
    </Container>
  );
};

export default TableHeader;
