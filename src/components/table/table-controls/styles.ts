import styled, { css } from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div`
  display: flex;
  align-items: center;
`;

export const Controls = styled.div`
  display: flex;
  align-items: center;
  padding: 21px 24px;
  width: 100%;
  border-radius: 12px 12px 0 0;
  background-color: ${colors.whisper};
`;

export const Count = styled.span`
  display: inline-block;
  margin-right: 16px;
  font-size: 14px;
  color: ${colors.lightGrey};
`;

export const Title = styled.h3`
  margin-right: auto;
  font-size: 18px;
  color: ${colors.shipGray};
`;

export const SwitchButton = styled.button<{ isactive?: "true" | "false" }>`
  line-height: 0;
  font-size: 18px;
  border: none;
  outline: none;
  background-color: transparent;
  color: ${colors.shipGray};
  cursor: pointer;

  ${props =>
    props.isactive === "false" &&
    css`
      pointer-events: none;
      color: ${colors.lightGrey};
    `}
`;

export const SelectWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-left: 24px !important;
  padding: 8px 16px;
  width: 118px;
  height: 34px;
  box-sizing: border-box;
  border-radius: 20px;
  background-color: white;

  .MuiSelect-root {
    padding: 0;
    width: 40px;
  }

  .MuiSelect-select:focus {
    background-color: transparent;
  }

  svg {
    right: -8px;
  }

  fieldset {
    padding: 0;
    border: none;
  }
`;

export const SelectText = styled.span`
  display: inline-block;
  margin-right: 8px;
  font-size: 14px;
  color: ${colors.lightGrey};
`;
