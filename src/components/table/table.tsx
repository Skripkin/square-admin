import React from "react";

import { Container, StyledTable } from "./styles";
import TableControls from "./table-controls";
import TableHeader from "./table-header";
import TableBody from "./table-body";

interface IMeta {
  activePage?: number;
  totalItems?: number;
  perPage?: number;
}

export interface ITableColumns {
  key: string;
  label?: string;
  width?: number;
  render?: (item?: any) => void;
}

interface IProps {
  title: string;
  data: any;
  columns: ITableColumns[];
  meta: IMeta;
  withoutControls?: boolean;
  onQueryChange: (meta: IMeta) => void;
}

const Table = ({
  title,
  data,
  columns,
  meta,
  onQueryChange,
  withoutControls = false
}: IProps) => {
  return (
    <Container>
      {!withoutControls && (
        <TableControls
          meta={meta}
          title={title}
          onQueryChange={onQueryChange}
        />
      )}
      <StyledTable>
        <TableHeader columns={columns} />
        <TableBody data={data} columns={columns} />
      </StyledTable>
    </Container>
  );
};

export default Table;
