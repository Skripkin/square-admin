import styled from "styled-components";

import { colors } from "constants/.";

export const Tbody = styled.tbody``;

export const Tr = styled.tr`
  box-shadow: 0px 1px 0px #f2f2f2;
`;

export const Td = styled.td`
  padding: 26px 24px;
  font-family: CircularStd;
  font-size: 15px;
  text-align: left;
  color: ${colors.shipGray};
`;
