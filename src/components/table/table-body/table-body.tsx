import React from "react";
import _get from "lodash/get";

import { Tbody, Tr, Td } from "./styles";

interface IProps {
  data: any[];
  columns: any[];
}

const TableBody = ({ data, columns }: IProps) => {
  return (
    <Tbody>
      {data.map((rowItem: any, rowIndex: number) => (
        <Tr key={rowIndex}>
          {columns.map((colItem: any, colIndex: number) => (
            <Td key={colIndex}>
              {colItem.render
                ? colItem.render(rowItem)
                : _get(rowItem, colItem.key, "")}
            </Td>
          ))}
        </Tr>
      ))}
    </Tbody>
  );
};

export default TableBody;
