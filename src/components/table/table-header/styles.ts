import styled from "styled-components";

import { colors } from "constants/.";

export const Thead = styled.thead`
  border-top: 1px solid ${colors.gallery};
  border-bottom: 1px solid ${colors.gallery};
  background-color: white;
`;

export const Th = styled.th`
  padding: 16px 24px;
  font-family: CircularStd;
  font-weight: 500;
  text-align: left;
  color: ${colors.lightGrey};
`;
