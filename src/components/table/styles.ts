import styled from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div`
  border: 1px solid ${colors.gallery};
  border-radius: 12px;
  background-color: white;
`;

export const StyledTable = styled.table`
  width: 100%;
  border-spacing: 0;
  border-collapse: collapse;
  border-radius: 12px;
`;
