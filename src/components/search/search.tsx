import React from "react";

import Close from "@material-ui/icons/Close";

import _debounce from "lodash/debounce";

import { Container, Input, CloseButton, SearchIcon } from "./styles";

interface IProps {
  onSubmit: (value: string) => void;
}

const Search = ({ onSubmit }: IProps) => {
  const [value, setValue] = React.useState("");

  const handleSubmit = _debounce(value => {
    onSubmit(value);
  }, 300);

  const handleChange = e => {
    const { value } = e.target;

    setValue(value);
    handleSubmit(value);
  };

  const handleClear = () => {
    setValue("");
  };

  return (
    <Container>
      <label htmlFor="search">
        <Input
          onChange={handleChange}
          value={value}
          id="search"
          placeholder="Search"
        />
        <SearchIcon iconName="search" />
      </label>
      {!!value.length && (
        <CloseButton onClick={handleClear}>
          <Close fontSize="inherit" />
        </CloseButton>
      )}
    </Container>
  );
};

export default Search;
