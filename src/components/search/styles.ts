import styled from "styled-components";

import { colors } from "constants/.";

import Icon from "../icon";

export const Container = styled.div`
  position: relative;
`;

export const Input = styled.input`
  padding: 0 32px 0 38px;
  width: 360px;
  height: 40px;
  line-height: 40px;
  box-sizing: border-box;
  border-radius: 20px;
  border: 1px solid ${colors.gallery};
  font-size: 15px;
  outline: none;
  background-color: ${colors.whisper};

  &::placeholder {
    font-size: 15px;
    line-height: 19px;
    color: ${colors.lightGrey};
  }

  &:focus {
    box-shadow: 0 0 3pt 2pt ${colors.whisper};
  }
`;

export const CloseButton = styled.button`
  display: inline-block;
  position: absolute;
  top: 4px;
  right: 4px;
  padding: 8px;
  border: none;
  font-size: 16px !important;
  background-color: transparent;
  outline: none;
  color: black;
  cursor: pointer;
`;

export const SearchIcon = styled(Icon)`
  position: absolute;
  top: 10px;
  left: 15px;
  width: 20px;
  height: 20px;
  stroke: ${colors.lightGrey};
  fill: ${colors.white};
`;
