import styled from "styled-components";

export const Container = styled.div`
  columns: 240px 4;
  column-gap: 20px;

  & > div {
    display: inline-block;
    margin-bottom: 20px;
  }
`;
