import React from "react";

import Action, { IProps as IAction } from "../action/action";
import { Container } from "./styled";

type IExtendedAction = Omit<IAction, "onAccept" | "onCancel">;

interface IProps {
  actions: IExtendedAction[];
  onAccept: (i: number) => void;
  onCancel: (i: number) => void;
}

const Actions = ({ actions, onAccept, onCancel }: IProps) => {
  return (
    <Container>
      {actions.map((item, i) => (
        <Action
          key={item.userinicial}
          {...item}
          onAccept={() => onAccept(i)}
          onCancel={() => onCancel(i)}
        />
      ))}
    </Container>
  );
};

export default Actions;
