import React from "react";

import { Thead, Th } from "./styles";

const TableHeader = ({ columns }) => {
  return (
    <Thead>
      <tr>
        {columns.map((column: any, index: number) => (
          <Th style={column.width && { flex: column.width }} key={index}>
            {column.label ? column.label : column.render()}
          </Th>
        ))}
      </tr>
    </Thead>
  );
};

export default TableHeader;
