import React from "react";

import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

import { Container, Title, SelectWrapper, SelectText } from "./styles";

const TableHeader = ({ title, meta, onQueryChange }) => {
  const [age, setAge] = React.useState(10);

  const handleChange = event => {
    const { value: perPage } = event.target;
    const newMeta = { ...meta, perPage };

    setAge(perPage);
    onQueryChange(newMeta);
  };

  return (
    <Container>
      <Title>{title}</Title>

      <SelectWrapper>
        <SelectText>Show:</SelectText>
        <FormControl variant="outlined">
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={age}
            onChange={handleChange}
          >
            <MenuItem value={10}>10</MenuItem>
            <MenuItem value={15}>15</MenuItem>
            <MenuItem value={20}>20</MenuItem>
          </Select>
        </FormControl>
      </SelectWrapper>
    </Container>
  );
};

export default TableHeader;
