import styled from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div`
  display: flex;
  align-items: center;
  padding: 21px 0;
  width: 100%;
  background-color: ${colors.whisper};
`;

export const Title = styled.h3`
  margin-right: auto;
  font-size: 18px;
  color: ${colors.shipGray};
`;

export const SelectWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-left: 24px !important;
  padding: 8px 16px;
  width: 118px;
  height: 34px;
  box-sizing: border-box;
  border-radius: 20px;
  background-color: white;

  .MuiSelect-root {
    padding: 0;
    width: 40px;
  }

  .MuiSelect-select:focus {
    background-color: transparent;
  }

  svg {
    right: -8px;
  }

  fieldset {
    padding: 0;
    border: none;
  }
`;

export const SelectText = styled.span`
  display: inline-block;
  margin-right: 8px;
  font-size: 14px;
  color: ${colors.lightGrey};
`;
