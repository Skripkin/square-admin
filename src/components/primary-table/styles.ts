import styled from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div`
  background-color: white;
`;

export const StyledTable = styled.table`
  width: 100%;
  border-spacing: 0;
  border-collapse: collapse;
  border-radius: 12px;
  border: 1px solid ${colors.gallery};
  background-color: white;
`;
