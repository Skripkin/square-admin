import React from "react";

import { Container, StyledTable } from "./styles";
import TableControls from "./table-controls";
import TableHeader from "./table-header";
import TableBody from "./table-body";
import Pagination from "./pagination";

interface IMeta {
  activePage?: number;
  totalItems?: number;
  perPage?: number;
}

export interface ITableColumns {
  key: string;
  label?: string;
  width?: number;
  render?: (item?: any) => void;
}

interface IProps {
  title: string;
  data: any;
  columns: ITableColumns[];
  meta: IMeta;
  onQueryChange: (meta: IMeta) => void;
}

const PrimaryTable = ({
  title,
  data,
  columns,
  meta,
  onQueryChange
}: IProps) => {
  return (
    <Container>
      <TableControls meta={meta} title={title} onQueryChange={onQueryChange} />
      <StyledTable>
        <TableHeader columns={columns} />
        <TableBody data={data} columns={columns} />
      </StyledTable>
      <Pagination meta={meta} onQueryChange={onQueryChange} />
    </Container>
  );
};

export default PrimaryTable;
