import React from "react";

import ArrowBack from "@material-ui/icons/KeyboardArrowLeft";
import ArrowNext from "@material-ui/icons/KeyboardArrowRight";

import { Container, ArrowButton, NumberButton } from "./styles";
import { generatePagination } from "./helpers";

const Pagination = ({ meta, onQueryChange }) => {
  const { activePage = 1, totalItems = 0, perPage = 20 } = meta;

  const pagination = generatePagination(activePage, totalItems, perPage);

  const handleActivePageChange = newActivePage => {
    const newMeta = { ...meta, activePage: newActivePage };

    onQueryChange(newMeta);
  };

  return (
    <Container>
      {!!pagination.length ? (
        <>
          <ArrowButton
            isactive={activePage !== 1 ? "false" : "true"}
            onClick={() => handleActivePageChange(activePage - 1)}
          >
            <ArrowBack fontSize="inherit" />
          </ArrowButton>

          {pagination.map(item => (
            <NumberButton
              key={item}
              isactive={activePage === item ? "true" : "false"}
              onClick={() => handleActivePageChange(item)}
            >
              {item}
            </NumberButton>
          ))}

          <ArrowButton onClick={() => handleActivePageChange(activePage + 1)}>
            <ArrowNext fontSize="inherit" />
          </ArrowButton>
        </>
      ) : null}
    </Container>
  );
};

export default Pagination;
