import styled, { css } from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding-top: 20px;
  padding-bottom: 20px;
  background-color: ${colors.whisper};
`;

const Button = styled.button`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  line-height: 0;
  margin-right: 32px;
  border: 1px solid ${colors.gallery};
  border-radius: 50%;
  outline: none;
  background-color: transparent;
  cursor: pointer;

  &:last-child {
    margin-right: 0;
  }
`;

export const ArrowButton = styled(Button)<{ isactive?: "true" | "false" }>`
  width: 32px;
  height: 32px;
  box-sizing: border-box;
  border-radius: 50%;
  font-size: 24px;
  background-color: white;
  color: ${colors.pink};

  ${props =>
    props.isactive !== "true" &&
    css`
      opacity: 0.8;
      pointer-events: none;
    `}
`;

export const NumberButton = styled(Button)<{ isactive?: "true" | "false" }>`
  width: 18px;
  height: 24px;
  border: none;
  font-size: 15px;
  color: ${colors.lightGrey};

  ${props =>
    props.isactive !== "true" &&
    css`
      color: ${colors.shipGray};
      pointer-events: none;
    `}
`;
