const generatePrevItems = activePage => {
  const items = [];

  for (let i = activePage - 1; i > 0 && items.length < 3; i--) {
    items.push(i);
  }

  return items;
};

const generateAfterItems = (activePage, totalPages) => {
  const items = [];

  for (let i = activePage + 1; i < totalPages && items.length < 3; i++) {
    items.push(i);
  }

  return items;
};

export const generatePagination = (activePage, totalItems, perPage) => {
  const totalPages = totalItems / perPage;
  const prevPages = generatePrevItems(activePage);
  const afterPages = generateAfterItems(activePage, totalPages);

  return [...prevPages, activePage, ...afterPages];
};
