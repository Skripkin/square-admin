import styled from "styled-components";

import { colors } from "constants/.";

export const Tbody = styled.tbody``;

export const Tr = styled.tr``;

export const Td = styled.td`
  padding: 26px 24px;
  font-family: CircularStd;
  font-size: 15px;
  text-align: left;
  color: ${colors.shipGray};
`;
