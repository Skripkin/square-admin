import styled, { css } from "styled-components";

import { colors } from "constants/.";

export const ToggleButton = styled.button`
  display: none;
  position: relative;
  line-height: 0;
  border: none;
  background: none;
  outline: none;
  background-color: rgba(0, 0, 0, 0.4);
  cursor: pointer;
`;

export const HomeIconContainer = styled.span`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 12px;
  right: 12px;
  width: 30px;
  height: 30px;
  line-height: 0;
  box-sizing: border-box;
  border-radius: 50%;
  background-color: rgba(255, 255, 255, 0.7);
`;

export const Action = styled.button`
  width: 100%;
  padding: 4px 10px;
  background-color: transparent;
  border: none;
  font-size: 15px;
  text-align: left;
  cursor: pointer;

  &:hover {
    background-color: ${colors.concrete};
  }
`;

export const Container = styled.div<{ popup: boolean }>`
  display: flex;
  position: relative;
  margin-right: 24px;
  margin-bottom: 22px;
  max-height: 176px;
  border-radius: 2px;

  ${props =>
    props.popup &&
    css`
      ${ToggleButton} {
        display: block;
      }
    `}

  &:hover ${ToggleButton} {
    display: block;
  }

  &:last-child {
    margin-right: 0;
  }

  img {
    width: 100%;
    height: auto;
  }
`;

export const PopupPage = styled.div`
  position: absolute;
  top: 30px;
  left: 15px;
  padding: 10px 0;
  width: 160px;

  border-radius: 4px;
  box-shadow: 0px 2px 12px rgba(0, 0, 0, 0.1);
  text-align: left;
  background: ${colors.white};

  p {
    font-size: 15px;
    line-height: 19px;
    color: ${colors.darkGrey};
    margin: 10px 0;
  }
`;

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  z-index: 1;
  padding: 12px;

  .dot {
    vertical-align: super;
    color: ${colors.white};
  }
`;
