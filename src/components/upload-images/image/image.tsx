import React from "react";

import HomeIcon from "@material-ui/icons/Home";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

import {
  Container,
  HomeIconContainer,
  ToggleButton,
  ButtonContainer,
  PopupPage,
  Action
} from "./styles";

interface IAction {
  label: string;
  callback: (image) => void;
}

interface IProps {
  main?: boolean;
  src: string;
  actions: IAction[];
}

const Image = ({ main = false, src, actions }: IProps) => {
  const [popup, setPopup] = React.useState<boolean>(false);

  return (
    <Container popup={popup}>
      {main && (
        <HomeIconContainer>
          <HomeIcon fontSize="inherit" />
        </HomeIconContainer>
      )}

      <img src={src} alt="Error" />

      <ClickAwayListener onClickAway={() => popup && setPopup(false)}>
        <ButtonContainer>
          <ToggleButton onClick={() => setPopup(true)}>
            <MoreHorizIcon className="dot" />
          </ToggleButton>
          {!!popup && (
            <PopupPage>
              {actions.map(({ label, callback }) => (
                <Action
                  key={label}
                  onClick={() => {
                    callback({ main, src });
                    setPopup(false);
                  }}
                >
                  {label}
                </Action>
              ))}
            </PopupPage>
          )}
        </ButtonContainer>
      </ClickAwayListener>
    </Container>
  );
};

export default Image;
