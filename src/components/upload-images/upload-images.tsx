import React from "react";

import { Container, ChooseImageContainer, Input, Plus } from "./styles";
import Image from "./image";

interface IImage {
  main?: boolean;
  src: string;
}

interface IProps {
  maxLength: number; // Максимальное количество картинок, возможных для загрузки
  images: IImage[];
  onChange: (images: IImage[]) => void;
}

class UploadImages extends React.Component<IProps> {
  setPopup = popup => {
    this.setState({ popup });
  };

  changeMainImage = (image: IImage) => {
    const { images, onChange } = this.props;

    const changedImages = images.map(img =>
      img.src === image.src ? { ...img, main: true } : { src: img.src }
    );

    onChange(changedImages);
  };

  deleteImage = (image: IImage) => {
    const { images, onChange } = this.props;

    const changedImages = images.filter(img => img.src !== image.src);

    onChange(changedImages);
  };

  uploadAnotherImage = (image: IImage) => {
    const { images, onChange } = this.props;
    const fileSelector = document.createElement("input");

    fileSelector.setAttribute("type", "file");
    fileSelector.addEventListener("change", (e: any) => {
      const [file] = e.target.files;

      const newImages = images.map(img =>
        img.src === image.src
          ? { ...image, src: URL.createObjectURL(file) }
          : img
      );

      onChange(newImages);
    });

    fileSelector.click();
  };

  handleChange = e => {
    const { images, onChange } = this.props;
    const [file] = e.target.files;

    const newImages = [...images, { src: URL.createObjectURL(file) }];

    onChange(newImages);
  };

  render() {
    const { images, maxLength } = this.props;

    const actions = [
      { label: "Make as main image", callback: this.changeMainImage },
      { label: "Upload another", callback: this.uploadAnotherImage },
      { label: "Delete", callback: this.deleteImage }
    ];

    return (
      <Container>
        {!!images.length &&
          images.map(image => (
            <Image key={image.src} {...image} actions={actions} />
          ))}
        {images.length < maxLength && (
          <ChooseImageContainer>
            <Input onChange={this.handleChange} />
            <Plus>+</Plus>
          </ChooseImageContainer>
        )}
      </Container>
    );
  }
}

export default UploadImages;
