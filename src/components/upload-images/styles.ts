import styled from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div`
  display: flex;
  justify-content: flex-start;
  flex-wrap: wrap;
  width: 100%;
  position: relative;
  box-sizing: border-box;
  background-color: ${colors.white};

  > * {
    margin-right: 24px;
    width: calc(33% - 14px);

    &:nth-child(3n) {
      margin-right: 0;
    }
  }
`;

export const ChooseImageContainer = styled.label`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 176px;
  box-sizing: border-box;
  border-radius: 2px;
  border: 1px solid ${colors.gallery};
  background-color: ${colors.whisper};
  cursor: pointer;
`;

export const Plus = styled.span`
  font-size: 30px;
  color: ${colors.lightGrey};
`;

export const Input = styled.input.attrs({ type: "file" })`
  position: absolute;
  transform: scale(0.001);
  opacity: 0.01;
`;
