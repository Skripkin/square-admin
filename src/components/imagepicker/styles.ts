import styled from "styled-components";

import { colors } from "constants/.";

export const DeleteButton = styled.button`
  display: inline-block;
  position: absolute;
  top: 2px;
  right: 2px;
  padding: 8px;
  border: none;
  font-size: 20px !important;
  background-color: transparent;
  outline: none;
  color: white;
  cursor: pointer;
  opacity: 0;
  transition: opacity 0.5s;
`;

export const Container = styled.label`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: relative;

  // Временные размеры
  width: 336px;
  min-height: 169px;

  border-radius: 8px;
  border: 1px solid ${colors.gallery};
  box-sizing: border-box;
  background-color: ${colors.whisper};
  cursor: pointer;

  &:hover {
    ${DeleteButton} {
      opacity: 1;
    }
  }
`;

export const Image = styled.img`
  width: 100%;
  height: auto;
  border-radius: 8px;
`;

export const Input = styled.input.attrs({ type: "file" })`
  position: absolute;
  transform: scale(0.001);
  opacity: 0.01;
`;

export const Text = styled.span`
  color: ${colors.lightGrey};
`;

export const ImageIconWrapper = styled.span`
  margin-bottom: 4px;
  font-size: 28px;
  color: ${colors.lightGrey};
`;
