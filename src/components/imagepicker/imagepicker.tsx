import React from "react";

import ImageIcon from "@material-ui/icons/BrokenImageOutlined";
import Delete from "@material-ui/icons/DeleteOutline";

import {
  Container,
  Text,
  ImageIconWrapper,
  Input,
  Image,
  DeleteButton
} from "./styles";

interface IState {
  file: any;
  value: any;
}

class Imagepicker extends React.Component<{}, IState> {
  state = {
    file: null,
    value: null
  };

  handleChange = event => {
    const [file] = event.target.files;

    this.setState({ file: URL.createObjectURL(file), value: file.fileName });
  };

  handleClear = e => {
    e.preventDefault();
    this.setState({ file: null, value: null });
  };

  render() {
    const { file, value } = this.state;

    return (
      <Container>
        <Input value={value || ""} onChange={this.handleChange} />
        {!!file ? (
          <>
            <Image src={file} alt={value} />
            <DeleteButton onClick={this.handleClear}>
              <Delete fontSize="inherit" />
            </DeleteButton>
          </>
        ) : (
          <>
            <ImageIconWrapper>
              <ImageIcon fontSize="inherit" />
            </ImageIconWrapper>
            <Text>Choose photo</Text>
          </>
        )}
      </Container>
    );
  }
}

export default Imagepicker;
