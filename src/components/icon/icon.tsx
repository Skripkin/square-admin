import React from "react";

import { Svg } from "./styles";

type iconName =
  | "home"
  | "chat-massage"
  | "calendar"
  | "image"
  | "womenshoe"
  | "bell"
  | "instagram"
  | "woman"
  | "box"
  | "leaf"
  | "lightning"
  | "usersfree"
  | "camera"
  | "location"
  | "wallet"
  | "checkuser"
  | "man"
  | "sneaker"
  | "copy"
  | "people"
  | "singleuser"
  | "credit"
  | "plant"
  | "sova"
  | "user"
  | "download"
  | "reserv"
  | "star"
  | "facebook"
  | "setting"
  | "users"
  | "file"
  | "search"
  | "send"
  | "delete"
  | "cros"
  | "arrow"
  | "ofsetting"
  | "strelkaniz"
  | "vector"
  | "single-folded-content";

interface IProps {
  iconName: iconName;
  className?: string;
}

const Icon = ({ iconName, className = "" }: IProps) => {
  return (
    <Svg className={className}>
      <use xlinkHref={`/svg/icons-sprite.svg#${iconName}`} />
    </Svg>
  );
};

export default Icon;
