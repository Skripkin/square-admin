import styled from "styled-components";

import { colors } from "constants/.";

export const Header = styled.div`
  padding: 24px 24px;
  color: ${colors.shipGray}
  font-weight: 500;
  font-size: 16px;
  line-height: 22px;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 331px;
  background: #ffffff;
  border-right: 1px solid ${colors.gallery};
  box-sizing: border-box;
`;
export const List = styled.ul`
  height: 100%;
  overflow: hidden;
  overflow-y: scroll;
`;
export const Item = styled.li`
  list-style: none;
`;
