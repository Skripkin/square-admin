import React from "react";

import Chat from "../../chat";

import { Header, Container, Item, List } from "./styles";

const ChatList = ({ chats }) => {
  return (
    <Container>
      <Header>Chats</Header>
      <List>
        {chats.map((item, index) => (
          <Item key={index}>
            <Chat
              avatar={item.avatar}
              date={item.date}
              name={item.name}
              text={item.text}
              isactive={item.isactive}
            />
          </Item>
        ))}
      </List>
    </Container>
  );
};

export default ChatList;
