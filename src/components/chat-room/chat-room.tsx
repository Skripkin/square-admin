import React from "react";

import {
  Container,
  ChatWrapper,
  Footer,
  Input,
  IconButton,
  SendIcon
} from "./styles";
import ChatList from "./chat-list";
import ChatHeader from "./chat-header";
import Messages from "./messages";

export interface ChatProps {
  avatar: string;
  name: string;
  text: string;
  date: string;
  isactive?: boolean;
}

export interface IProps {
  chats: ChatProps[];
}

const ChatRoom = ({ chats }: IProps) => {
  return (
    <Container>
      <ChatList chats={chats} />
      <ChatWrapper>
        <ChatHeader />
        <Messages />
        <Footer>
          <Input placeholder="Write your message..."></Input>
          <IconButton>
            <SendIcon iconName="send" />
          </IconButton>
        </Footer>
      </ChatWrapper>
    </Container>
  );
};

export default ChatRoom;
