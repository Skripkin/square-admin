import styled from "styled-components";

import { colors } from "constants/.";
import Icon from "components/icon";

export const Container = styled.div`
  display: flex;
  height: calc(100vh - 72px - 50px);
  overflow: hidden;
  background: ${colors.whisper};
  border: 1px solid ${colors.gallery};
  box-sizing: border-box;
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);
  border-radius: 12px;
`;

export const ChatWrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100%;
  width: 100%;
  background: ${colors.whisper};
`;

export const Footer = styled.div`
  display: flex;
  align-items: center;
  height: 62px;
  background: white;
  border-top: 1px solid ${colors.gallery};
  font-size: 16px;
  line-height: 20px;
  overflow: hidden;
`;

export const Input = styled.textarea`
  flex: 1;
  height: 100%;
  padding: 20px 0 21px 24px;
  font-size: 16px;
  border: none;
  outline: none;
  resize: none;
`;

export const IconButton = styled.button`
  margin: 0 24px;
  font-size: 16px;
  border: none;
  background: inherit;
  outline: none;
  color: ${colors.pink};
`;

export const SendIcon = styled(Icon)`
  // stroke: ${colors.shipGray};
  fill: ${colors.pink};
`;
