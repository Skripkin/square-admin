import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  position: relative;
  flex: 1 1 0;
  flex-direction: column;
  justify-content: flex-end;
  overflow-y: hidden;
  z-index: 1;
`;

export const MessageList = styled.ul`
  overflow-y: auto;
`;

export const MessageItem = styled.li`
  list-style: none;
  margin: 16px 24px;
`;

export const MessageWrapper = styled.li`
  max-width: 86%;
`;
