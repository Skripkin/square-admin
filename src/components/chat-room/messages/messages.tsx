import React from "react";

import Message from "../../message";

import { Container, MessageList, MessageItem, MessageWrapper } from "./styles";

const messages = [
  { date: new Date(), text: "Nisi officia fugiat quis enim officia" },
  { date: new Date(), text: "Hey. How are you?", isOwn: true },
  {
    date: new Date(),
    text:
      "Nisi officia fugiat quis enim officia mollit laboris veniam ut voluptate in sit pariatur do velit pariatur fugiat ea laboris"
  },
  {
    date: new Date(),
    text: "Nisi officia fugiat quis enim officia",
    isOwn: true
  },
  { date: new Date(), text: "Hey. How are you?" },
  {
    date: new Date(),
    text:
      "Nisi officia fugiat quis enim officia mollit laboris veniam ut voluptate in sit pariatur do velit pariatur fugiat ea laboris",
    isOwn: true
  },
  { date: new Date(), text: "Nisi officia fugiat quis enim officia" },
  { date: new Date(), text: "Hey. How are you?", isOwn: true },
  {
    date: new Date(),
    text:
      "Nisi officia fugiat quis enim officia mollit laboris veniam ut voluptate in sit pariatur do velit pariatur fugiat ea laboris"
  },
  {
    date: new Date(),
    text: "Nisi officia fugiat quis enim officia",
    isOwn: true
  },
  { date: new Date(), text: "Hey. How are you?" },
  {
    date: new Date(),
    text:
      "Nisi officia fugiat quis enim officia mollit laboris veniam ut voluptate in sit pariatur do velit pariatur fugiat ea laboris",
    isOwn: true
  }
];

const Messages = () => {
  return (
    <Container>
      <MessageList>
        {messages.map((item, index) => (
          <MessageItem
            key={index}
            style={{
              display: "flex",
              justifyContent: item.isOwn ? "flex-end" : "flex-start"
            }}
          >
            <MessageWrapper>
              <Message date={item.date} text={item.text} isOwn={item.isOwn} />
            </MessageWrapper>
          </MessageItem>
        ))}
      </MessageList>
    </Container>
  );
};

export default Messages;
