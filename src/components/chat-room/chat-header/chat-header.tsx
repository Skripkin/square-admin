import React from "react";

import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

import { MoreHoriz } from "@material-ui/icons";

import {
  Container,
  Actions,
  Info,
  Wrapper,
  Img,
  ImgConteiner,
  IconButton,
  DeleteIcon
} from "./styles";

const options = ["Mute", "Copy"];

const ITEM_HEIGHT = 48;

const ChatList = () => {
  const name = "Harold Watson";

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Container>
      <Wrapper>
        <Info>
          <ImgConteiner>
            <Img src="Debra.png" />
          </ImgConteiner>
          <div>{name}</div>
        </Info>
        <Actions>
          <IconButton>
            <DeleteIcon iconName="delete" />
          </IconButton>
          <IconButton onClick={handleClick}>
            <MoreHoriz />
          </IconButton>
          <Menu
            id="long-menu"
            anchorEl={anchorEl}
            keepMounted
            open={open}
            onClose={handleClose}
            PaperProps={{
              style: {
                maxHeight: ITEM_HEIGHT * 4.5,
                width: 200
              }
            }}
          >
            {options.map(option => (
              <MenuItem
                key={option}
                selected={option === "Pyxis"}
                onClick={handleClose}
              >
                {option}
              </MenuItem>
            ))}
          </Menu>
        </Actions>
      </Wrapper>
    </Container>
  );
};

export default ChatList;
