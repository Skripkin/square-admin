import styled from "styled-components";

import { colors } from "constants/.";

import Icon from "../../icon";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  background: white;
  border-bottom: 1px solid ${colors.athensGray};
`;

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 16px 24px;
`;

export const Info = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const Actions = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  color: ${colors.lightGrey};
`;

export const ImgConteiner = styled.div`
  width: 32px;
  height: 32px;
  margin-right: 20px;
`;

export const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 50%;
`;

export const IconButton = styled.button`
  margin: 0 8px;
  border: none;
  background: inherit;
  outline: none;
  color: ${colors.lightGrey};
  cursor: pointer;
`;

export const DeleteIcon = styled(Icon)`
  stroke: ${colors.lightGrey};
  fill: ${colors.white};
`;
