import React from "react";

import { withStyles } from "@material-ui/styles";
import CircleCheckedFilled from "@material-ui/icons/CheckCircle";
import CircleUnchecked from "@material-ui/icons/RadioButtonUnchecked";

import { colors } from "constants/.";

import {
  Container,
  Checkbox as SQCheckbox,
  Title,
  IconWrapper,
  CheckboxIcon
} from "./styles";

interface IProps {
  icon?: any;
  name: string;
  classes: any;
  title: string;
  checked: boolean;
  onChange: (name: string, checked: boolean) => void;
}

const styles = theme => ({
  button: {
    "&:hover": {
      backgroundColor: "transparent"
    }
  }
});

const Checkbox = ({
  icon,
  name,
  title,
  checked: propChecked,
  onChange,
  classes
}: IProps) => {
  const [checked, setChecked] = React.useState(propChecked);

  const handleChange = () => {
    const value = !checked;

    setChecked(value);
    onChange(name, value);
  };

  return (
    <Container onClick={handleChange}>
      {!!icon && (
        <IconWrapper>
          <CheckboxIcon iconName={icon} />
        </IconWrapper>
      )}
      <Title>{title}</Title>
      <SQCheckbox
        disableRipple
        disableFocusRipple
        disableTouchRipple
        checked={checked}
        onChange={handleChange}
        value={name}
        className={classes.button}
        inputProps={{
          "aria-label": name
        }}
        icon={<CircleUnchecked style={{ color: "lightGray" }} />}
        checkedIcon={<CircleCheckedFilled style={{ color: colors.pink }} />}
      />
    </Container>
  );
};

export default withStyles(styles)(Checkbox);
