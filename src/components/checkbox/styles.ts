import styled from "styled-components";

import MaterialCheckbox from "@material-ui/core/Checkbox";

import { colors } from "constants/.";

import Icon from "../icon";

export const Container = styled.button`
  display: flex;
  align-items: center;
  padding: 0 16px;
  min-width: 220px;
  height: 50px;
  border: none;
  box-sizing: border-box;
  border: 1px solid ${colors.concrete};
  box-shadow: 0px 2px 16px rgba(242, 242, 242, 0.7);
  border-radius: 4px;
  background-color: white;
  outline: none;
  cursor: pointer;
`;

export const Checkbox = styled(MaterialCheckbox)``;

export const Title = styled.span`
  margin-right: auto;
  font-size: 14px;
  color: ${colors.darkGrey};
`;

export const IconWrapper = styled.span`
  line-height: 0;
  margin-right: 18px;
  font-size: 21px;
  color: ${colors.lightGrey};
`;

export const CheckboxIcon = styled(Icon)`
  fill: ${colors.lightGrey};
`;
