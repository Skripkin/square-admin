import styled from "styled-components";

import { colors } from "constants/.";

export const ItemsForm = styled.div`
  & > div {
    width: 100%;
  }
  .MuiOutlinedInput-input {
    padding: 15px 14px;
  }

  .MuiInputLabel-outlined {
    transform: translate(14px, 15px) scale(1);
  }

  fieldset {
    border-color: ${colors.concrete} !important;
  }
`;

export const TitleOfSelect = styled.p`
  margin-bottom: 10px;
  font-size: 14px;
  line-height: 18px;
  color: ${colors.darkGrey};
`;
