import React from "react";

import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import MaterialSelect from "@material-ui/core/Select";

import { ItemsForm, TitleOfSelect } from "./styled";

interface IOption {
  title: string;
  value: string;
}

interface IProps {
  title?: string;
  value: string;
  options: IOption[];
  onChange: (value: string) => void;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      minWidth: 100
    },
    selectEmpty: {
      marginTop: theme.spacing(2)
    }
  })
);

const Select = ({ title, options, value, onChange }: IProps) => {
  const classes = useStyles("");

  return (
    <ItemsForm>
      <TitleOfSelect>{title}</TitleOfSelect>
      <FormControl variant="outlined" className={classes.formControl}>
        <MaterialSelect
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          value={value}
          onChange={(e: any) => onChange(e.target.value)}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {options.map(item => (
            <MenuItem key={item.value} value={item.value}>
              {item.title}
            </MenuItem>
          ))}
        </MaterialSelect>
      </FormControl>
    </ItemsForm>
  );
};

export default Select;
