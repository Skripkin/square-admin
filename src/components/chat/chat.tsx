import React from "react";

import {
  Container,
  Img,
  ImgConteiner,
  InfoContainer,
  Info,
  Message,
  Name,
  Time
} from "./styles";

interface IProps {
  avatar: string;
  name: string;
  text: string;
  date: string;
  isactive?: boolean;
}

const Chat = ({ avatar, name, text, date, isactive }: IProps) => {
  return (
    <Container isactive={isactive ? "true" : "false"}>
      <ImgConteiner>
        <Img src={avatar} />
      </ImgConteiner>
      <InfoContainer>
        <Info>
          <Name>{name}</Name>
          <Time>{date}</Time>
        </Info>
        <Message>{text}</Message>
      </InfoContainer>
    </Container>
  );
};

export default Chat;
