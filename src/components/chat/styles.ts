import styled, { css } from "styled-components";

import { colors } from "constants/.";

export const ImgConteiner = styled.div`
  margin-right: 20px;
`;

export const Img = styled.img`
  width: 50px;
  height: 50px;
  object-fit: cover;
  border-radius: 50%;
`;

export const InfoContainer = styled.div`
  width: 100%;
  overflow: hidden;
  text-overflow: clip;
`;

export const Info = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const Name = styled.div`
  font-weight: 600;
  font-size: 16px;
  line-height: 22px;
  letter-spacing: -0.25px;
  color: ${colors.shipGray};
`;

export const Time = styled.div`
  font-size: 13px;
  line-height: 16px;
  text-align: right;
  color: ${colors.lightGrey};
`;
export const Message = styled.div`
  padding: 12px 0;
  height: 52px;
  line-height: 19px;
  font-size: 15px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  text-align: left;
  color: ${colors.lightGrey};
`;

export const Container = styled.button<{ isactive: "true" | "false" }>`
  display: flex;
  padding: 16px 19px;
  width: 100%;
  background: white;
  box-shadow: inset 0px -1px 0px ${colors.athensGray};
  border: none;

  outline: none;
  border-radius: 0px;
  font-family: CircularStd;
  font-style: normal;
  cursor: pointer;

  ${props =>
    props.isactive === "true" &&
    css`
      background-color: ${colors.whisper};

      ${Name} {
        display: block;
        color: ${colors.pink};
      }
    `}

  &:hover {
    background-color: ${colors.whisper};

    ${Name} {
      display: block;
      color: ${colors.pink};
    }
  }
`;
