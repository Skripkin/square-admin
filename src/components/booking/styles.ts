import styled from "styled-components";

import { colors } from "constants/.";

import Icon from "../icon";

export const Container = styled.div`
  box-sizing: border-box;
  border: 1px solid ${colors.gallery};
  border-radius: 8px;
  background-color: white;
`;

export const Header = styled.div`
  padding: 8px 0;
  border-bottom: 1px solid ${colors.gallery};
  border-radius: 8px 8px 0 0;
  background: ${colors.cornflowerBlue};
  color: white;
`;

export const Title = styled.p`
  line-height: 20px;
  font-size: 16px;
  text-align: center;
`;

export const SubTitle = styled.p`
  line-height: 18px;
  font-size: 16px;
  text-align: center;
  opacity: 0.7;
`;

export const Body = styled.div`
  overflow-y: auto;
  padding: 0 8px;
`;

export const Card = styled.div`
  margin: 10px 16px 0 16px;
  margin-bottom: 0;
  border-bottom: 1px solid ${colors.concrete};

  &:last-child {
    border-bottom: none;
  }
`;

export const Info = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Time = styled.div`
  font-weight: 500;
  font-size: 13px;
  color: ${colors.darkGrey};
`;

export const MemberCountWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const MemberCount = styled.span`
  font-size: 14px;
`;

export const MembersWrapper = styled.div`
  padding: 10px 0;
`;

export const BookIcon = styled(Icon)`
  width: 16px;
  height: 16px;
  stroke: ${colors.lightGrey};
  fill: ${colors.lightGrey};
`;
