import React from "react";
import moment from "moment";

import { Members } from "components";

import * as Styled from "./styles";

interface IMember {
  dateFrom: Date;
  dateTo: Date;
  members: string[];
}

interface IProps {
  date: Date;
  slotCounter: number;
  members: IMember[];
}

const Bookiing = ({ date, members, slotCounter }: IProps) => {
  const formattedDate = moment(date).format("ddd, DD");

  return (
    <Styled.Container>
      <Styled.Header>
        <Styled.Title>{formattedDate}</Styled.Title>
        <Styled.SubTitle>{slotCounter} spots</Styled.SubTitle>
      </Styled.Header>

      <Styled.Body>
        {members.map((member, index) => (
          <Styled.Card key={index}>
            <Styled.Info>
              <Styled.Time>
                {moment(member.dateFrom).format("hh:mm") +
                  " - " +
                  moment(member.dateTo).format("hh:mm")}
              </Styled.Time>
              <Styled.MemberCountWrapper>
                <Styled.MemberCount>{member.members.length}</Styled.MemberCount>
                <Styled.BookIcon iconName="user" />
              </Styled.MemberCountWrapper>
            </Styled.Info>
            <Styled.MembersWrapper>
              <Members counter={4} images={member.members}></Members>
            </Styled.MembersWrapper>
          </Styled.Card>
        ))}
      </Styled.Body>
    </Styled.Container>
  );
};

export default Bookiing;
