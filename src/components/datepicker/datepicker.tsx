import React from "react";
import moment from "moment";

import DayPicker, { DateUtils } from "react-day-picker";

import { Container } from "./styles";

moment.locale("es");

interface IProps {
  multi?: boolean;
  value: Date[];
  space?: number;
  onChange: (value: Date[]) => void;
}

const Datepicker = ({ value, onChange, multi = false, space = 8 }: IProps) => {
  const handleDayClick = day => {
    if (!multi) {
      onChange([day]);
      return;
    }

    const newValue = [...value];
    const selectedIndex = newValue.findIndex(v => DateUtils.isSameDay(v, day));

    if (selectedIndex > -1) {
      newValue.splice(selectedIndex, 1);
    } else {
      newValue.push(day);
    }

    onChange(newValue);
  };

  return (
    <Container space={space}>
      <DayPicker
        weekdaysShort={Array.apply(null, Array(7)).map((_, i) =>
          moment(i, "e")
            .startOf("week")
            .isoWeekday(i)
            .format("ddd")
        )}
        firstDayOfWeek={1}
        selectedDays={value}
        onDayClick={handleDayClick}
      />
    </Container>
  );
};

export default Datepicker;
