import styled from "styled-components";
import "react-day-picker/lib/style.css";

import { colors } from "constants/.";

export const Container = styled.div<{ space: number }>`
  position: relative;
  font-family: CircularStd;
  background-color: white;

  * {
    outline: none;
  }

  .DayPicker {
    width: 100%;
  }

  .DayPicker-Month {
    display: block;
    margin: 0;
    width: 100%;
  }

  .DayPicker-NavButton {
    display: flex;
    align-items: center;
    top: 13px;
    font-size: 24px;
    font-family: "Material Icons";
    color: ${colors.shipGray};
    background: initial;
  }

  .DayPicker-NavButton.DayPicker-NavButton--prev {
    left: 18px;

    &::after {
      content: "keyboard_arrow_left";
    }
  }

  .DayPicker-NavButton.DayPicker-NavButton--next {
    right: 12px;

    &::after {
      content: "keyboard_arrow_right";
    }
  }

  .DayPicker-Caption {
    display: block;
    padding-top: 20px;
    padding-bottom: 20px;
    border-bottom: 1px solid ${colors.gallery};

    > div {
      font-size: 16px;
      text-align: center;
    }
  }

  .DayPicker-Weekdays {
    display: block;
    margin-top: 10px;
    margin-bottom: 14px;
  }

  .DayPicker-WeekdaysRow {
    display: flex;
    justify-content: space-between;
    padding-right: 16px;
    padding-left: 16px;
  }

  .DayPicker-Weekday {
    font-size: 13px;
  }

  .DayPicker-Day {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 28px;
    height: 28px;
    font-size: 15px;

    &:not(:last-child) {
      margin-right: ${props => props.space}px;
    }

    &--selected {
      background-color: ${colors.pink} !important;
      color: white !important;
    }

    &--today {
      font-weight: 100;
    }
  }

  .DayPicker-Week {
    display: flex;
    justify-content: flex-start;
    margin-bottom: 12px;
    padding-left: 16px;
    padding-right: 16px;
  }
`;
