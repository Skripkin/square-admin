import React from "react";

import Menu, { MenuProps } from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {
  createStyles,
  makeStyles,
  Theme,
  withStyles
} from "@material-ui/core/styles";

import SetDays from "./set-days";

import Datepicker from "components/datepicker";

import * as Styles from "./styled";
import { SET_CHECKBOX } from "./mock";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    menu: {
      boxShadow: "0px 2px 12px rgba(0, 0, 0, 0.1)",
      borderRadius: "8px",
      backgroundColor: "white"
    },
    calendarMenu: {
      top: "0px !important",
      right: "0px !important",
      bottom: "0px !important",
      left: "0px !important",
      margin: "auto",
      padding: "10px",
      maxWidth: "384px",
      boxShadow: "0px 2px 12px rgba(0, 0, 0, 0.1)",
      borderRadius: "8px"
    }
  })
);

const StyledMenu = withStyles({
  paper: {
    boxShadow: "0px 2px 12px rgba(0, 0, 0, 0.1)",
    borderRadius: "8px"
  }
})((props: MenuProps) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "right"
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "right"
    }}
    {...props}
  />
));

const ChooseDay = () => {
  const classes = useStyles("");
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [openwind, setOpenwind] = React.useState<null | HTMLElement>(null);
  const [date, setDate] = React.useState([new Date()]);

  const Click = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setOpenwind(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
    setOpenwind(null);
  };

  return (
    <div>
      <Styles.Container onClick={Click}>
        <Styles.FixIcon iconName="ofsetting" />
      </Styles.Container>
      <StyledMenu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        PopoverClasses={{
          paper: classes.menu
        }}
      >
        <MenuItem disableTouchRipple>
          <button onClick={handleClick}>
            <Styles.MenuItemList>Manage calendar</Styles.MenuItemList>
          </button>
        </MenuItem>
      </StyledMenu>
      <Menu
        id="simple-menu-calendar"
        anchorEl={openwind}
        keepMounted
        open={Boolean(openwind)}
        onClose={handleClose}
        PopoverClasses={{
          paper: classes.calendarMenu
        }}
      >
        <Styles.ChooseDayBackdrop onClick={handleClose} />
        <Styles.ChooseDayBox>
          <Styles.ChooseDayTitle>Choose day</Styles.ChooseDayTitle>
          <button onClick={handleClose}>
            <Styles.FooterIcon iconName="cros" />
          </button>
        </Styles.ChooseDayBox>
        <Styles.DataPickerBox>
          <Datepicker multi value={date} onChange={setDate} space={16} />
        </Styles.DataPickerBox>
        <Styles.MenuContainer>
          <Styles.BottomDayTitle>Choosed Days</Styles.BottomDayTitle>
          <Styles.ChooseTimeBox>
            {!!date.length &&
              date.map((item, i) => (
                <SetDays key={i} dataTitle={item} checkTitle={SET_CHECKBOX} />
              ))}
          </Styles.ChooseTimeBox>
          <Styles.SaveButton onClick={handleClose}>Save</Styles.SaveButton>
        </Styles.MenuContainer>
      </Menu>
    </div>
  );
};

export default ChooseDay;
