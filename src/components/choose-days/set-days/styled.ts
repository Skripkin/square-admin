import styled from "styled-components";

import MenuItem from "@material-ui/core/MenuItem";

import { colors } from "constants/.";
import Icon from "components/icon";

export const SetDays = styled.div`
  display: flex;
  align-items: center;
  margin-top: 8px;
  margin-right: 8px;
  padding: 7px 12px;
  border-radius: 4px;
  background: #f7f8fa;
`;

export const LeftTimeText = styled.span`
  font-size: 13px;
  line-height: 16px;
  color: ${colors.black};
`;

export const Slach = styled.span`
  margin: 0 8px;
  color: #dee1e7;
`;

export const TimeMenu = styled.button`
  display: flex;
  align-items: center;
`;

export const TimeText = styled.span`
  font-size: 13px;
  line-height: 16px;
  color: ${colors.black};
  opacity: 0.6;
`;

export const StrelkaIcon = styled(Icon)`
  width: 6px;
  margin-left: 8px;
`;

export const ChooseDayBox = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 12px;
`;

export const ChooseDayTitle = styled.span`
  margin-bottom: 10px;
  font-weight: 500;
  font-size: 18px;
  line-height: 23px;
  color: ${colors.shipGray};
`;

export const FooterIcon = styled(Icon)`
  stroke: ${colors.lightGrey};
  fill: ${colors.white};
`;

export const MenuCheckBox = styled(MenuItem)`
  &:hover {
    background-color: inherit !important;
  }

  .MuiListItem-gutters {
    padding-left: 12px;
    padding-right: 12px;
  }

  .MuiCheckbox-root {
    padding: 0;
  }

  button {
    height: 38px;
    padding: 0;
    box-shadow: none;
    border: none;
  }
`;
