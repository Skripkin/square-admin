import React from "react";
import moment from "moment";

import Menu, { MenuProps } from "@material-ui/core/Menu";

import {
  createStyles,
  makeStyles,
  Theme,
  withStyles
} from "@material-ui/core/styles";

import * as Styles from "./styled";

import Checkbox from "components/checkbox";

interface SCheck {
  name: string;
  title: string;
}

interface SDay {
  dataTitle: any;
  checkTitle: SCheck[];
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    menu: {
      boxShadow: "0px 2px 12px rgba(0, 0, 0, 0.1)",
      borderRadius: "8px",
      backgroundColor: "white"
    },
    calendarMenu: {
      top: "0px !important",
      right: "0px !important",
      bottom: "0px !important",
      left: "0px !important",
      margin: "auto",
      padding: "10px",
      maxWidth: "384px",
      boxShadow: "0px 2px 12px rgba(0, 0, 0, 0.1)",
      borderRadius: "8px"
    }
  })
);

const StyledMenu = withStyles({
  paper: {
    boxShadow: "0px 2px 12px rgba(0, 0, 0, 0.1)",
    borderRadius: "8px"
  }
})((props: MenuProps) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "right"
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "right"
    }}
    {...props}
  />
));

const SetDays = ({ dataTitle, checkTitle }: SDay) => {
  const classes = useStyles("");
  const [openCheck, setOpenCheck] = React.useState<null | HTMLElement>(null);
  const [checked, setChecked] = React.useState(false);
  const [items, setItems] = React.useState(checkTitle);

  const day = moment(dataTitle).format("ddd, Do MMM");

  const CheckClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setOpenCheck(event.currentTarget);
  };

  const handleChange = item => {
    setItems(
      items.map(weekDay => (weekDay.name === item.name ? item : weekDay))
    );
  };

  const Close = () => {
    setOpenCheck(null);
  };

  return (
    <div>
      <Styles.SetDays>
        <Styles.LeftTimeText>{day}</Styles.LeftTimeText>
        <Styles.Slach>|</Styles.Slach>
        <Styles.TimeMenu onClick={CheckClick}>
          <Styles.TimeText>2 Timeframes</Styles.TimeText>
          <Styles.StrelkaIcon iconName="strelkaniz" />
        </Styles.TimeMenu>
        <StyledMenu
          id="simple-menu-calendar"
          anchorEl={openCheck}
          keepMounted
          open={Boolean(openCheck)}
          onClose={Close}
          PopoverClasses={{
            paper: classes.menu
          }}
        >
          <Styles.ChooseDayBox>
            <Styles.ChooseDayTitle>Choose day</Styles.ChooseDayTitle>
            <button onClick={Close}>
              <Styles.FooterIcon iconName="cros" />
            </button>
          </Styles.ChooseDayBox>
          {checkTitle.map(item => (
            <Styles.MenuCheckBox
              onClick={() => handleChange(item)}
              key={item.name}
              disableTouchRipple
            >
              <Checkbox
                checked={checked}
                name={item.name}
                title={item.title}
                onChange={(_, checked) => setChecked(checked)}
              />
            </Styles.MenuCheckBox>
          ))}
        </StyledMenu>
      </Styles.SetDays>
    </div>
  );
};

export default SetDays;
