import styled from "styled-components";

import { colors } from "constants/.";

import Icon from "../icon";
import Button from "components/button";

export const Container = styled.button`
  margin-right: 12px;
  vertical-align: middle;
  border: none;
  background: none;
`;

export const FixIcon = styled(Icon)`
  width: 16px;
  height: 16px;
  fill: #c0c1ce;
`;

export const MenuItemList = styled.p`
  margin: 5px 10px;
  font-size: 15px;
  line-height: 19px;
  color: ${colors.shipGray};
`;

export const ChooseDayBox = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 12px;
`;

export const DataPickerBox = styled.div`
  margin: 0px 20px;
  border-bottom: 1px solid ${colors.concrete};
`;

export const ChooseDayBackdrop = styled.button`
  display: block;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: -1;
  width: 100vw;
  height: 100vh;
  background-color: rgba(0, 0, 0, 0.3);
  cursor: pointer;
`;

export const ChooseDayTitle = styled.span`
  margin-bottom: 10px;
  font-weight: 500;
  font-size: 18px;
  line-height: 23px;
  color: ${colors.shipGray};
`;

export const FooterIcon = styled(Icon)`
  stroke: ${colors.lightGrey};
  fill: ${colors.white};
`;

export const MenuContainer = styled.div`
  padding: 0 24px;
`;

export const BottomDayTitle = styled.p`
  margin: 16px 0;
  font-size: 14px;
  line-height: 18px;
  color: ${colors.darkGrey};
`;

export const ChooseTimeBox = styled.div`
  display: flex;
  flex-wrap: wrap;
  max-width: 300px;
`;

export const SaveButton = styled(Button)`
  width: 100%;
  margin-top: 24px;
`;
