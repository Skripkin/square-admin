export const SET_CHECKBOX = [
  {
    name: "All Day",
    title: "All Day"
  },
  {
    name: "2:00pm - 4:00pm (3 offers)",
    title: "2:00pm - 4:00pm (3 offers)"
  },
  {
    name: "5:00pm - 8:00pm (2 offers)",
    title: "5:00pm - 8:00pm (2 offers)"
  }
];
