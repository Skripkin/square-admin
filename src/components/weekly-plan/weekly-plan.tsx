import React from "react";
import moment from "moment";

import _get from "lodash/get";

import * as Styled from "./styles";
import Offer from "./offer";
import { IOffer } from "./offer/offer";
import Stub from "./stub";

const weekdayNames = Array.apply(null, Array(7)).map((_, i) =>
  moment(i, "e")
    .startOf("week")
    .isoWeekday(i + 1)
    .format("dddd")
);

const hoursADay = Array.apply(null, Array(17)).map((_, i) =>
  moment(i + 6, "h").format("HH:mm")
);

interface IDisplayedOffer extends IOffer {
  position: number;
}

export interface IWeekDay {
  type: string;
  offers: IOffer[];
}

export interface IWeeklyPlan {
  weekDays: IWeekDay[];
}

const WeeklyPlan = ({ weekDays = [] }: IWeeklyPlan) => {
  const [activeWeekDay, setActiveWeekDay] = React.useState(weekDays[0].type);

  const currentWeekDay = weekDays.find(({ type }) => type === activeWeekDay);
  const activeOffers = _get(currentWeekDay, "offers", [] as IOffer[]);
  let displayedOffers = [] as IDisplayedOffer[];

  return (
    <Styled.Container>
      <Styled.WeekDays>
        {weekdayNames.map(name => (
          <Styled.WeekDay
            key={name}
            onClick={() => setActiveWeekDay(name)}
            isactive={name === activeWeekDay ? "true" : "false"}
          >
            {name}
          </Styled.WeekDay>
        ))}
      </Styled.WeekDays>
      <Styled.Content>
        {hoursADay.map(hour => {
          const h = Number(hour.split(":")[0]);

          const stubs = displayedOffers.reduce((acc, offer) => {
            const { position, to, id } = offer;

            if (h === to) {
              displayedOffers = displayedOffers.filter(o => o.id !== id);
            }

            acc[position] = {};

            return [...acc];
          }, []);

          const offersForTime = activeOffers.reduce((acc, offer) => {
            if (offer.from !== h) {
              return acc;
            }

            const offerIndex = acc.findIndex(o => !o);
            const index = offerIndex !== -1 ? offerIndex : acc.length;

            if (offer.from !== offer.to) {
              displayedOffers.push({ ...offer, position: index });
            }

            acc[index] = offer;

            return [...acc];
          }, stubs);

          return (
            <Styled.Hour key={hour}>
              <Styled.HourValue>{hour}</Styled.HourValue>
              <Styled.HourContent>
                <Styled.HourLine />
                <Styled.Offers>
                  {offersForTime.map((offer, i) => {
                    if (!!offer && !!Object.keys(offer).length) {
                      return <Offer key={offer.id} {...offer} />;
                    }

                    return <Stub key={i} />;
                  })}
                </Styled.Offers>
              </Styled.HourContent>
            </Styled.Hour>
          );
        })}
      </Styled.Content>
    </Styled.Container>
  );
};

export default WeeklyPlan;
