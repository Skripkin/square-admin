import styled from "styled-components";

export const Container = styled.div`
  position: relative;
  z-index: 1;
  margin-top: 2px;
  margin-right: 16px;
  width: 195px;
  height: 39px;
  background-color: transparent;

  &:last-child {
    margin-right: 0;
  }
`;
