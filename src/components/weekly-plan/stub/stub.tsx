import React from "react";

import { Container } from "./styles";

const Stub = () => <Container />;

export default Stub;
