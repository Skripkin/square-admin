import React from "react";

import {
  Container,
  Count,
  UsersCount,
  IconWrapper,
  Content,
  OfferIcon
} from "./styles";

export interface IOffer {
  id: number;
  from: number;
  to: number;
  count: number;
  usersCount: number;
}

const ROW_HEIGHT = 39;
const GAP_HEIGHT = 5;

const Offer = ({ count, usersCount, from, to }: IOffer) => {
  const hours = to - from;

  const height =
    hours === 0 ? ROW_HEIGHT : ROW_HEIGHT + (ROW_HEIGHT + GAP_HEIGHT) * hours;

  return (
    <Container containerHeight={height}>
      <Content>
        <Count>
          {count} {count > 1 ? "offers" : "offer"}
        </Count>
        <UsersCount>{usersCount}</UsersCount>
        <IconWrapper>
          <OfferIcon iconName="user" />
        </IconWrapper>
      </Content>
    </Container>
  );
};

export default Offer;
