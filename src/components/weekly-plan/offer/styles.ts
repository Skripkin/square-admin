import styled from "styled-components";

import { colors } from "constants/.";

import Icon from "../../icon";

export const Container = styled.button<{ containerHeight: number }>`
  position: relative;
  z-index: 2;
  margin-top: 2px;
  margin-right: 16px;
  width: 195px;
  height: ${({ containerHeight }) => `${containerHeight}px`};
  box-sizing: border-box;
  border: none;
  border-radius: 4px;
  outline: none;
  background-color: ${colors.concrete};
  color: ${colors.lightGrey};
  cursor: pointer;

  &:last-child {
    margin-right: 0;
  }
`;

export const Content = styled.div`
  display: flex;
  align-items: flex-start;
  padding: 12px 16px;
  height: 100%;
`;

export const Count = styled.span`
  margin-right: auto;
  font-size: 13px;
`;

export const UsersCount = styled.span`
  font-size: 14px;
  color: ${colors.shipGray};
`;

export const IconWrapper = styled.span`
  line-height: 0;
  font-size: 16px;
`;

export const OfferIcon = styled(Icon)`
  width: 16px;
  height: 16px;
  stroke: ${colors.lightGrey};
  fill: ${colors.lightGrey};
`;
