import styled, { css } from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div`
  border: 1px solid ${colors.gallery};
  border-radius: 6px;
  background-color: white;
`;

export const WeekDays = styled.div`
  display: flex;
`;

export const WeekDay = styled.button<{ isactive: "true" | "false" }>`
  flex: 1;
  height: 48px;
  line-height: 48px;
  box-sizing: border-box;
  border: none;
  border-bottom: 1px solid ${colors.gallery};
  border-right: 1px solid ${colors.gallery};
  outline: none;
  font-size: 15px;
  text-align: center;
  background-color: ${colors.whisper};
  color: ${colors.lightGrey};
  cursor: pointer;

  &:last-child {
    border-right: none;
  }

  ${({ isactive }) =>
    isactive === "true" &&
    css`
      border-bottom: 1px solid ${colors.pink};
      background-color: white;
      color: ${colors.shipGray};
    `}
`;

export const Content = styled.div`
  padding: 35px 24px 12px 16px;
  max-height: 420px;
  overflow-y: auto;
`;

export const Hour = styled.div`
  display: flex;
  align-items: flex-start;
  height: 44px;
`;

export const HourContent = styled.div`
  flex: 1;
`;

export const HourValue = styled.span`
  display: inline-block;
  margin-top: -8px;
  margin-right: 20px;
  width: 38px;
  font-size: 13px;
  color: ${colors.lightGrey};
`;

export const HourLine = styled.hr`
  border: none;
  height: 1px;
  background-color: ${colors.gallery};
`;

export const Offers = styled.div`
  display: flex;
`;
