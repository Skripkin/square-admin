import React from "react";
import _get from "lodash/get";

import CloseIcon from "@material-ui/icons/Close";

import Input from "components/input";
import Select from "components/select";

import { COUNTRY, CITY } from "./mock";
import * as Styles from "./styles";

const NewBusiness = props => {
  const goBack = _get(props, "0.history.goBack", () => null);

  return (
    <Styles.Container>
      <Styles.Header>
        <span>Create new business</span>
        <Styles.CloseButton onClick={goBack}>
          <CloseIcon />
        </Styles.CloseButton>
      </Styles.Header>
      <div>
        <Input
          placeholder=""
          title="Business name"
          value=""
          onChange={() => null}
        />
        <Styles.Adress>
          <Input placeholder="" title="Adress" value="" onChange={() => null} />
          <Input
            placeholder=""
            title="ZIP code"
            value=""
            onChange={() => null}
          />
        </Styles.Adress>
        <Styles.Adress>
          <Select
            title="Country"
            options={COUNTRY}
            value={COUNTRY[0].value}
            onChange={() => null}
          />
          <Select
            title="City, State"
            options={CITY}
            value={CITY[0].value}
            onChange={() => null}
          />
        </Styles.Adress>
        <Input placeholder="" title="Email" value="" onChange={() => null} />
        <Styles.Adress>
          <Input
            placeholder=""
            title="Password"
            value=""
            onChange={() => null}
          />
          <Input
            placeholder=""
            title="Confirm password"
            value=""
            onChange={() => null}
          />
        </Styles.Adress>
        <Styles.Register>
          By clicking “Register” I agree to Square’s{" "}
          <Styles.Link href="#">Terms of Service</Styles.Link>
        </Styles.Register>
        <Styles.RegisterButton>Register</Styles.RegisterButton>
      </div>
    </Styles.Container>
  );
};

export default NewBusiness;
