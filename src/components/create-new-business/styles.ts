import styled from "styled-components";

import Button from "components/button";

import { colors } from "constants/.";

export const Container = styled.div`
  max-width: 450px;
  padding: 24px 32px;
  border-radius: 12px;
  background: ${colors.white};
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 35px;
`;

export const Title = styled.span`
  align-self: center;
  font-weight: 500;
  font-size: 18px;
  line-height: 23px;
  color: ${colors.shipGray};
`;

export const Adress = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 24px 0;

  & > div {
    width: 47%;
  }
`;

export const Register = styled.p`
  font-size: 13px;
  line-height: 16px;
  text-align: center;
  color: ${colors.lightGrey};
`;

export const Link = styled.a`
  font-size: 13px;
  line-height: 16px;
  color: ${colors.lightGrey};
`;

export const RegisterButton = styled(Button)`
  width: 100%;
  margin-top: 24px;
`;

export const CloseButton = styled.button`
  color: ${colors.shipGray};
  cursor: pointer;
`;
