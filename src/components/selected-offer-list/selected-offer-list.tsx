import React from "react";

import SelectedOffer, { IOffer } from "../selected-offer/selected-offer";
import { SelectTitle, Container } from "./styles";

interface IProps {
  offers: IOffer[];
  onRemove: (id: number) => void;
}

const SelectedOfferList = ({ offers, onRemove }: IProps) => {
  return (
    <Container>
      <SelectTitle>
        <span>Offers selected</span>
        <span>Copy</span>
      </SelectTitle>
      {offers.map((item, i) => (
        <div key={item.text}>
          <SelectedOffer
            image={item.image}
            text={item.text}
            onRemove={() => onRemove(i)}
          />
        </div>
      ))}
    </Container>
  );
};

export default SelectedOfferList;
