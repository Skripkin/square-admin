import styled from "styled-components";

import { colors } from "constants/.";

export const SelectTitle = styled.div`
  display: flex;
  justify-content: space-between;

  span:first-child {
    margin-left: 7px;
    font-size: 14px;
    line-height: 18px;
    color: ${colors.darkGrey};
  }

  span:last-child {
    margin-right: 11px;
    font-size: 14px;
    line-height: 18px;
    color: ${colors.pink};
  }
`;

export const Container = styled.div`
  div {
    margin-bottom: 4px;
  }
`;
