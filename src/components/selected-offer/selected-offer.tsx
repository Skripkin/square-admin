import React from "react";

import Avatar from "@material-ui/core/Avatar";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

import Close from "@material-ui/icons/Close";
import { CloseButton, Container } from "./styles";

export interface IOffer {
  image: string;
  text: string;
  onRemove?: () => void;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      "& > *": {
        margin: theme.spacing(1)
      }
    },
    small: {
      width: 32,
      height: 32
    }
  })
);

const SelectedOffer = ({ image, text, onRemove }: IOffer) => {
  const classes = useStyles("");

  return (
    <Container>
      <div className={classes.root}>
        <Avatar
          alt="Remy Sharp"
          src={image}
          className={!onRemove ? classes.small : ""}
        />
      </div>
      <p>{text}</p>

      {!!onRemove && (
        <CloseButton onClick={onRemove}>
          <Close fontSize="inherit" />
        </CloseButton>
      )}
    </Container>
  );
};

export default SelectedOffer;
