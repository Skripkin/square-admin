import styled from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div`
  display: flex
  align-items: center;

  p {
    font-size: 15px;
    line-height: 19px;
    color: ${colors.shipGray};
  }
  
`;

export const CloseButton = styled.button`
  margin-left: auto;
  padding: 8px;
  border: none;
  font-size: 16px !important;
  background-color: transparent;
  outline: none;
  color: ${colors.lightGrey};
  cursor: pointer;
`;
