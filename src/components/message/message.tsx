import React from "react";
import moment from "moment";

import { Container, Text, Time } from "./styles";

interface IProps {
  text: string;
  date: Date;
  isOwn?: boolean;
}

const Message = ({ text, date, isOwn }: IProps) => {
  const time = moment(date).format("hh:mm");

  return (
    <Container isown={isOwn ? "true" : "false"}>
      <Text>{text}</Text>
      <Time>{time}</Time>
    </Container>
  );
};

export default Message;
