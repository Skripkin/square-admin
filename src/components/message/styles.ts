import styled, { css } from "styled-components";
import { colors } from "constants/.";

export const Container = styled.div<{ isown?: "true" | "false" }>`
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  padding: 12px;
  border-radius: 12px;
  border-top-left-radius: 0;
  font-family: CircularStd;
  background: ${colors.athensGray};
  color: ${colors.shipGray};

  ${props =>
    props.isown === "true" &&
    css`
      background: ${colors.pink};
      border-radius: 12px;
      border-bottom-right-radius: 0;
      color: ${colors.white};
    `}
`;

export const Text = styled.div`
  font-size: 15px;
  line-height: 19px;
`;

export const Time = styled.div`
  padding: 0 6px;
  font-size: 13px;
  line-height: 16px;
  opacity: 0.7;
`;
