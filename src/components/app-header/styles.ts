import styled from "styled-components";

import Popper from "@material-ui/core/Popper";

import Face from "@material-ui/icons/Face";
import ArrowDropDown from "@material-ui/icons/ArrowDropDown";

import { colors } from "constants/.";
import Icon from "../icon";

export const Container = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  z-index: 1001;
  height: 72px;
  box-shadow: 0 1px 0 ${colors.gallery};
  background-color: white;
`;

export const SearchWrapper = styled.div`
  margin-left: 40px;
  margin-right: auto;
`;

export const IconButton = styled.button`
  display: flex;
  padding: 8px;
  font-size: 18px;
  border: none;
  background-color: transparent;
  cursor: pointer;

  svg {
    font-size: inherit;
  }
`;

export const PopperRoot = styled(Popper)`
  width: 100%;
`;

export const FaceIcon = styled(Face)`
  margin-right: 10px;
  color: black;
`;

export const UserWrapper = styled.div`
  position: relative;
  width: 275px;
  text-align: center;
`;

export const UserButton = styled.button`
  display: inline-flex;
  justify-content: space-between;
  align-items: center;
  height: 100%;
  border: none;
  background-color: transparent;
  outline: none;
  cursor: pointer;
`;

export const UserAvatar = styled.img`
  margin-right: 10px;
  width: 32px;
  height: 32px;
  border-radius: 10px;
`;

export const UserInfo = styled.div`
  margin-right: 20px;
  text-align: left;
`;

export const CustomerName = styled.span`
  display: block;
  margin-top: 4px;
  font-weight: 300;
  font-size: 12px;
  opacity: 0.7;
`;

export const ExpandIcon = styled(ArrowDropDown)`
  font-size: 14px !important;
`;

export const MediumSpan = styled.span`
  font-size: 15px;
  font-weight: 500;
`;

export const ProfileIcon = styled(Icon)`
  stroke: ${colors.shipGray};
  fill: ${colors.white};
`;
