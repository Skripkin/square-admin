import React from "react";

import MenuList from "@material-ui/core/MenuList";
import MenuItem from "@material-ui/core/MenuItem";

import * as Styled from "./styles";

interface IProps {
  history: any;
  toggleMenu: () => void;
}

interface IState {
  menuItems: any[];
}

class User extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
    // const { history } = props;

    this.state = {
      menuItems: [
        {
          label: "Some option",
          icon: <Styled.FaceIcon />,
          onClick: this.handleItemClick(() => null)
        }
      ]
    };
  }

  handleItemClick = callback => () => {
    this.props.toggleMenu();
    callback();
  };

  render() {
    const { menuItems } = this.state;

    return (
      <MenuList>
        {menuItems.map((menu, index) => (
          <MenuItem onClick={menu.onClick} key={index}>
            {menu.icon}
            {menu.label}
          </MenuItem>
        ))}
      </MenuList>
    );
  }
}

export default User;
