import React from "react";

import Paper from "@material-ui/core/Paper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";

import { Search } from "components";

import User from "./user";
import * as Styled from "./styles";

const AppHeader = () => {
  const [isOpen, setIsOpen] = React.useState<boolean>(false);
  const [anchorEl, setAnchorEl] = React.useState(null);

  const toggleMenu = () => {
    if (isOpen) {
      setAnchorEl(null);
    }

    setIsOpen(!isOpen);
  };

  const handleClick = event =>
    setAnchorEl(anchorEl ? null : event.currentTarget);

  return (
    <Styled.Container>
      <Styled.SearchWrapper>
        <Search onSubmit={() => null} />
      </Styled.SearchWrapper>

      <Styled.IconButton>
        <Styled.ProfileIcon iconName="chat-massage" />
      </Styled.IconButton>
      <Styled.IconButton>
        <Styled.ProfileIcon iconName="bell" />
      </Styled.IconButton>

      <ClickAwayListener onClickAway={() => isOpen && toggleMenu()}>
        <Styled.UserWrapper>
          <Styled.UserButton
            onClick={event => {
              handleClick(event);

              if (!isOpen) {
                toggleMenu();
              }
            }}
          >
            <Styled.UserAvatar src="../user.png" />
            <Styled.UserInfo>
              <Styled.MediumSpan>Darlene Miles</Styled.MediumSpan>
              <Styled.CustomerName>Marketing Administrator</Styled.CustomerName>
            </Styled.UserInfo>
            <Styled.ExpandIcon />
          </Styled.UserButton>

          <Styled.PopperRoot
            transition
            open={isOpen}
            disablePortal
            anchorEl={anchorEl}
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{
                  transformOrigin:
                    placement === "bottom" ? "center top" : "center bottom"
                }}
              >
                <Paper>
                  <User history={{}} toggleMenu={toggleMenu} />
                </Paper>
              </Grow>
            )}
          </Styled.PopperRoot>
        </Styled.UserWrapper>
      </ClickAwayListener>
    </Styled.Container>
  );
};

export default AppHeader;
