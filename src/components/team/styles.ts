import styled from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div`
  padding: 26px 20px;
  width: 267px;
  background-color: ${colors.white};
  border: 1px solid ${colors.gallery};
  box-sizing: border-box;
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);
  border-radius: 12px;

  .MuiAvatar-root {
    margin-left: 0;
  }
`;

export const SearchWrapper = styled.div`
  margin-top: 16px;
  margin-bottom: 16px;

  input {
    width: 100%;
  }
`;

export const OfferWrapper = styled.div``;
