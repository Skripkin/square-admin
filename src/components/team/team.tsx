import React from "react";

import * as Styled from "./styles";
import { Search } from "components";
import SelectedOffer, { IOffer } from "../selected-offer/selected-offer";

interface IProps {
  teamate: IOffer[];
}

const Team = ({ teamate }: IProps) => {
  return (
    <Styled.Container>
      <p>Team</p>
      <Styled.SearchWrapper>
        <Search onSubmit={() => null} />
      </Styled.SearchWrapper>
      {teamate.map(item => (
        <Styled.OfferWrapper key={item.text}>
          <SelectedOffer image={item.image} text={item.text} />
        </Styled.OfferWrapper>
      ))}
    </Styled.Container>
  );
};

export default Team;
