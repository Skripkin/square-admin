import styled from "styled-components";

export const LayoutContainer = styled.div`
  display: flex;
`;

export const LeftSide = styled.div`
  width: 220px;
`;

export const RightSide = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

export const LayoutContent = styled.div`
  flex: 1;
`;
