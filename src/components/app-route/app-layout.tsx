import React from "react";

import { AppHeader, Menu } from "components";

import * as Styled from "./styles";

interface IProps {
  children: React.ReactElement | React.ReactElement[];
}

const AppLayout = ({ children }: IProps) => (
  <Styled.LayoutContainer>
    <Styled.LeftSide>
      <Menu />
    </Styled.LeftSide>
    <Styled.RightSide>
      <AppHeader />
      <Styled.LayoutContent>{children}</Styled.LayoutContent>
    </Styled.RightSide>
  </Styled.LayoutContainer>
);

export default AppLayout;
