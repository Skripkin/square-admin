import React from "react";
import { Route, RouteProps } from "react-router-dom";

import AppLayout from "./app-layout";

interface AppRouteProps extends RouteProps {
  component: React.ComponentType<any>;
  [props: string]: any;
}

const AppRoute = ({ component: Component, ...rest }: AppRouteProps) => (
  <Route
    {...rest}
    render={props => (
      <AppLayout>
        <Component {...props} />
      </AppLayout>
    )}
  />
);

export default AppRoute;
