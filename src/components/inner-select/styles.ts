import styled from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-left: 24px !important;
  padding: 8px 16px;
  min-width: 154px;
  height: 34px;
  box-sizing: border-box;
  border-radius: 20px;
  background-color: ${colors.white};

  .MuiSelect-root {
    padding: 0;
    font-size: 14px;
    line-height: 18px;
    text-align: right;
    color: ${colors.shipGray};
  }

  .MuiSelect-selectMenu.MuiSelect-selectMenu {
    padding-right: 0;
  }

  .MuiSelect-select:focus {
    background-color: transparent;
  }

  svg {
    position: relative;
    right: -8px;
  }

  fieldset {
    padding: 0;
    border: none;
  }
`;

export const Title = styled.span`
  display: inline-block;
  margin-right: 8px;
  font-size: 14px;
  color: ${colors.lightGrey};
`;
