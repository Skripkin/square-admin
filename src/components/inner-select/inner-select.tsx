import React from "react";

import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

import { Container, Title } from "./styles";

interface IOption {
  label: string;
  value: string | number;
}

interface IProps {
  title: string;
  value: string | number;
  options: IOption[];
  onChange: (value: string | number) => void;
}

const InnerSelect = ({ title, value, options, onChange }: IProps) => {
  const handleChange = event => {
    const { value: optionValue } = event.target;

    onChange(optionValue);
  };

  return (
    <Container>
      <Title>{title}:</Title>
      <FormControl variant="outlined">
        <Select
          labelId={`${title}-select-outlined-label`}
          id={`${title}-select-outlined`}
          value={value}
          onChange={handleChange}
        >
          {options.map(({ label, value }) => (
            <MenuItem value={value} key={label}>
              {label}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Container>
  );
};

export default InnerSelect;
