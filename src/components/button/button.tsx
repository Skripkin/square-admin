import styled from "styled-components";

import { colors } from "constants/.";

const Button = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  min-width: 106px;
  height: 40px;
  border: 0;
  border-radius: 4px;
  font-size: 14px;
  font-family: CircularStd;
  background-color: ${colors.pink};
  color: white;
  cursor: pointer;
`;

export default Button;
