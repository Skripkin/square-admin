import React from "react";

import Icon from "../icon";
import * as Styled from "./styles";

interface ILink {
  icon?: any;
  title: string;
  isactive?: boolean;
  link: string;
}

interface IProps {
  links: ILink[];
}

const SubMenu = ({ links }: IProps) => {
  return (
    <Styled.Container>
      {links.map(({ icon, title, isactive, link }) => (
        <Styled.Link
          key={title}
          isactive={isactive ? "true" : "false"}
          to={link}
        >
          <Styled.IconWrapper>
            {!!icon && <Icon iconName={icon} />}
          </Styled.IconWrapper>
          {title}
        </Styled.Link>
      ))}
    </Styled.Container>
  );
};

export default SubMenu;
