import styled, { css } from "styled-components";
import { Link as ReactLink } from "react-router-dom";

import { colors } from "constants/.";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  top: 24px;
  right: 40px;
  padding: 12px 0;
  width: 265px;
  box-sizing: border-box;
  border-radius: 12px;
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);
  background-color: white;
`;

const getStylesForActiveLink = () => css`
  position: relative;
  color: ${colors.pink};

  ${IconWrapper} {
    svg > use {
      fill: white;
      stroke: ${colors.pink};
      stroke-width: 2px;
    }

    &::after {
      content: "";
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      width: 2px;
      background-color: ${colors.pink};
    }
  }
`;

export const IconWrapper = styled.span`
  display: flex;
  margin-right: 20px;
  font-size: 25px;

  svg > use {
    fill: white;
    stroke: ${colors.lightGrey};
    stroke-width: 2px;
  }
`;

export const Link = styled(ReactLink)<{ isactive?: "true" | "false" }>`
  display: inline-flex;
  align-items: center;
  padding: 15px 25px;
  font-size: 15px;
  text-decoration: none;
  color: ${colors.lightGrey};

  ${props => props.isactive === "true" && getStylesForActiveLink()}

  &:hover {
    ${getStylesForActiveLink}
  }
`;
