import { urls } from "constants/.";

interface MenuItem {
  icon?: any;
  text: string;
  link: string;
  isactive?: boolean;
}

export const MENU: MenuItem[] = [
  {
    icon: "home",
    text: "Home",
    link: urls.HOME
  },
  {
    icon: "calendar",
    text: "Timetable",
    isactive: true,
    link: urls.TIMETABLE
  },
  {
    icon: "reserv",
    text: "Reservations",
    link: urls.RESERVATIONS
  },
  {
    icon: "image",
    text: "Contents",
    link: urls.CONTENS
  },
  {
    icon: "star",
    text: "Coupon",
    link: "#"
  }
];
