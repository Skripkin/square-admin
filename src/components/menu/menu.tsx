import React from "react";
import { withRouter } from "react-router-dom";

import _get from "lodash/get";

import { urls } from "constants/.";

import { CreateNewBusiness, ModalBox } from "components/.";

import * as Styled from "./styles";
import { MENU } from "./constants";

import ChooseLocation from "./choose-location";

const Menu = ({ history, match }) => {
  const path = _get(history, "location.pathname", "/");
  const SELECT_OPTIONS = [
    {
      title: "Holding",
      value: "1"
    },
    {
      title: "Marrone Rosso",
      value: "2",
      img: "./Cameron.png"
    },
    {
      title: "Del Papa",
      value: "3"
    },
    {
      title: "Del Papa Marrone Rosso",
      value: "4"
    },
    {
      title: "Del Papa",
      value: "5"
    }
  ];

  const choose = e => {};

  const isAdmin = true;

  return (
    <>
      <Styled.Container>
        <Styled.LogoWrapper to={urls.HOME}>
          <img src="../logo.svg" alt="logo" />
        </Styled.LogoWrapper>
        {isAdmin && (
          <Styled.ChooseLocationWrapper>
            <ChooseLocation
              title="Choose location"
              options={SELECT_OPTIONS}
              value={SELECT_OPTIONS[0].value}
              onChange={choose}
              onCreateNewBusiness={() =>
                history.push(urls.check(`${match.url}/create-new-business`))
              }
            />
          </Styled.ChooseLocationWrapper>
        )}
        <Styled.Content>
          {MENU.map(item => (
            <Styled.MenuItem
              key={item.text}
              to={item.link}
              isactive={
                path === item.link ||
                (path.includes(item.link) && item.link !== urls.HOME)
                  ? "true"
                  : "false"
              }
            >
              <Styled.IconWrapper>
                <Styled.MenuIcon iconName={item.icon} />
              </Styled.IconWrapper>
              <Styled.Text>{item.text}</Styled.Text>
            </Styled.MenuItem>
          ))}
        </Styled.Content>
      </Styled.Container>
      <ModalBox
        type="modal"
        parentPath={match.url}
        path={urls.check(`${match.url}/create-new-business`)}
        Component={CreateNewBusiness}
      />
    </>
  );
};

export default withRouter(Menu);
