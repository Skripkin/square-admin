import styled from "styled-components";

import { colors } from "constants/.";

export const Text = styled.div`
  font-size: 14px;
  line-height: 18px;
  // color: ${colors.darkGrey};
`;

export const ItemsForm = styled.div`
  & > div {
    width: 100%;
  }

  .MuiOutlinedInput-input {
    padding: 8px 16px;
  }
  .MuiSelect-select {
    &:focus {
      border-radius: 4px;
      background-color: #f5f5f5;
    }
  }

  .MuiInputLabel-outlined {
    transform: translate(14px, 15px) scale(1);
  }

  fieldset {
    border-color: ${colors.concrete} !important;
  }
`;

export const TitleOfSelect = styled.p`
  margin-bottom: 10px;
  font-size: 14px;
  line-height: 18px;
  color: ${colors.darkGrey};
`;

export const ItemWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const ImageWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 32px;
  height: 32px;
  margin-right: 8px;
  border-radius: 50%;
`;

export const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const Initials = styled.div`
  color: white;
`;

export const AddItem = styled.div`
  padding: 5px 0px;
  color: ${colors.pink};
`;
