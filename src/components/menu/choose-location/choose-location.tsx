import React from "react";

import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import MaterialSelect from "@material-ui/core/Select";

import { colors } from "constants/.";

import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";

import {
  ItemsForm,
  TitleOfSelect,
  ImageWrapper,
  ItemWrapper,
  Image,
  Text,
  Initials,
  AddItem
} from "./styled";

interface IOption {
  title: string;
  value: string;
  img?: string;
}

interface IProps {
  title: string;
  value: string;
  options: IOption[];
  onChange: (value: string) => void;
  onCreateNewBusiness: () => void;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      backgroundColor: colors.whisper,
      minWidth: 100
    },
    root: {
      paddingTop: "10px",
      paddingBottom: "10px",
      color: colors.darkGrey,

      "&:hover": {
        color: colors.darkGrey,
        backgroundColor: "#F5F5F5 !important"
      },

      "&:focus": {
        color: colors.shipGray,
        backgroundColor: "#F5F5F5"
      },
      "&:selected": {
        backgroundColor: "#F5F5F5"
      }
    },
    selected: {
      backgroundColor: "#F5F5F5 !important"
    }
  })
);

const ChooseLocation = ({
  title,
  options,
  value,
  onChange,
  onCreateNewBusiness = () => null
}: IProps) => {
  const classes = useStyles("");

  const colors = ["#8DC66B", "#6BB5C6", "#E19C5D"];

  return (
    <ItemsForm>
      <TitleOfSelect>{title}</TitleOfSelect>
      <FormControl variant="outlined" className={classes.formControl}>
        <MaterialSelect
          labelId="simple-select-outlined-label"
          id="simple-select-outlined"
          value={value}
          onChange={(e: any) => onChange(e.target.value)}
          IconComponent={KeyboardArrowDownIcon}
          MenuProps={{
            anchorOrigin: {
              vertical: 52,
              horizontal: "left"
            },
            transformOrigin: {
              vertical: "top",
              horizontal: "left"
            },
            getContentAnchorEl: null
          }}
        >
          {options.map((item, index) => (
            <MenuItem
              className={classes.root}
              classes={{
                selected: classes.selected
              }}
              key={item.value}
              value={item.value}
            >
              <ItemWrapper>
                <ImageWrapper
                  style={{ background: colors[index % colors.length] }}
                >
                  {item.img ? (
                    <Image src={item.img} />
                  ) : (
                    <Initials>{`${item.title
                      .toUpperCase()
                      .split(" ")
                      .map(item => item.substr(0, 1))
                      .splice(0, 3)
                      .join("")}`}</Initials>
                  )}
                </ImageWrapper>
                <Text>{item.title}</Text>
              </ItemWrapper>
            </MenuItem>
          ))}
          <MenuItem value="add" role="button">
            <AddItem onClick={onCreateNewBusiness}>
              + Create new business
            </AddItem>
          </MenuItem>
        </MaterialSelect>
      </FormControl>
    </ItemsForm>
  );
};

export default ChooseLocation;
