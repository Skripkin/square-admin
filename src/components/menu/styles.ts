import styled, { css } from "styled-components";

import { Link } from "react-router-dom";

import { colors } from "constants/.";

import Icon from "../icon";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  padding: 0 0 0 24px;
  background-color: white;
`;

export const LogoWrapper = styled(Link)`
  display: inline-flex;
  align-items: center;
  height: 72px;
  border-right: 1px solid ${colors.gallery};
`;

export const Content = styled.div`
  flex: 1;
  padding-top: 35px;
  border-right: 1px solid ${colors.gallery};
`;

export const IconWrapper = styled.span`
  display: inline-flex;
  margin-right: 18px;
  font-size: 24px;
  color: ${colors.shipGray};
`;

export const MenuIcon = styled(Icon)`
  fill: white;
  stroke: ${colors.shipGray};
  stroke-width: 2px;
`;

export const Text = styled.span`
  font-size: 14px;
  align-self: center;
  color: ${colors.shipGray};
`;

export const ChooseLocationWrapper = styled.div`
  position: relative;
  padding: 16px 24px 16px 0;
  border-right: 1px solid ${colors.gallery};

  &:after {
    content: "";
    position: absolute;
    height: 1px;
    left: -29px;
    right: 0px;
    bottom: 0;
    background: ${colors.gallery};
  }
`;

export const MenuItem = styled(Link)<{ isactive: "true" | "false" }>`
  display: flex;
  margin-right: 24px;
  margin-bottom: 35px;
  font-size: 14px;
  text-decoration: none;

  &:last-child {
    margin-bottom: 0;
  }

  &:hover {
    ${IconWrapper} {
      color: ${colors.pink};
    }

    ${MenuIcon} {
      stroke: ${colors.pink};
    }

    ${Text} {
      color: ${colors.pink};
    }
  }

  ${props =>
    props.isactive === "true" &&
    css`
      pointer-events: none;

      ${IconWrapper} {
        color: ${colors.pink};
      }

      ${Text} {
        color: ${colors.pink};
      }

      ${MenuIcon} {
        stroke: ${colors.pink};
      }
    `}
`;
