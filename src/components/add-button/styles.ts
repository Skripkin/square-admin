import styled from "styled-components";

import { colors } from "constants/.";

export const Container = styled.div`
  .MuiFab-secondary {
    box-shadow: none;
    background-color: ${colors.pink};
  }
`;
