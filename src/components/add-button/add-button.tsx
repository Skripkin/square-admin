import React from "react";

import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import { makeStyles } from "@material-ui/core/styles";

import { Container } from "./styles";

const useStyles = makeStyles(theme => ({
  margin: {
    margin: theme.spacing(1)
  },
  extendedIcon: {
    marginRight: theme.spacing(1)
  }
}));

interface IProps {
  onClick?: () => void;
}

const AddButton = ({ onClick = () => null }: IProps) => {
  const classes = useStyles("");

  return (
    <Container onClick={onClick}>
      <Fab
        size="small"
        color="secondary"
        aria-label="add"
        className={classes.margin}
      >
        <AddIcon />
      </Fab>
    </Container>
  );
};

export default AddButton;
