import React from "react";

import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

import {
  Input,
  Select,
  Button,
  SelectedOfferList,
  Checkbox
} from "components/.";

import * as Styles from "./styled";
import { TIPS, SELECTED_OFFERS, SET_CHECKBOX } from "./mock";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    menu: {
      boxShadow: "0px 2px 12px rgba(0, 0, 0, 0.1)",
      borderRadius: "8px",
      backgroundColor: "white"
    }
  })
);

const SetTimeFrame = () => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [openwind, setOpenwind] = React.useState<null | HTMLElement>(null);
  const [checked, setChecked] = React.useState(false);

  const [items, setItems] = React.useState(SET_CHECKBOX);

  const classes = useStyles("");

  const handleChange = item => {
    setItems(
      items.map(weekDay => (weekDay.name === item.name ? item : weekDay))
    );
  };

  const Click = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setOpenwind(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
    setOpenwind(null);
  };

  return (
    <Styles.Container>
      <Styles.Header>
        <Styles.HeaderIcon iconName="arrow" />
        <Styles.TimeTitle>11:00 - 14:00</Styles.TimeTitle>
        <button onClick={Click}>
          <Styles.HeaderIcon iconName="copy" />
        </button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
          PopoverClasses={{
            paper: classes.menu
          }}
        >
          <MenuItem disableTouchRipple>
            <button onClick={handleClick}>
              <Styles.MenuItemList>Duplicate offers</Styles.MenuItemList>
            </button>
          </MenuItem>
          <MenuItem disableTouchRipple>
            <Styles.MenuItemList>Duplicate timeframe</Styles.MenuItemList>
          </MenuItem>
        </Menu>
        <Menu
          id="simple-menu"
          anchorEl={openwind}
          keepMounted
          open={Boolean(openwind)}
          onClose={handleClose}
          PopoverClasses={{
            paper: classes.menu
          }}
        >
          <Styles.ChooseDayBackdrop onClick={handleClose} />
          <Styles.ChooseDayBox>
            <Styles.ChooseDayTitle>Choose day</Styles.ChooseDayTitle>
            {!!checked ? (
              <button onClick={handleClose}>
                <Styles.ItemOk>OK</Styles.ItemOk>
              </button>
            ) : (
              <button onClick={handleClose}>
                <Styles.FooterIcon iconName="cros" />
              </button>
            )}
          </Styles.ChooseDayBox>
          {items.map(item => (
            <Styles.MenuCheckBox
              onClick={() => handleChange(item)}
              key={item.name}
              disableTouchRipple
            >
              <Checkbox
                checked={checked}
                name={item.name}
                title={item.title}
                onChange={(_, checked) => setChecked(checked)}
              />
            </Styles.MenuCheckBox>
          ))}
        </Menu>
      </Styles.Header>
      <Select
        title="Choose day"
        options={TIPS}
        value={TIPS[0].value}
        onChange={() => null}
      />
      <Styles.FromToForm>
        <Input placeholder="" title="from" value="" onChange={() => null} />
        <Styles.FromToSlach>-</Styles.FromToSlach>
        <Input placeholder="" title="To" value="" onChange={() => null} />
        <Input placeholder="" title="Spots" value="" onChange={() => null} />
      </Styles.FromToForm>
      <Styles.TwoInputBox>
        <Select
          title="Type"
          options={TIPS}
          value={TIPS[0].value}
          onChange={() => null}
        />
        <Input placeholder="" title="Budget" value="" onChange={() => null} />
      </Styles.TwoInputBox>
      <Styles.OfferList>
        <SelectedOfferList offers={SELECTED_OFFERS} onRemove={() => null} />
      </Styles.OfferList>
      <Select
        title="Add new offer"
        options={TIPS}
        value={TIPS[0].value}
        onChange={() => null}
      />
      <Styles.DeleteBox>
        <Styles.FooterIcon iconName="delete" />
        <Styles.DeleteText>Delete timeframe</Styles.DeleteText>
      </Styles.DeleteBox>
      <Styles.ButtonBox>
        <Styles.GrayButton>
          <Styles.GrayButtonText>Cancel</Styles.GrayButtonText>
        </Styles.GrayButton>
        <Button>
          <span>Create</span>
        </Button>
      </Styles.ButtonBox>
    </Styles.Container>
  );
};

export default SetTimeFrame;
