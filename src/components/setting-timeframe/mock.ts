export const TIPS = [
  {
    title: "SomeItems",
    value: "1"
  },
  {
    title: "SomeItems",
    value: "2"
  },
  {
    title: "SomeItems",
    value: "3"
  },
  {
    title: "SomeItems",
    value: "4"
  },
  {
    title: "SomeItems",
    value: "5"
  }
];

export const SELECTED_OFFERS = [
  {
    image: "./ice-cream.png",
    text: "Milk Icecream"
  },
  {
    image: "./pizza.png",
    text: "Pizza Mania"
  },
  {
    image: "./salat.png",
    text: "Cheesecake"
  }
];

export const SET_CHECKBOX = [
  {
    name: "Monday",
    title: "Monday"
  },
  {
    name: "Tuesday",
    title: "Tuesday"
  },
  {
    name: "Wednesday",
    title: "Wednesday"
  },
  {
    name: "Thirsday",
    title: "Thirsday"
  }
];
