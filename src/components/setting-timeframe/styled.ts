import styled from "styled-components";

import MenuItem from "@material-ui/core/MenuItem";

import { Icon, Button } from "components/.";
import { Title } from "components/checkbox/styles";

import { colors } from "constants/.";

export const Container = styled.div`
  max-width: 275px;
  padding: 18px 0;
  box-shadow: -1px 0px 0px #ebebeb;
  background: #ffffff;

  & > div {
    padding-left: 24px;
    padding-right: 24px;
  }
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  position: relative;
  margin-bottom: 24px;
  padding-bottom: 15px;
  border-bottom: 1px solid #ebebeb;
`;

export const MenuItemList = styled.p`
  font-size: 15px;
  line-height: 19px;
  color: #404043;
`;

export const MenuCheckBox = styled(MenuItem)`
  &:hover {
    background-color: inherit !important;
  }

  .MuiCheckbox-root {
    padding: 0;
  }

  button {
    height: 38px;
    padding: 0;
    box-shadow: none;
    border: none;
  }
`;

export const ItemOk = styled.span`
  font-weight: 500;
  font-size: 14px;
  line-height: 18px;
  text-align: right;
  color: ${colors.pink};
`;

export const ChooseDayBox = styled.div`
  height: 24px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 16px;

  ${Title} {
    color: ${colors.shipGray};
  }
`;

export const ChooseDayTitle = styled.span`
  font-size: 14px;
  color: ${colors.darkGrey};
`;

export const ChooseDayBackdrop = styled.button`
  display: block;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: -1;
  width: 100vw;
  height: 100vh;
  background-color: rgba(0, 0, 0, 0.3);
  cursor: pointer;
`;

export const TimeTitle = styled.span`
  margin-right: auto;
  align-self: center;
`;

export const HeaderIcon = styled(Icon)`
  stroke: #404043;
  fill: white;
`;

export const FromToForm = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 24px 0;

  & > div {
    max-width: 73px;
  }

  & > div:last-child {
    max-width: 48px;
  }
`;

export const FromToSlach = styled.span`
  padding-top: 30px;
  margin: auto 0;
`;

export const TwoInputBox = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 24px;

  & > div {
    width: 47%;
  }
`;

export const OfferList = styled.div`
  margin-bottom: 24px;
`;

export const DeleteBox = styled.div`
  display: flex;
  align-items: flex-end;
  padding: 16px 0;
  margin-top: 16px;
  box-shadow: inset 0px 1px 0px #f2f2f2;
`;

export const DeleteText = styled.span`
  font-size: 13px;
  line-height: 16px;
  color: #9ea0b4;
`;

export const FooterIcon = styled(Icon)`
  stroke: #9ea0b4;
  fill: white;
`;

export const ButtonBox = styled.div`
  display: flex;
  justify-content: space-around;
  padding-top: 20px;
  box-shadow: inset 0px 1px 0px #f2f2f2;
  background: #ffffff;

  & > button {
    width: 47%;
  }
`;

export const GrayButton = styled(Button)`
  border: 1px solid #ebebeb;
  border-radius: 4px;
  box-sizing: border-box;
  background: #fafafa;
`;

export const GrayButtonText = styled.span`
  color: #404043;
`;
