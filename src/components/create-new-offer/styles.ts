import styled from "styled-components";

import Select from "@material-ui/core/Select";

import Icon from "../icon";
import Button from "components/button";

import { colors } from "constants/.";

export const CloseIcon = styled(Icon)`
  stroke: #404043;
`;

export const Container = styled.div`
  max-width: 100%;
  position: relative;
  padding: 32px;
  border-radius: 15px 0 0 15px;
  background: ${colors.white};

  .MuiOutlinedInput-inputSelect {
    background: #fcfcfc;
  }

  input {
    background: #fcfcfc;
    border: 1.5px solid #f3f3f3;
  }
`;

export const ImageWrapper = styled.div`
  text-align: center;

  label {
    width: 100%;
  }
`;

export const HeaderBox = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 32px;
`;

export const Title = styled.span`
  vertical-align: middle;
  text-align: left;
  font-weight: 500;
  font-size: 18px;
  line-height: 23px;
  text-transform: capitalize;
  color: ${colors.shipGray};
`;

export const Close = styled.button`
  vertical-align: top;
  text-align: right;
`;

export const InputContainer = styled.div`
  margin: 24px 0;
`;

export const TwoInputs = styled.div`
  display: flex;
  position: relative;
  align-items: flex-end;
  margin: 24px 0;
`;

export const SelectWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-left: 24px !important;
  padding: 8px 16px;
  width: 218px;
  min-height: 49px;
  border: 1.5px solid #f3f3f3;
  border-radius: 4px;
  box-sizing: border-box;
  background: #fcfcfc;

  .MuiSelect-root {
    padding: 0;
    font-size: 14px;
    line-height: 18px;
    text-align: right;
    color: ${colors.shipGray};
  }

  .MuiSelect-select:focus {
    background-color: transparent;
  }

  svg {
    position: relative;
    right: -8px;
  }

  fieldset {
    padding: 0;
    border: none;
  }
`;

export const SelectText = styled.span`
  display: inline-block;
  margin-right: 8px;
  font-size: 14px;
  color: ${colors.shipGray};
`;

export const SpecialSelect = styled(Select)`
  max-width: 156px;
`;

export const AddContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  padding: 24px 0;
  margin-bottom: 24px;
  border-bottom: 1px solid #f2f2f2;
  border-top: 1px solid #f2f2f2;
`;

export const RightAddInput = styled.div`
  flex: 0.3;
  margin-left: 8px;
`;

export const LeftAddInput = styled.div`
  flex: 1.7;
`;

export const AddButton = styled.button`
  position: absolute;
  bottom: -12px;
  padding: 3px 10px;
  left: 50%;
  transform: translateX(-50%);
  border: 1px solid #ebebeb;
  border-radius: 12px;
  box-sizing: border-box;
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);
  background: #ffffff;
  color: ${colors.pink};
`;

export const ButtonBox = styled.div`
  display: flex;
  justify-content: space-around;
  padding: 20px 0;
  border-radius: 0 0 0 15px;
  box-shadow: inset 0px 1px 0px #f2f2f2;
  background: #ffffff;

  & > button {
    min-width: 156px;
  }
`;

export const GrayButton = styled(Button)`
  border: 1px solid #ebebeb;
  border-radius: 4px;
  box-sizing: border-box;
  background: #fafafa;
`;

export const GrayButtonText = styled.span`
  color: #404043;
`;
