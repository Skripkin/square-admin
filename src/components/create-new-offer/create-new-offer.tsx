import React from "react";
import _get from "lodash/get";

import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";

import Imagepicker from "components/imagepicker";
import Select from "components/select";
import Input from "components/input";
import Button from "components/button";

import * as Styled from "./styles";

import { SELECT_OPTIONS } from "./mock";

const CreateNewOffer = props => {
  const [age, setAge] = React.useState(20);
  const handleChange = event => {
    const { value: perPage } = event.target;

    setAge(perPage);
  };
  const history = _get(props, "0.history", {});

  return (
    <Styled.Container>
      <Styled.HeaderBox>
        <Styled.Title>Create new offer</Styled.Title>
        <Styled.Close onClick={history.goBack}>
          <Styled.CloseIcon iconName="cros" />
        </Styled.Close>
      </Styled.HeaderBox>
      <Styled.ImageWrapper>
        <Imagepicker />
      </Styled.ImageWrapper>
      <Styled.InputContainer>
        <Input
          placeholder=""
          title="Offer name"
          value=""
          onChange={() => null}
        />
      </Styled.InputContainer>
      <Select
        title="Type"
        options={SELECT_OPTIONS}
        value={SELECT_OPTIONS[0].value}
        onChange={() => null}
      />
      <Styled.TwoInputs>
        <Input
          placeholder=""
          title="Insert discount"
          value=""
          onChange={() => null}
        />
        <Styled.SelectWrapper>
          <Styled.SelectText>%</Styled.SelectText>
          <FormControl variant="outlined">
            <Styled.SpecialSelect
              labelId="demo-simple-select-outlined-label"
              id="demo-simple-select-outlined"
              value={age}
              onChange={handleChange}
            >
              <MenuItem value={10}>All items</MenuItem>
              <MenuItem value={15}>Today</MenuItem>
              <MenuItem value={20}>All</MenuItem>
            </Styled.SpecialSelect>
          </FormControl>
        </Styled.SelectWrapper>
      </Styled.TwoInputs>
      <Styled.AddContainer>
        <Styled.LeftAddInput>
          <Input
            placeholder=""
            title="First item"
            value=""
            onChange={() => null}
          />
        </Styled.LeftAddInput>
        <Styled.RightAddInput>
          <Input placeholder="" title="Qt" value="" onChange={() => null} />
        </Styled.RightAddInput>
        <Styled.AddButton>+ Add Item</Styled.AddButton>
      </Styled.AddContainer>
      <Input placeholder="" title="Notes" value="" onChange={() => null} />
      <Styled.ButtonBox>
        <Styled.GrayButton>
          <Styled.GrayButtonText>Cancel</Styled.GrayButtonText>
        </Styled.GrayButton>
        <Button>
          <span>Create</span>
        </Button>
      </Styled.ButtonBox>
    </Styled.Container>
  );
};

export default CreateNewOffer;
