import styled from "styled-components";

import { colors } from "constants/.";

export const InputContainer = styled.div`
  .MuiFormControl-root {
    width: 100%;
  }
`;

export const InputTitle = styled.p`
  margin-bottom: 10px;
  line-height: 18px;
  font-size: 14px;
  color: ${colors.darkGrey};
`;
