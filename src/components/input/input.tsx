import React from "react";

import { createStyles, withStyles, Theme } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import InputBase from "@material-ui/core/InputBase";

import { colors } from "constants/.";

import { InputTitle, InputContainer } from "./styles";

interface IProps {
  title: string;
  placeholder: string;
  value: string;
  onChange: (value: string) => void;
}

const BootstrapInput = withStyles((theme: Theme) =>
  createStyles({
    input: {
      borderRadius: 4,
      border: `1px solid ${colors.concrete}`,
      fontSize: 16,
      padding: "14px 26px 14px 12px"
    }
  })
)(InputBase);

const Input = ({ title, placeholder, value, onChange }: IProps) => {
  return (
    <InputContainer>
      <FormControl>
        <InputTitle>{title}</InputTitle>
        <InputLabel htmlFor="demo-customized-textbox"></InputLabel>
        <BootstrapInput
          value={value}
          placeholder={placeholder}
          id="demo-customized-textbox"
          onChange={e => onChange(e.target.value)}
        />
      </FormControl>
    </InputContainer>
  );
};

export default Input;
