import React from "react";
import moment from "moment";

import * as Styled from "./styles";
import { colors } from "../../constants";

interface MetricType {
  paid: boolean;
  personCount: number;
  from: Date;
  to: Date;
}

const EventsForDate = ({ paid, personCount, from, to }: MetricType) => {
  const eventColor = paid === true ? colors.green : colors.pink;
  const day = moment(from).format("ddd, DD MMM");
  const time = `${moment(from).format("hh:ss")} - ${moment(to).format(
    "hh:ss"
  )}`;

  return (
    <Styled.Container>
      <Styled.DateBlock>
        <Styled.Status>
          <Styled.StatusIcon style={{ background: eventColor }} />
          <Styled.DateText>{day}</Styled.DateText>
        </Styled.Status>
        <Styled.PersonCount>
          <span>{personCount}</span>
          <Styled.EventsDateIcon iconName="user" />
        </Styled.PersonCount>
      </Styled.DateBlock>
      <Styled.Time>{time}</Styled.Time>
    </Styled.Container>
  );
};

export default EventsForDate;
