import styled from "styled-components";

import { colors } from "../../constants";

import Icon from "../icon";

export const Container = styled.div`
  padding: 16px 16px 24px;
  box-sizing: border-box;
  border: 1px solid ${colors.gallery};
  box-shadow: 0px 0px 32px rgba(237, 237, 237, 0.5);
  border-radius: 12px;
  background-color: ${colors.white};
`;

export const DateBlock = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  & {
    text-transform: capitalize;
  }
`;

export const Status = styled.div`
  display: flex;
  align-items: center;
`;

export const StatusIcon = styled.div`
  width: 8px;
  height: 8px;
  border-radius: 50%;
`;

export const DateText = styled.span`
  margin-left: 8px;
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  color: ${colors.shipGray};
`;

export const PersonCount = styled.div`
  display: flex;
  align-items: center;
  color: ${colors.shipGray};

  & > span {
    margin-right: 5px;
  }
`;

export const Time = styled.div`
  margin-top: 16px;
  color: ${colors.lightGrey};
`;

export const EventsDateIcon = styled(Icon)`
  width: 16px;
  height: 16px;
  vertical-align: text-bottom;
  stroke: ${colors.lightGrey};
  fill: ${colors.lightGrey};
`;
