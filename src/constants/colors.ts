export const black = "#000000";
export const white = "#FFFFFF";

export const whisper = "#FAFAFC";
export const concrete = "#F3F3F3";
export const shipGray = "#404043";
export const gallery = "#EBEBEB";
export const lightGrey = "#9EA0B4";
export const darkGrey = "#807F87";
export const athensGray = "#F2F3F5";

export const pink = "#F13F98";

export const cornflowerBlue = "#536DF5";
export const royalBlue = "#367FE1";

export const green = "#56D354";

export const carnation = "#F46C64";
