const urls = {
  check: (url: string) => url.replace("//", "/"),
  HOME: "/",
  CONTACTS: "/contacts",
  CONTENS: "/contens",
  ACTIONS: "/contens/actions",
  RESERVATIONS: "/reservations",
  IMAGES: "/images",
  ACCOUNTSETTINGS: "/account-settings",
  BILLING: "/billing",
  SUBSCRIPTIONS: "/subscriptions",
  NOTIFICATIONS: "/notifications",
  CHAT: "/chat",
  TIMETABLE: "/time-table"
};

export default urls;
