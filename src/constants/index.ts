import * as colors from "./colors";
import urls from "./urls";

export { colors, urls };
