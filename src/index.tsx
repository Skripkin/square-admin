import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { ModalContainer } from "react-router-modal";

import App from "./app";
import GlobalStyle from "./styles";
import GlobalFonts from "./styles/fonts";

ReactDOM.render(
  <Router>
    <GlobalStyle />
    <GlobalFonts />
    <App />
    <ModalContainer outDelay={300} />
  </Router>,
  document.getElementById("root")
);
