import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";

import {
  Home,
  Contacts,
  Contens,
  Actions,
  Reservations,
  Images,
  Subscriptions,
  Billing,
  Notifications,
  Chat,
  TimeTable,
  AccountSettings
} from "pages/.";
import { AppRoute } from "components/.";
import { urls } from "constants/.";

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <AppRoute path={urls.TIMETABLE} component={TimeTable} />
        <AppRoute path={urls.ACCOUNTSETTINGS} component={AccountSettings} />
        <AppRoute path={urls.SUBSCRIPTIONS} component={Subscriptions} />
        <AppRoute path={urls.CHAT} component={Chat} />
        <AppRoute path={urls.ACTIONS} component={Actions} />
        <AppRoute path={urls.CONTENS} component={Contens} />
        <AppRoute path={urls.RESERVATIONS} component={Reservations} />
        <AppRoute path={urls.BILLING} component={Billing} />
        <AppRoute path={urls.IMAGES} component={Images} />
        <AppRoute path={urls.NOTIFICATIONS} component={Notifications} />
        <AppRoute path={urls.CONTACTS} component={Contacts} />
        <AppRoute path={urls.HOME} component={Home} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
