import { createGlobalStyle } from "styled-components";

import { colors } from "constants/.";

const globalStyles = createGlobalStyle`
  *,
  *:before,
  *:after {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    -webkit-tap-highlight-color: transparent;
  }

  body {
    color: black;
    background-color: ${colors.whisper};
    font-family: CircularStd;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -webkit-touch-callout: none;
    -webkit-text-size-adjust: none;
    -webkit-user-select: none;
  }

  button {
    border: none;
    outline: none;
    background-color: transparent;
    cursor: pointer;
  }

  .title {
    line-height: 28px;
    font-weight: 500;
    font-size: 22px;
    text-transform: capitalize;
    color: ${colors.shipGray};
  }

  .inner-container {
    position: relative;
    padding: 24px 312px 24px 40px;
    height: 100%;
    box-sizing: border-box;
  }

  .inner-right-block {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    width: 275px;
    border-left: 1px solid ${colors.gallery};
    box-sizing: border-box;
    background-color: white;
  }
`;

export default globalStyles;
