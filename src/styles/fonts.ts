import { createGlobalStyle } from "styled-components";

export function fontFace(
  name,
  src,
  fontWeight = "normal",
  fontStyle = "normal"
) {
  return `
    @font-face{
      font-family: "${name}";
      font-style: ${fontStyle};
      font-weight: ${fontWeight};
      src: url(${require("../../public/fonts/" + src + ".eot")});
      src: url(${require("../../public/fonts/" +
        src +
        ".eot")}?#iefix) format("embedded-opentype"),
           url(${require("../../public/fonts/" + src + ".otf")}) format("otf"),
           url(${require("../../public/fonts/" +
             src +
             ".woff")}) format("woff"),
           url(${require("../../public/fonts/" +
             src +
             ".woff2")}) format("woff2"),
           url(${require("../../public/fonts/" +
             src +
             ".ttf")}) format("truetype"),
           url(${require("../../public/fonts/" +
             src +
             ".svg")}#${name}) format("svg");
      }
  `;
}

const GlobalFonts = createGlobalStyle`
  ${fontFace("CircularStd", "CircularStd/CircularStd-Book/CircularStd-Book")};
  ${fontFace(
    "CircularStd",
    "CircularStd/CircularStd-BookItalic/CircularStd-BookItalic",
    "medium",
    "italic"
  )};
  ${fontFace(
    "CircularStd",
    "CircularStd/CircularStd-Bold/CircularStd-Bold",
    "bold"
  )};
  ${fontFace(
    "CircularStd",
    "CircularStd/CircularStd-BoldItalic/CircularStd-BoldItalic",
    "bold",
    "italic"
  )};
    
  body{
    font-family: 'CircularStd';
  }  
`;

export default GlobalFonts;
